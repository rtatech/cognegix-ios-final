//
//  DataManager.swift

//
import Foundation
import Alamofire
import SwiftyJSON

class DataManager: NSObject {

    func callToAPI( _ strUrl : String,parameters:Parameters, completion:@escaping (_ result: AnyObject, _ error: AnyObject)->Void)
    {

        print("111:\(strUrl)")
         print("111 parameters:\(parameters)")
        General.showActivityIndicator()
        
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            APP_DELEGATE.showAlert(strMessage: "Internet Connection not Available!")
            return
        }else{
            print("Internet Connection Available!")
            
        }
        
        
        let manager = RequestManager()
        manager.requestUrl(query: strUrl, params: parameters) { (result, error) in
            print("Result:>>>>>\(result)")
            print("Error:>>>>>\(error)")
            if error.isKind(of: NSError.classForCoder()) //Error
            {
                completion("" as AnyObject, error)
            }
            else
            {
                //Parse result
                let manager = ParseManager()
                if APP_DELEGATE.API_Name == NotificationList{
                    let arrPosts = manager.parseNotificationListFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == Notification_mark_read{
                    let arrPosts = manager.parseBooleanFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == changepassword{
                    let arrPosts = manager.parseBooleanFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == guest_help_raise{
                    let arrPosts = manager.parseBooleanFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == update_profile_type{
                    let arrPosts = manager.parseBooleanFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == ForgetPassword{
                    let arrPosts = manager.parseBooleanFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == OTPVerification{
                    let arrPosts = manager.parseBooleanFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == updateProfile{
                    let arrPosts = manager.parseLoginFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == forumpostadd{
                    let arrPosts = manager.parseBooleanFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == forumposts{
                    let arrPosts = manager.parseforumpostsFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == forum_thread_posts{
                    let arrPosts = manager.parseforumThreadPostsFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == forum_thread_post_delete{
                    let arrPosts = manager.parseforumThreadPostsFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == myquery_thread_list{
                    let arrPosts = manager.parsemyqueryThreadListFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == my_query_thread_message_list{
                    let arrPosts = manager.parsemyqueryThreadMessageListFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == my_query_thread_message{
                    let arrPosts = manager.parseBooleanFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == my_query_check_thread{
                    let arrPosts = manager.parseBooleanMyQueryCheckThreadFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == Badges_of_User_by_Program{
                    let arrPosts = manager.parseBadgesOfUserByProgramFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == program_topics{
                    let arrPosts = manager.parseProgramTopicsListFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == user_program_badges{
                    let arrPosts = manager.parseUserBadgesListFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == program_badge_leaders{
                    let arrPosts = manager.parseProgramBadgesLeaderFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == program_users{
                    let arrPosts = manager.parseParticipantListFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == badge_distribute{
                    let arrPosts = manager.parseAssignBadgeBooleanFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == program_learning_tree_search{
                    let arrPosts = manager.parseLearningTreeSearchFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == program_detail{
                    let arrPosts = manager.parseUserProgramDetailsFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }
                
                
                
                
                
                
                else if APP_DELEGATE.API_Name == pages{
                    let arrPosts = manager.parsePagesFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == detailevent{
                    let arrPosts = manager.parseEventFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == signup_first{
                    let arrPosts = manager.parseOTPFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == bookevent{
                    let arrPosts = manager.parseVerificationFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else if APP_DELEGATE.API_Name == verifysocialaccount{
                    let arrPosts = manager.parseVerificationFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }else{
                    let arrPosts = manager.parseLoginFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                }
            }

        }
    }
    
    func callToGetAPI( _ strUrl : String,parameters:Parameters, completion:@escaping (_ result: AnyObject, _ error: AnyObject)->Void)
    {
        
        print("111:\(strUrl)")
        print("111 parameters:\(parameters)")
        General.showActivityIndicator()
        
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            APP_DELEGATE.showAlert(strMessage: "Internet Connection not Available!")
            return
        }else{
            print("Internet Connection Available!")
            
        }
        
        
        //Request navigation
        //if APP_DELEGATE.API_Name == load_dashboard{
            let manager = RequestManager()
            manager.requestDashboardGetUrl(query: strUrl, params: parameters) { (result, error) in
                print("Result:>>>>>\(result)")
                print("Error:>>>>>\(error)")
                if error.isKind(of: NSError.classForCoder()) //Error
                {
                    completion("" as AnyObject, error)
                }
                else
                {
                    //Parse result
                    let manager = ParseManager()
                   if APP_DELEGATE.API_Name == load_dashboard{
                        let arrPosts = manager.parseUserDashboardListFromJson(result: result)
                        completion(arrPosts as AnyObject, "" as AnyObject)
                    }else if APP_DELEGATE.API_Name == user_programs{
                        let arrPosts = manager.parseUserDashboardListFromJson(result: result)
                        completion(arrPosts as AnyObject, "" as AnyObject)
                   }else if APP_DELEGATE.API_Name == program_users{
                    let arrPosts = manager.parseUserDashboardListFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                   }else if APP_DELEGATE.API_Name == help_faq_list{
                    let arrPosts = manager.parseHelpDeskFAQListFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                   }else if APP_DELEGATE.API_Name == user_past_programs{
                    let arrPosts = manager.parsePastProgramListFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                   }else if APP_DELEGATE.API_Name == user_documents{
                    let arrPosts = manager.parsePastProgramListFromJson(result: result)
                    completion(arrPosts as AnyObject, "" as AnyObject)
                    }
                    
                    
                    
                }
                
            }
       /* }else{
            //Other get request
            let manager = RequestManager()
            manager.requestGetUrl(query: strUrl, params: parameters) { (result, error) in
                print("Result:>>>>>\(result)")
                print("Error:>>>>>\(error)")
                if error.isKind(of: NSError.classForCoder()) //Error
                {
                    completion("" as AnyObject, error)
                }
                else
                {
                    //Parse result
                    let manager = ParseManager()
                    if APP_DELEGATE.API_Name == user_badges{
                        let arrPosts = manager.parseUserBadgesListFromJson(result: result)
                        completion(arrPosts as AnyObject, "" as AnyObject)
                    }else if APP_DELEGATE.API_Name == load_dashboard{
                        let arrPosts = manager.parseUserDashboardListFromJson(result: result)
                        completion(arrPosts as AnyObject, "" as AnyObject)
                    }else{
                        let arrPosts = manager.parseLoginFromJson(result: result)
                        completion(arrPosts as AnyObject, "" as AnyObject)
                    }
                }
                
            }
        }*/
        
        
    }
    
    
    func postRequestWithImageUpload(urlString: String, imageData: Data, dict: Parameters, completion:@escaping (_ result: AnyObject, _ error: AnyObject)->Void)  {
        
         General.showActivityIndicator()
     
        
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                //if APP_DELEGATE.isImageSelected{
                    multipartFormData.append(imageData, withName: "photo", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                //}
                
                for  (key, value)  in dict  {
                    
                    //if value is String {
                        multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                        
                        //multipartFormData.append(value.data(using: .utf8)!, withName: key)
                   // }
                    //                    if let val = value {
                    
                }
                
            }, to: urlString,
               headers:[
                "Content-Type": "application/json",
                "Accept":"application/json",
                "Authorization":"Bearer \(Singleton.shared.entUserModel.arrUser_detail?.strApiToken ?? "")"
                ],
               encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseString{response in
                        print(response)}
                    upload.responseJSON { response in
                        print(response.result)
                        if response.result.isSuccess {
                            
                            if let result = response.result.value as? [String: Any] {
                                print("result:\(result)")
                                
                                let json = JSON(result)
                                var arrUser = [UserModel]()
                                
                                if !json.isEmpty
                                {
                                    print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
                                    guard json["status"].boolValue != nil else {
                                        if !json["status"].boolValue{
                                            APP_DELEGATE.errorMessage = (json["message"].stringValue)
                                        }
                                        return
                                        
                                    }
                                    
                                    print("***:\(json)")
                                    let arrData = json //as? [[String:Any]]
                                    // let arrUserProfile = json["user_profile_detail"] //as? [[String:Any]]
                                    // let arrUserOrg = json["user_organization_detail"] //as? [[String:Any]]
                                    
                                    if let objUser = UserModel(JSONString: arrData.description)
                                    {
                                        arrUser.append(objUser)
                                    }
                                   
                                }
                                  completion(arrUser as AnyObject, "" as AnyObject)
                            } else {
                                completion("" as AnyObject, "" as AnyObject)
                            }
                        }
                    }
                case .failure(let error):
                    print(error)
                    
                }
                
            })
            
       
        
    }
    
    
    func postRequestWithUserImage(urlString: String, imageData: Data, dict: Parameters, completion:@escaping (_ result: AnyObject, _ error: AnyObject)->Void)  {
        
        General.showActivityIndicator()
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            multipartFormData.append(imageData, withName: "user_photo", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            
            
            for  (key, value)  in dict as! [String : String] {
                
                //                    if let val = value {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
            
        }, to: urlString,
           
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    if response.result.isSuccess {
                        
                        if let result = response.result.value as? [String: Any] {
                            print("result:\(result)")
                            
                            let json = JSON(result)
                            var arrEvent = [UserModel]()
                            
                            if !json.isEmpty
                            {
                                
                                //guard json["data"].dictionary?["result"]?.array != nil else {
                                if json["error"].dictionary?["message"] != nil{
                                    APP_DELEGATE.errorMessage = (json["error"].dictionary?["message"]?.stringValue)!
                                }
                                // return arrEvent
                                // }
                                
                                print("***:\(json["data"]["result"])")
                                let arrData = json["data"]["result"] //as? [[String:Any]]
                                
                                if let objUser = UserModel(JSONString: arrData.description)
                                {
                                    arrEvent.append(objUser)
                                }
                                
                                
                                completion(arrEvent as AnyObject, "" as AnyObject)
                                
                            }
                        } else {
                            completion("" as AnyObject, "" as AnyObject)
                        }
                    }
                }
            case .failure(let error):
                print(error)
                
            }
            
        })
        
        
        
    }
   /* func signupMe( _ strUrl : String,parameters:Parameters, completion:@escaping (_ result: AnyObject, _ error: AnyObject)->Void)
    {
     
        print("111:\(strUrl)")
        let manager = RequestManager()
        General.showActivityIndicator()
     
        manager.requestUrl(query: strUrl, params: parameters) { (result, error) in
            print("Result:>>>>>\(result)")
            print("Error:>>>>>\(error)")
            if error.isKind(of: NSError.classForCoder()) //Error
            {
                completion("" as AnyObject, error)
            }
            else
            {
                //Parse result
                let manager = ParseManager()
                let arrPosts = manager.parseLoginFromJson(result: result)
                completion(arrPosts as AnyObject, "" as AnyObject)
            }
            
        }
    }*/
}


