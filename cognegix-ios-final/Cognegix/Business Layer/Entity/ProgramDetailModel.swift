//
//  ProgramDetailModel.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 03/07/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import ObjectMapper

class ProgramDetailModel: Mappable {
    
    var bStatus : Bool = false
    var strMessage : String = ""
   
    var objProgramdetails : programDetailObjectModel?
    var userBadges : userProfileBadgeListModel?
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                                           <- map["status"]
        strMessage                                                        <- map["message"]
        objProgramdetails                                                    <- map["program_detail"]
        
        userBadges                                                         <- map["badges"]
    }
}

class programDetailObjectModel: Mappable {
    
    var iID : Int = 0
    var strCode : String = ""
    var strName : String = ""
    var strPhoto : String = ""
    var strPhoto_path : String = ""
    var strDocument_path : String = ""
    var strDescription : String = ""
    var iVersion : Int = 0
    var strStart_Date : String = ""
    var strEnd_Date : String = ""
    var iAllow_Self_Enrol : Int = 0
    var iBadges_Enable : Int = 0
    var iFastest_Activity_Completion_User_Limit : Int = 0
    var iparticipant_distribution_badges : Int = 0
    var icontent_rating_badges : Int = 0
    var customer_party_id : Int = 0
    var tenant_party_id : Int  = 0
    var strUser_role : String = ""
    var iProgram_user_id : Int =  0
    var iRole_user_id : Int = 0
    var iCompletion_percentage : CGFloat = 0.0
    var iRemaining_distribution_badges : Int = 0
    var objLearning_tree : Learning_treeModel?
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        iID                                     <- map["id"]
        strPhoto                                <- map["photo"]
        strPhoto_path                           <- map["photo_path"]
        strDocument_path                        <- map["document_path"]
        strCode                                 <- map["code"]
        strName                                 <- map["name"]
        strDescription                          <- map["description"]
        iVersion                                <- map["version"]
        strStart_Date                           <- map["start_date"]
        strEnd_Date                             <- map["end_date"]
        iAllow_Self_Enrol                       <- map["allow_self_enrol"]
        iBadges_Enable                          <- map["badges_enabled"]
        iFastest_Activity_Completion_User_Limit <- map["fastest_activity_completion_user_limit"]
        iparticipant_distribution_badges        <- map["participant_distribution_badges"]
        icontent_rating_badges                  <- map["content_rating_badges"]
        customer_party_id                       <- map["customer_party_id"]
        tenant_party_id                         <- map["tenant_party_id"]
        strUser_role                            <- map["user_role"]
        iProgram_user_id                        <- map["program_user_id"]
        iRole_user_id                           <- map["role_user_id"]
        objLearning_tree                        <- map["learning_tree"]
        iCompletion_percentage                <- map["completion_percentage"]
        iRemaining_distribution_badges          <- map["iRemaining_distribution_badges"]
    }
}



