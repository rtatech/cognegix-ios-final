//
//  ForumPostsModel.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 05/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import ObjectMapper

class ForumPostsModel: Mappable {
    
    var bStatus : Bool = false
    var iRemaining : Int = 0
    var arrForumMessages : [forumMessagesModel]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                                 <- map["status"]
        iRemaining                                              <- map["remaining"]
        arrForumMessages                                        <- map["thread_list"]
        
    }
}

class forumMessagesModel: Mappable {
    
    var iID : Int = 0
    var iForum_id : Int = 0
    var iProgramUserId : Int = 0
    var strMessage : String = ""
    var strSubject : String = ""
    var strCreatdDate : String = ""
    var strUpdatedDate : String = ""
    var strDeletedDate : String = ""
    
    var strCreatedBy : String = ""
    var iUpdatedBy : Int = 0
    var iDeletedBy : Int = 0
    
    var strFirstName : String = ""
    var strLastName : String = ""
    var iUserId : Int = 0
    var strCreated_by_photo : String = ""
    var strFormatted_created_at : String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        iID                     <- map["id"]
        iForum_id               <- map["forum_id"]
        iProgramUserId        <- map["program_user_id"]
        
        strMessage                <- map["description"]
        strSubject                  <- map["subject"]
        strCreatedBy              <- map["created_by"]
        iUpdatedBy              <- map["updated_by"]
        iDeletedBy              <- map["deleted_by"]
       
        strCreatdDate           <- map["created_at"]
        strUpdatedDate          <- map["updated_at"]
        strDeletedDate          <- map["deleted_at"]
        
        strFirstName            <- map["first_name"]
        strLastName             <- map["last_name"]
        iUserId                 <- map["user_id"]
        strCreated_by_photo     <- map["created_by_photo"]
        strFormatted_created_at <- map["formatted_created_at"]
        
    }
    
    
}
