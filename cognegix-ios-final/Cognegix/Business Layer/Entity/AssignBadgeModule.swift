//
//  AssignBadgeModule.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 28/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import ObjectMapper

class AssignBadgeModule: Mappable {
    
    var bStatus : Bool = false
    var strMessage : String = ""
    var iremaining_distribution_badges : Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                                 <- map["status"]
        strMessage                                              <- map["message"]
        iremaining_distribution_badges                                        <- map["remaining_distribution_badges"]
        
    }
}
