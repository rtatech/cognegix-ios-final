//
//  ProgramBadgeLeadersModelClass.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 13/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import ObjectMapper


class ProgramBadgeLeadersModelClass: Mappable {
    
    var bStatus : Bool = false
    var strMessage : String = ""
    var arrProgramBadgeLeadersList : ProgramBadgeLeadersList?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                                  <- map["status"]
        strMessage                                              <- map["message"]
        arrProgramBadgeLeadersList                                   <- map["badge_leaders_list"]
        
    }
}

class ProgramBadgeLeadersList: Mappable {
    
    var arrLeaderFastestActivityBadge : leaderFastestActivityBadge?
    var arrLeaderContentRatingBadgeCount : leaderContentRatingBadgeCount?
    var arrLeaderQuizCompletionBadgeCount : leaderQuizCompletionBadgeCount?
    var arrLeaderFacilitatorBadgeCount : leaderFacilitatorBadgeCount?
    var arrLeaderParticipantBadgeCount : leaderParticipantBadgeCount?
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        arrLeaderFastestActivityBadge                                 <- map["fastestActivityBadge"]
        arrLeaderContentRatingBadgeCount                              <- map["contentRatingBadgeCount"]
        arrLeaderQuizCompletionBadgeCount                             <- map["quizCompletionBadgeCount"]
        
        arrLeaderFacilitatorBadgeCount                     <- map["facilitatorBadgeCount"]
        arrLeaderParticipantBadgeCount                        <- map["participantBadgeCount"]
        
    }
    
    
}

class leaderFastestActivityBadge: Mappable {
    var strName : String = ""
    var iCount : Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        strName                                 <- map["name"]
        iCount                                    <- map["count"]
    }
}


class leaderContentRatingBadgeCount: Mappable {
    var strName : String = ""
    var iCount : Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        strName                                 <- map["name"]
        iCount                                    <- map["count"]
    }
}



class leaderQuizCompletionBadgeCount: Mappable {
    var strName : String = ""
    var iCount : Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        strName                                 <- map["name"]
        iCount                                    <- map["count"]
    }
}



class leaderFacilitatorBadgeCount: Mappable {
    var strName : String = ""
    var iCount : Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        strName                                 <- map["name"]
        iCount                                    <- map["count"]
    }
}




class leaderParticipantBadgeCount: Mappable {
    var strName : String = ""
    var iCount : Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        strName                                 <- map["name"]
        iCount                                    <- map["count"]
    }
}
