//
//  EventModel.swift

import UIKit
import ObjectMapper


class EventModel: Mappable {
    
    var event_id : String?
    var event_name : String?
    var event_speciality : String?
    var event_image : String?
    var event_video : String?
    var event_date : String?
    var event_time : String?
    var dine_in_seats : String?
    var dine_in_price : String?
    var take_away_seats : String?
    var take_away_price : String?
    var type_of_meal : String?
    var spice_meter : String?
    var special_note : String?
    var short_address : String?
    var detail_address : String?
    var post_code : String?
    var latitude : String?
    var longitude : String?
    var user_ID : String?
    var created_date : String?
    var distance : String?
    var rating : Int?
    var menu_records : Int?
    var host_name : String?
    var already_rate_flag : String?
  //  var menu_content = [MenuModel]()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        event_id <- map["event_id"]
        event_name <- map["event_name"]
        event_speciality <- map["event_speciality"]
        event_image <- map["event_image"]
        event_video <- map["event_video"]
        event_date <- map["event_date"]
        event_time <- map["event_time"]
        dine_in_seats <- map["dine_in_seats"]
        dine_in_price <- map["dine_in_price"]
        take_away_seats <- map["take_away_seats"]
        take_away_price <- map["take_away_price"]
        type_of_meal <- map["type_of_meal"]
        spice_meter <- map["spice_meter"]
        special_note <- map["special_note"]
        short_address <- map["short_address"]
        detail_address <- map["detail_address"]
        post_code <- map["post_code"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        user_ID <- map["user_ID"]
        created_date <- map["created_date"]
        distance <- map["distance"]
        rating <- map["rating"]
        menu_records <- map["menu_records"]
       // menu_content <- map["menu_content"]
        host_name <- map["host_name"]
        already_rate_flag <- map["already_rate_flag"]
    }
}
