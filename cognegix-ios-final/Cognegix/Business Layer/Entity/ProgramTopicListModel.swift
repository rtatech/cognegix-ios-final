//
//  ProgramTopicListModel.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 10/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import ObjectMapper

class ProgramTopicListModel: Mappable {
    
    var bStatus : Bool = false
    var strMessage : String = ""
    var arrPerson_to_ask : [person_to_askModel]?
    var arrProgram_topics : [program_topicsModel]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                                 <- map["status"]
        strMessage                                              <- map["message"]
        arrPerson_to_ask                                   <- map["person_to_ask"]
        arrProgram_topics                                  <- map["program_topics"]
        
    }
}


class person_to_askModel: Mappable {
    
    var iID : Int = 0
    var strName : String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        iID                                                 <- map["id"]
        strName                                              <- map["name"]
    }
}


class program_topicsModel: Mappable {
    
    var iTopic_id : Int = 0
    var strTopic_name : String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        iTopic_id                                                 <- map["topic_id"]
        strTopic_name                                              <- map["topic_name"]
       
        
    }
}

