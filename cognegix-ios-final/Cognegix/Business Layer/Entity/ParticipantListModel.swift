//
//  ParticipantListModel.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 20/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import ObjectMapper

class ParticipantListModel: Mappable {
    
    var bStatus : Bool = false
    var strMessage : String = ""
    var arrUserList : [userListModel]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                                 <- map["status"]
        strMessage                                              <- map["message"]
        arrUserList                                        <- map["user_list"]
        
    }
}

class userListModel: Mappable {
    var  iId                    : Int = 0
    var  strFirst_name          : String = ""
    var  strLast_nam            : String = ""
    var  strEmail               : String = ""
    var  strPersonal_email      : String = ""
    var  strGender              : String = ""
    var  iEmployee_id           : Int = 0
    var  strDepartment          : String = ""
    var  strDesignation         : String = ""
    var  strLinkedin_profile    : String = ""
    var  strFacebook_id         : String = ""
    var  strTwitter_handle      : String = ""
    var  strGoogle_profile      : String = ""
    var  iParty_id              : Int = 0
    var  iTenant_party_id       : Int = 0
    var  iCustomer_party_id     : Int = 0
    var  strPhoto               : String = ""
    var  strWork_phone_number   : String = ""
    var  strHome_phone_number   : String = ""
    var  iIs_private            : Int = 0
    var  iUser_id               : Int = 0
    var  strRole_name           : String = ""
    var strOrganization_name    : String = ""
    var arrProgramBadgeLeadersList : ParticipantBadgeCountList?
    var iRemaining_distribution_badges : Int = 0
   
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        iId                          <- map["id"]
        strFirst_name                <- map["first_name"]
        strLast_nam                  <- map["last_name"]
        strEmail                     <- map["email"]
        strPersonal_email            <- map["personal_email"]
        strGender                    <- map["gender"]
        iEmployee_id                 <- map["employee_id"]
        strDepartment                <- map["department"]
        strDesignation               <- map["designation"]
        strLinkedin_profile         <- map["linkedin_profile"]
        strFacebook_id              <- map["facebook_id"]
        strTwitter_handle           <- map["twitter_handle"]
        strGoogle_profile            <- map["google_profile"]
        iParty_id                   <- map["party_id"]
        iTenant_party_id            <- map["tenant_party_id"]
        iCustomer_party_id          <- map["customer_party_id"]
        strPhoto                    <- map["photo"]
        strWork_phone_number        <- map["work_phone_number"]
        strHome_phone_number        <- map["home_phone_number"]
        iIs_private                 <- map["is_private"]
        iUser_id                <- map["user_id"]
        strRole_name            <- map["role_name"]
        arrProgramBadgeLeadersList <- map["badges"]
        strOrganization_name        <- map["organization_name"]
        iRemaining_distribution_badges  <- map["remaining_distribution_badges"]
        
       
        
        
    }
    
    
}


class ParticipantBadgeCountList: Mappable {
    
    var iParticipantFastestActivityBadge : Int = 0
    var iParticipantContentRatingBadgeCount : Int = 0
    var iParticipantQuizCompletionBadgeCount : Int = 0
    var iParticipantFacilitatorBadgeCount : Int = 0
    var iParticipantParticipantBadgeCount : Int = 0
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        iParticipantFastestActivityBadge                                 <- map["fastestActivityBadge"]
        iParticipantContentRatingBadgeCount                              <- map["contentRatingBadgeCount"]
        iParticipantQuizCompletionBadgeCount                             <- map["quizCompletionBadgeCount"]
        
        iParticipantFacilitatorBadgeCount                     <- map["facilitatorBadgeCount"]
        iParticipantParticipantBadgeCount                        <- map["participantBadgeCount"]
        
    }
    
    
}
