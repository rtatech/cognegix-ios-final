//
//  LearningTreeSearchModel.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 25/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import ObjectMapper

class LearningTreeSearchModel: Mappable {
    
    var bStatus : Bool = false
    var strMessage : String = ""
    var arrLearningTreeProgram : [ProgramLearningTreeListModel]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                                     <- map["status"]
        strMessage                                                  <- map["message"]
        arrLearningTreeProgram                                      <- map["search_result"]
        
    }
}

class ProgramLearningTreeListModel: Mappable {
    
    var strProgram_name : String = ""
    var strTopic_name : String = ""
    var strLaunch_url : String = ""
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        strProgram_name                     <- map["program_name"]
        strTopic_name         <- map["topic_name"]
        strLaunch_url               <- map["launch_url"]
    }
    
    
}
