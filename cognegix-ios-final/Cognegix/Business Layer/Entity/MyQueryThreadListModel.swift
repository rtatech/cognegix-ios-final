//
//  MyQueryThreadListModel.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 06/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import ObjectMapper


class MyQueryThreadListModel: Mappable {
    
    var bStatus : Bool = false
    var strMessage : String = ""
    var arrQueries : [queriesListModel]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                           <- map["status"]
        strMessage                                        <- map["message"]
        arrQueries                                        <- map["queries"]
        
    }
}

class queriesListModel: Mappable {
    
    var iQueryThreadId : Int = 0
    var iSenderUserId : Int = 0
    var strSenderName : String = ""
    var iReceiverUserId : Int = 0
    var strReceiverName : String = ""
    var iProgramId : Int = 0
    var strProgramName : String = ""
    var strTopicName : String = ""
    var iTopicId : Int = 0
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        iQueryThreadId              <- map["query_thread_id"]
        iSenderUserId               <- map["sender_user_id"]
        strSenderName               <- map["sender_name"]
        iReceiverUserId             <- map["receiver_user_id"]
        strReceiverName             <- map["receiver_name"]
        iProgramId                  <- map["program_id"]
        strProgramName               <- map["program_name"]
        strTopicName                 <- map["topic_name"]
        iTopicId                     <- map["topic_id"]
        
    }
    
    
}
