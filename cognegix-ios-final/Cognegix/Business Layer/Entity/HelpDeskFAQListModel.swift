//
//  HelpDeskFAQListModel.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 22/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import ObjectMapper
class HelpDeskFAQListModel: Mappable {
    
    var bStatus : Bool = false
    var strMessage : String = ""
    var arrFaqs : [FAQsListModel]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                                 <- map["status"]
        strMessage                                              <- map["message"]
        arrFaqs                                                 <- map["faqs"]
        
    }
}

class FAQsListModel: Mappable {
    
    var strTitle : String = ""
    var strDescription : String = ""
    var bIsExpand : Bool = false
    var dHeight : Double  = 0.0
     var dTitleHeight : Double  = 0.0
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        strTitle                     <- map["title"]
        strDescription         <- map["description"]
        bIsExpand               <- map["expand"]
        dHeight                 <- map["height"]
        dTitleHeight  <- map["titleHeight"]
    }
    
    
}
