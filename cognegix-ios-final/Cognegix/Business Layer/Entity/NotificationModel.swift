//
//  NotificationModel.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 04/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import ObjectMapper


class NotificationModel: Mappable {
    
    var bStatus : Bool = false
    var strMessage : String = ""
    var arrNotifications : [notificationsListModel]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                                 <- map["status"]
        strMessage                                              <- map["message"]
        arrNotifications                                        <- map["notifications"]
       
    }
}

class notificationsListModel: Mappable {
    var iID : Int = 0
    var strNotification : String = ""
    var iTenant_party_id : Int = 0
    var iPartyID : Int = 0
    var iIs_read : Int = 0
    var strCreatdDate : String = ""
    var strUpdatedDate : String = ""
    var strDeletedDate : String = ""
    var strFormatted_created_at : String = ""
    var iCreatedBy : Int = 0
    var iUpdatedBy : Int = 0
    var iDeletedBy : Int = 0
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        iID                     <- map["id"]
        strNotification         <- map["notification"]
        iTenant_party_id        <- map["tenant_party_id"]
        iPartyID                <- map["party_id"]
        iIs_read                <- map["is_read"]
        iCreatedBy              <- map["created_by"]
        iUpdatedBy              <- map["updated_by"]
        iDeletedBy              <- map["deleted_by"]
        strCreatdDate           <- map["created_at"]
        strUpdatedDate          <- map["updated_at"]
        strDeletedDate          <- map["deleted_at"]
        strFormatted_created_at <- map["formatted_created_at"]
    }
    

}
