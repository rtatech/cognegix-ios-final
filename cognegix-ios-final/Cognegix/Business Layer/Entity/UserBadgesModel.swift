//
//  UserBadgesModel.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 07/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import ObjectMapper


class UserProgramBadgesModelClass: Mappable {
    
    var bStatus : Bool = false
    var strMessage : String = ""
    var arrUserBadgesModel : userProgramBadgesListModel?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                              <- map["status"]
        strMessage                                           <- map["message"]
        arrUserBadgesModel                                   <- map["user_badges_list"]
        
    }
}

class userProgramBadgesListModel: Mappable {
    
    
    var iProgram_id : Int = 0
    var iUser_id : Int = 0
    var iID : Int = 0
    var iMax_distribution_badges : Int = 0
    var iContent_rated_count : Int = 0
    var iCurrent_distribution_badges : Int = 0
    var iFastestActivityBadge :   Int = 0
    var iContentRatingBadgeCount :   Int = 0
    var iQuizCompletionBadgeCount :   Int = 0
    var iFacilitatorBadgeCount:   Int = 0
    var iParticipantBadgeCount:   Int = 0
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        iProgram_id                                 <- map["program_id"]
        iUser_id                                    <- map["user_id"]
        iID                                             <- map["id"]
        
        iMax_distribution_badges                     <- map["max_distribution_badges"]
        iContent_rated_count                        <- map["content_rated_count"]
        iCurrent_distribution_badges                <- map["current_distribution_badges"]
        
        
        iFastestActivityBadge                           <- map["fastest_activity_badge_count"]
        iContentRatingBadgeCount                       <- map["content_rating_badge_count"]
        iQuizCompletionBadgeCount                      <- map["quiz_completion_badge_count"]
        iFacilitatorBadgeCount                         <- map["facilitator_badge_count"]
        iParticipantBadgeCount                         <- map["participant_badge_count"]
        
    }
    
    
}

