//
//  DashboardModel.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 09/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import ObjectMapper

class DashboardModel: Mappable {
    
    var bStatus : Bool = false
    var strMessage : String = ""
    var iUnread_notification_count : Int = 0
    var arrUpcoming_activitiesList : [UpcomingActivitiesModel]?
    var arrProgramList : [programListModel]?
    var arrBoardMessagesList : [BoardMessagesListModel]?
    var arrCriticalAlertsList : [String]?
    var userBadges : userProfileBadgeListModel?
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                                           <- map["status"]
        strMessage                                                        <- map["message"]
        iUnread_notification_count                                        <- map["unread_notification_count"]
        arrUpcoming_activitiesList                                        <- map["upcoming_activities"]
        arrProgramList                                                    <- map["program_list"]
        arrBoardMessagesList                                              <- map["board_messages"]
        arrCriticalAlertsList                                             <- map["critical_alerts"]
        userBadges                                                         <- map["badges"]
    }
}

class userProfileBadgeListModel: Mappable {
    
    var iContentRatingBadgeCount : String = "0"
    var iFacilitatorBadgeCount : String = "0"
    var iFastestActivityBadge : String = "0"
    var iParticipantBadgeCount : String = "0"
    var iQuizCompletionBadgeCount : String = "0"
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        iContentRatingBadgeCount                                     <- map["contentRatingBadgeCount"]
        iFacilitatorBadgeCount                                 <- map["facilitatorBadgeCount"]
        iFastestActivityBadge                                 <- map["fastestActivityBadge"]
        iParticipantBadgeCount                          <- map["participantBadgeCount"]
        iQuizCompletionBadgeCount                                <- map["quizCompletionBadgeCount"]
    }
}
class programListModel: Mappable {
  
    var iID : Int = 0
    var strCode : String = ""
    var strName : String = ""
    var strPhoto : String = ""
    var strPhoto_path : String = ""
    var strDocument_path : String = ""
    var strDescription : String = ""
    var iVersion : Int = 0
    var strStart_Date : String = ""
    var strEnd_Date : String = ""
    var iAllow_Self_Enrol : Int = 0
    var iBadges_Enable : Int = 0
    var iFastest_Activity_Completion_User_Limit : Int = 0
    var iparticipant_distribution_badges : Int = 0
    var icontent_rating_badges : Int = 0
    var customer_party_id : Int = 0
    var tenant_party_id : Int  = 0
    var strUser_role : String = ""
    var iProgram_user_id : Int =  0
    var iRole_user_id : Int = 0
    var iCompletion_percentage : CGFloat = 0.0
    var objLearning_tree : Learning_treeModel?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        iID                                     <- map["id"]
        strPhoto                                <- map["photo"]
        strPhoto_path                           <- map["photo_path"]
        strDocument_path                        <- map["document_path"]
        strCode                                 <- map["code"]
        strName                                 <- map["name"]
        strDescription                          <- map["description"]
        iVersion                                <- map["version"]
        strStart_Date                           <- map["start_date"]
        strEnd_Date                             <- map["end_date"]
        iAllow_Self_Enrol                       <- map["allow_self_enrol"]
        iBadges_Enable                          <- map["badges_enabled"]
        iFastest_Activity_Completion_User_Limit <- map["fastest_activity_completion_user_limit"]
        iparticipant_distribution_badges        <- map["participant_distribution_badges"]
        icontent_rating_badges                  <- map["content_rating_badges"]
        customer_party_id                       <- map["customer_party_id"]
        tenant_party_id                         <- map["tenant_party_id"]
        strUser_role                            <- map["user_role"]
        iProgram_user_id                        <- map["program_user_id"]
        iRole_user_id                           <- map["role_user_id"]
        objLearning_tree                        <- map["learning_tree"]
        iCompletion_percentage                <- map["completion_percentage"]
       
    }
}

class Learning_treeModel : Mappable{
    var strProgramDirectorVideoUrl : String = ""
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        strProgramDirectorVideoUrl <- map["programDirectorVideoUrl"]
    }
}
class BoardMessagesListModel : Mappable{
    var iId : Int = 0
    var iCustomer_party_ids : Int = 0
    var strStart_date : String = ""
    var strMessage_from : String = ""
    var strMessage_subject : String = ""
    var strEnd_date : String = ""
    var strMessage_body : String = ""
    var strProgram_ids : String = ""
    var iIs_active : Int = 0
    var iCustomer_party_id : Int = 0
    var strMessage_for : String = ""
    var iRole_id : Int = 0
    var iTenant_party_id : Int = 0
    var strName : String = ""
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
         iId                                                        <- map["id"]
         iCustomer_party_ids                                        <- map["customer_party_ids"]
         strStart_date                                              <- map["start_date"]
         strMessage_from                                            <- map["message_from"]
         strMessage_subject                                         <- map["message_subject"]
         strEnd_date                                                <- map["end_date"]
         strMessage_body                                            <- map["message_body"]
         strProgram_ids                                             <- map["program_ids"]
         iIs_active                                                 <- map["is_active"]
         iCustomer_party_id                                         <- map["customer_party_id"]
         strMessage_for                                             <- map["message_for"]
        iRole_id                                                    <- map["role_id"]
         iTenant_party_id                                           <- map["tenant_party_id"]
         strName                                                    <- map["name"]
    }
    
}

class UpcomingActivitiesModel : Mappable{
    
    
    var strDay : String = ""
    var strDate : String = ""
    var strMonth : String = ""
    var strFull_date : String = ""
    var arrActivities : [UpcomingActivitiesDetailActivityModel]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        strDay                                         <- map["day"]
        strDate                                        <- map["date"]
        strMonth                                       <- map["month"]
        strFull_date                                   <- map["full_date"]
        arrActivities                                  <- map["activities"]
    }
    
}

class UpcomingActivitiesDetailActivityModel : Mappable{
    
    var strTopic_name : String = ""
    var iTopic_id : Int = 0
    var strDate : String = ""
    var strLaunch_url : String = ""
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        strTopic_name                                        <- map["topic_name"]
        iTopic_id                                        <- map["topic_id"]
        strDate                                        <- map["date"]
        strLaunch_url                                   <- map["launch_url"]
    }
    
}

class CriticalAlertsListModel : Mappable{
  //  var arrTopics : [DashboardTopicsModel]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
       // arrTopics                                        <- map[""]
        
    }
    
}

