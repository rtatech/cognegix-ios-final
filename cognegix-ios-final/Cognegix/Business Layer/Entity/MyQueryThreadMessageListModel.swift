//
//  MyQueryThreadMessageListModel.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 06/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import ObjectMapper

class MyQueryThreadMessageListModel: Mappable {
    
    var bStatus : Bool = false
    var arrQueryMessage : [queryMessagesModel]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                           <- map["status"]
        arrQueryMessage                                        <- map["query_messages"]
        
    }
}

class queryMessagesModel: Mappable {
  
    
    
    var iId : Int = 0
    var iMyQueryId : Int = 0
    var strMessage : String = ""
    var iIsFromSender : Int = 0
    var iIsRead : Int = 0
    var strCreatedAt : String = ""
    var strFormatted_created_at : String = ""
    var strSender_name : String = ""
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        iId                         <- map["id"]
        iMyQueryId                  <- map["my_query_id"]
        strMessage                  <- map["message"]
        iIsFromSender               <- map["is_from_sender"]
        iIsRead                     <- map["is_read"]
        strCreatedAt                <- map["created_at"]
       strFormatted_created_at      <- map["formatted_created_at"]
        strSender_name              <- map["sender_name"]
    }
    
    
}
