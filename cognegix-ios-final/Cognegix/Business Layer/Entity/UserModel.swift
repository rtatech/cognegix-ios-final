//
//  UserModel.swift


import UIKit
import ObjectMapper

class UserModel: Mappable {
    
    var bStatus : Bool = false
    var strMessage : String = ""
    var arrUser_organization_detail : user_organization_detail?
    var arrUser_profile_detail : user_profile_detail?
    var arrUser_detail : user_detail?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        bStatus                                                 <- map["status"]
        strMessage                                              <- map["message"]
        
        arrUser_organization_detail                             <- map["user_organization_detail"]
        arrUser_profile_detail                                  <- map["user_profile_detail"]
        arrUser_detail                                          <- map["user_detail"]
        
    }
}

class user_detail: Mappable {
    
    var iID : Int = 0
    var iIsActive : Int = 0
    var iPartyID : Int = 0
    var iCreatedBy : Int = 0
    var iUpdatedBy : Int = 0
    var iDeletedBy : Int = 0
    var strFirstName : String = ""
    var strLastName : String = ""
    var strUsername : String = ""
    var strEmail : String = ""
    var strPersonalEmail : String = ""
    var strApiToken : String = ""
    var strVerificationOTP : String = ""
    var strCreatdDate : String = ""
    var strUpdatedDate : String = ""
    var strDeletedDate : String = ""
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        iID                     <- map["id"]
        iIsActive               <- map["is_active"]
        iPartyID                <- map["party_id"]
        iCreatedBy              <- map["created_by"]
        iUpdatedBy              <- map["updated_by"]
        iDeletedBy              <- map["deleted_by"]
        strFirstName            <- map["first_name"]
        strLastName             <- map["last_name"]
        strUsername             <- map["username"]
        strEmail                <- map["email"]
        strPersonalEmail        <- map["personal_email"]
        strApiToken             <- map["api_token"]
        strVerificationOTP      <- map["verification_otp"]
        strCreatdDate           <- map["created_at"]
        strUpdatedDate          <- map["updated_at"]
        strDeletedDate          <- map["deleted_at"]
        
    }
}

class user_profile_detail: Mappable {
    
    var iID : Int = 0
    var strFirstName : String = ""
    var strLastName : String = ""
    var strEmail : String = ""
    var strPersonalEmail : String = ""
    var strGender : String = ""
    var strEmployeeID : String?
    var strDepartment : String = ""
    var strDesignation : String = ""
    var strLinkedInProfile : String = ""
    var strFBProfile : String = ""
    var strTwitterProfile : String = ""
    var googleProfile : String = ""
    var strPhoto : String = ""
    var strOriginalPhoto : String = ""
    var iPartyID : Int = 0
    var iTenantPartyId : Int = 0
    var iCustomerPartyID : Int = 0
    var strWork_phone_number : String = ""
    var strHome_phone_number : String = ""
    var dProfile_completion_percentage : Double = 0.0
    var iUser_id : Int = 0
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        iID                         <- map["id"]
        strFirstName                <- map["first_name"]
        strLastName                 <- map["last_name"]
        strEmail                    <- map["email"]
        strPersonalEmail            <- map["personal_email"]
        strGender                   <- map["gender"]
        strEmployeeID               <- map["employee_id"]
        strDepartment               <- map["department"]
        strDesignation              <- map["designation"]
        strLinkedInProfile          <- map["linkedin_profile"]
        strFBProfile                <- map["facebook_id"]
        strTwitterProfile           <- map["twitter_handle"]
        googleProfile               <- map["google_profile"]
        strPhoto                    <- map["photo"]
        strOriginalPhoto            <- map["original_photo"]
        iPartyID                    <- map["party_id"]
        iTenantPartyId              <- map["tenant_party_id"]
        iCustomerPartyID            <- map["customer_party_id"]
        strWork_phone_number        <- map["work_phone_number"]
        strHome_phone_number        <- map["home_phone_number"]
        dProfile_completion_percentage <- map["profile_completion_percentage"]
        iUser_id                 <- map["user_id"]
    }
}


class user_organization_detail: Mappable {
    
    
    var iID : Int = 0
    var strName : String = ""
    var strOrganizationCode : String = ""
    var strWebsiteURL : String = ""
    var strLogo : String = ""
    var strOriginalPhoto : String = ""
    var iParyID : Int = 0
    var iTenantPartyID : Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        iID                 <- map["id"]
        strName             <- map["name"]
        strOrganizationCode <- map["organization_code"]
        strWebsiteURL       <- map["website_url"]
        strLogo             <- map["logo"]
        strOriginalPhoto    <- map["original_logo"]
        iParyID             <- map["party_id"]
        iTenantPartyID      <- map["tenant_party_id"]
        
    }
}

