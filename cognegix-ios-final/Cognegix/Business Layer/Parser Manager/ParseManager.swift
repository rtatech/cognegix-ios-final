//
//  ParseManager.swift

import Foundation
import SwiftyJSON
import ObjectMapper

class ParseManager: NSObject
{
    /**
     This parse post from the response

     - Parameter result: get posts from json response

     - Returns: Array of Post entity
     */
    func parseLoginFromJson(result : AnyObject) -> [UserModel]
    {
        let json = JSON(result)
       
        var arrUser = [UserModel]()
       
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
           /* guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrUser
            }*/

                print("***:\(json)")
                let arrData = json //as? [[String:Any]]
               // let arrUserProfile = json["user_profile_detail"] //as? [[String:Any]]
               // let arrUserOrg = json["user_organization_detail"] //as? [[String:Any]]
            
                if let objUser = UserModel(JSONString: arrData.description)
                {
                    arrUser.append(objUser)
                }
            
                /*if let objUser = user_profile_detail(JSONString: arrUserProfile.description)
                {
                    arrUser_profile_detail.append(objUser)
                }
            
                if let objUser = user_organization_detail(JSONString: arrUserOrg.description)
                {
                    arrUser_organization_detail.append(objUser)
                }*/
        }
        return (arrUser) //[arrUser,arrUser_profile_detail,arrUser_organization_detail]
    }
    
    func parseNotificationListFromJson(result : AnyObject) -> [NotificationModel]
    {
        let json = JSON(result)
        var arrNotification = [NotificationModel]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrNotification
            }
            
            print("***:\(json)")
            let arrData = json
            if let objNotification = NotificationModel(JSONString: arrData.description)
            {
                arrNotification.append(objNotification)
            }
            
        }
        
        return arrNotification
    }
    
    
    func parseBadgesOfUserByProgramFromJson(result : AnyObject) -> [NotificationModel]
    {
        let json = JSON(result)
        var arrNotification = [NotificationModel]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrNotification
            }
            
            print("***:\(json)")
            let arrData = json
            if let objNotification = NotificationModel(JSONString: arrData.description)
            {
                arrNotification.append(objNotification)
            }
            
        }
        
        return arrNotification
    }
    
    func parseProgramTopicsListFromJson(result : AnyObject) -> [ProgramTopicListModel]
    {
        let json = JSON(result)
        var arrNotification = [ProgramTopicListModel]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrNotification
            }
            
            print("***:\(json)")
            let arrData = json
            if let objNotification = ProgramTopicListModel(JSONString: arrData.description)
            {
                arrNotification.append(objNotification)
            }
            
        }
        
        return arrNotification
    }
    
    
    func parseUserBadgesListFromJson(result : AnyObject) -> [UserProgramBadgesModelClass]
    {
        let json = JSON(result)
        var arrBadge = [UserProgramBadgesModelClass]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrBadge
            }
            
            print("***:\(json)")
            let arrData = json
            if let objBadge = UserProgramBadgesModelClass(JSONString: arrData.description)
            {
                arrBadge.append(objBadge)
            }
        }
        return arrBadge
    }
    
    
    func parseProgramBadgesLeaderFromJson(result : AnyObject) -> [ProgramBadgeLeadersModelClass]
    {
        let json = JSON(result)
        var arrBadge = [ProgramBadgeLeadersModelClass]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrBadge
            }
            
            print("***:\(json)")
            let arrData = json
            if let objBadge = ProgramBadgeLeadersModelClass(JSONString: arrData.description)
            {
                arrBadge.append(objBadge)
            }
        }
        return arrBadge
    }
    
    func parseParticipantListFromJson(result : AnyObject) -> [ParticipantListModel]
    {
        let json = JSON(result)
        var arrBadge = [ParticipantListModel]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrBadge
            }
            
            print("***:\(json)")
            let arrData = json
            if let objBadge = ParticipantListModel(JSONString: arrData.description)
            {
                arrBadge.append(objBadge)
            }
        }
        return arrBadge
    }
    
    func parseLearningTreeSearchFromJson(result : AnyObject) -> [LearningTreeSearchModel]
    {
        let json = JSON(result)
        var arrBadge = [LearningTreeSearchModel]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrBadge
            }
            
            print("***:\(json)")
            let arrData = json
            if let objBadge = LearningTreeSearchModel(JSONString: arrData.description)
            {
                arrBadge.append(objBadge)
            }
        }
        return arrBadge
    }
    
    
    func parseUserDashboardListFromJson(result : AnyObject) -> [DashboardModel]
    {
        let json = JSON(result)
        var arrBadge = [DashboardModel]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrBadge
            }
            
            print("***:\(json)")
            let arrData = json
            if let objBadge = DashboardModel(JSONString: arrData.description)
            {
                arrBadge.append(objBadge)
            }
        }
        return arrBadge
    }
    
    
    func parseUserProgramDetailsFromJson(result : AnyObject) -> [ProgramDetailModel]
    {
        let json = JSON(result)
        var arrBadge = [ProgramDetailModel]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrBadge
            }
            
            print("***:\(json)")
            let arrData = json
            if let objBadge = ProgramDetailModel(JSONString: arrData.description)
            {
                arrBadge.append(objBadge)
            }
        }
        return arrBadge
    }
    
    
    func parseHelpDeskFAQListFromJson(result : AnyObject) -> [HelpDeskFAQListModel]
    {
        let json = JSON(result)
        var arrBadge = [HelpDeskFAQListModel]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrBadge
            }
            
            print("***:\(json)")
            let arrData = json
            if let objBadge = HelpDeskFAQListModel(JSONString: arrData.description)
            {
                arrBadge.append(objBadge)
            }
        }
        return arrBadge
    }
    
    func parsePastProgramListFromJson(result : AnyObject) -> [DashboardModel]
    {
        let json = JSON(result)
        var arrBadge = [DashboardModel]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrBadge
            }
            
            print("***:\(json)")
            let arrData = json
            if let objBadge = DashboardModel(JSONString: arrData.description)
            {
                arrBadge.append(objBadge)
            }
        }
        return arrBadge
    }
    
    
    
    func parsemyqueryThreadListFromJson(result : AnyObject) -> [MyQueryThreadListModel]
    {
        let json = JSON(result)
        var arrMyQueryThread = [MyQueryThreadListModel]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrMyQueryThread
            }
            
            print("***:\(json)")
            let arrData = json
            if let objMyQueryThread = MyQueryThreadListModel(JSONString: arrData.description)
            {
                arrMyQueryThread.append(objMyQueryThread)
            }
            
        }
        
        return arrMyQueryThread
    }
    
    
    func parsemyqueryThreadMessageListFromJson(result : AnyObject) -> [MyQueryThreadMessageListModel]
    {
        let json = JSON(result)
        var arrMyQueryThreadMessageThread = [MyQueryThreadMessageListModel]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrMyQueryThreadMessageThread
            }
            
            print("***:\(json)")
            let arrData = json
            if let objMyQueryThreadMessage = MyQueryThreadMessageListModel(JSONString: arrData.description)
            {
                arrMyQueryThreadMessageThread.append(objMyQueryThreadMessage)
            }
            
        }
        
        return arrMyQueryThreadMessageThread
    }
    
    
    func parseforumpostsFromJson(result : AnyObject) -> [ForumPostsModel]
    {
        let json = JSON(result)
        var arrForumPost = [ForumPostsModel]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrForumPost
            }
            
            print("***:\(json)")
            let arrData = json
            if let objForumPost = ForumPostsModel(JSONString: arrData.description)
            {
                arrForumPost.append(objForumPost)
            }
            
        }
        
        return arrForumPost
    }
    
    
    func parseforumThreadPostsFromJson(result : AnyObject) -> [ForumThreadPostsModel]
    {
        let json = JSON(result)
        var arrForumPost = [ForumThreadPostsModel]()
        
        if !json.isEmpty
        {
            print("json[status].dictionary:\(String(describing: json["status"].boolValue))")
            guard json["status"].boolValue != nil else {
                if !json["status"].boolValue{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                return arrForumPost
            }
            
            print("***:\(json)")
            let arrData = json
            if let objForumPost = ForumThreadPostsModel(JSONString: arrData.description)
            {
                arrForumPost.append(objForumPost)
            }
            
        }
        
        return arrForumPost
    }
    
    
    
    func parsePagesFromJson(result : AnyObject) -> String
    {
        let json = JSON(result)
        var pageContent = String()
        
        if !json.isEmpty
        {
            guard json["data"].dictionary?["result"]?.dictionary != nil else {
                if json["error"].dictionary?["message"] != nil{
                    APP_DELEGATE.errorMessage = (json["error"].dictionary?["message"]?.stringValue)!
                }
                
                return pageContent
            }
            
            print("***:\(json["data"]["result"])")
            let arrData = json["data"]["result"] //as? [[String:Any]]
            pageContent = arrData["page_content"].stringValue
            print("page_content:\(pageContent)")
        }
        return pageContent
    }

    func parseEventListFromJson(result : AnyObject) -> [EventModel]
    {
        let json = JSON(result)
        var arrEvent = [EventModel]()
        
        if !json.isEmpty
        {

            guard json["data"].dictionary?["result"]?.array != nil else {
                if json["error"].dictionary?["message"] != nil{
                    APP_DELEGATE.errorMessage = (json["error"].dictionary?["message"]?.stringValue)!
                }
                return arrEvent
            }
            
            print("***:\(json["data"]["result"])")
            let arrData = json["data"]["result"].array //as? [[String:Any]]
            for arr in arrData!{
            if let objUser = EventModel(JSONString: arr.description)
            {
                arrEvent.append(objUser)
            }
            }
           
            
        }
        return arrEvent
    }
    
    
    func parseEventFromJson(result : AnyObject) -> [EventModel]
    {
        let json = JSON(result)
        var arrEvent = [EventModel]()
        
        if !json.isEmpty
        {
            guard json["data"].dictionary?["result"]?.dictionary != nil else {
                if json["error"].dictionary?["message"] != nil{
                    APP_DELEGATE.errorMessage = (json["error"].dictionary?["message"]?.stringValue)!
                }
                
                return arrEvent
            }
            
            print("***:\(json["data"]["result"])")
            let arrData = json["data"]["result"] //as? [[String:Any]]
            
            if let objUser = EventModel(JSONString: arrData.description)
            {
                arrEvent.append(objUser)
            }
            
        }
        return arrEvent
    }
    
    func parseBooleanFromJson(result : AnyObject) -> Bool{
        
        let json = JSON(result)
        var outVal = Bool()
        
        if !json.isEmpty
        {
            guard json["status"].boolValue != nil else {
                return false
            }
            print("***:\(json["status"])")
            
            let arrData = json["status"] //as? [[String:Any]]
            
            if json["status"] == true{
                APP_DELEGATE.errorMessage = json["message"].stringValue
                outVal = true
                
            }else{
                outVal = false
                APP_DELEGATE.errorMessage = json["message"].stringValue
            }
            
        }
        
        return outVal
        
    }
    
    
    
    
    func parseAssignBadgeBooleanFromJson(result : AnyObject) -> [AssignBadgeModule]
    {
        let json = JSON(result)
        var arrEvent = [AssignBadgeModule]()
        
        if !json.isEmpty
        {
            guard json["status"].boolValue != nil else {
                if json["message"].stringValue != nil{
                    APP_DELEGATE.errorMessage = (json["message"].stringValue)
                }
                
                return arrEvent
            }
            
            print("***:\(json)")
            let arrData = json //as? [[String:Any]]
            
            if let objUser = AssignBadgeModule(JSONString: arrData.description)
            {
                arrEvent.append(objUser)
            }
            
        }
        return arrEvent
    }
    
    
    
    func parseBooleanMyQueryCheckThreadFromJson(result : AnyObject) -> Bool{
        
        let json = JSON(result)
        var outVal = Bool()
        
        if !json.isEmpty
        {
            guard json["status"].boolValue != nil else {
                return false
            }
            print("***:\(json["status"])")
            
            let arrData = json["status"] //as? [[String:Any]]
            
            if json["status"].bool == true{
                APP_DELEGATE.iQueryThread = json["query_thread_id"].int!
                outVal = true
                
            }else{
                outVal = false
                APP_DELEGATE.errorMessage = json["message"].stringValue
            }
            
        }
        
        return outVal
        
    }
    
    func parseOTPFromJson(result : AnyObject) -> Bool{
        
        let json = JSON(result)
        var outVal = Bool()
        
        if !json.isEmpty
        {
            guard json["data"].dictionary?["result"]?.dictionary != nil else {
                if json["error"].dictionary?["message"] != nil{
                    APP_DELEGATE.errorMessage = (json["error"].dictionary?["message"]?.stringValue)!
                }
                
                return false
            }
            print("***:\(json["data"]["result"])")
            
            let arrData = json["data"]["result"] //as? [[String:Any]]
            
            if arrData["status"].stringValue == "success" || arrData["status"].stringValue == "SUCCESS"{
               // SINGLETON_OBJECT.OTPCode = arrData["otp"].stringValue
                outVal = true
                
            }else{
                outVal = false
                
            }
            
        }
        
        return outVal
        
    }
    
    
    func parseVerificationFromJson(result : AnyObject) -> Bool{
        
        let json = JSON(result)
        var outVal = Bool()
        
        if !json.isEmpty
        {
            guard json["data"].dictionary?["result"]?.dictionary != nil else {
                return false
            }
            print("***:\(json["data"]["result"])")
            
            let arrData = json["data"]["result"] //as? [[String:Any]]
            
            if arrData["status"].stringValue == "success" || arrData["status"].stringValue == "SUCCESS"{
                
                outVal = true
                
            }else{
                outVal = false
                
            }
            
        }
        
        return outVal
        
    }

    
    
}


