//
//  CriticalAlertViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 28/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class CriticalAlertViewController: BaseVC,iCarouselDelegate, iCarouselDataSource {

    
    //TODO: - General
    
    //TODO: - Controls
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var viwOuter: UIView!
    @IBOutlet weak var viwCarousel: iCarousel!
    @IBOutlet weak var viwTitle: UIView!
    @IBOutlet weak var btnCloseOutlet: UIButton!
    
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name(NotificationIdentifierReloaddashboard), object: nil)
        
        
        pageControl.hidesForSinglePage = true
        viwCarousel.isPagingEnabled = true
        viwCarousel.bounces = false
        viwCarousel.delegate = self
        viwCarousel.dataSource = self
        viwCarousel.reloadData()
        
        self.initialise()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func methodOfReceivedNotification(notification: Notification){
        pageControl.numberOfPages = (SINGLETON_OBJECT.entDashboardModel.arrCriticalAlertsList?.count)!//subjectArray.count
        viwCarousel.reloadData()
        print("Data recived")
    }
    
    //TODO: - Custom Function
    func initialise(){
         viwTitle.round(corners: [.topRight, .bottomRight], radius: self.viwTitle.frame.size.height/2)
        
        roundedCornerView(viw: self.viwOuter, clr: lightGrayColor)
    }
    
    //MARK:- Carousel Delegate
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        if SINGLETON_OBJECT.entDashboardModel.bStatus{
            return (SINGLETON_OBJECT.entDashboardModel.arrCriticalAlertsList?.count)!//
        }else{
            return 0
        }
    }
    
    //show the card at the particular index
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let viwCard = (Bundle.main.loadNibNamed("CriticalAlertView", owner: self, options: nil)![0] as? UIView)! as! CriticalAlertView
        viwCard.frame = viwCarousel.frame
        
        viwCard.lblMessage.text = SINGLETON_OBJECT.entDashboardModel.arrCriticalAlertsList?[index]
        
        return viwCard
    }
    //For spacing of two items
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if(carousel == viwCarousel)
        {
            if (option == .spacing) {
                return value * 1.1
            }
            return value
        }
        else
        {
            if (option == .spacing) {
                return value * 1.0
            }
            return value
        }
        
    }
    
    //scrolling started
    func carouselDidScroll(_ carousel: iCarousel) {      
        let index = carousel.currentItemIndex
        pageControl.currentPage = index
        
    }
    
    //on select of specific item
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
    
    //scroll end
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        
    }
    
    
    
    
    //TODO: - UIButton Action

    @IBAction func btnCloseAction(_ sender: UIButton) {
        APP_DELEGATE.hideCriticalAlertView = true
        NotificationCenter.default.post(name: Notification.Name(NotificationIdentifierCloseCriticalAlert), object: nil)
        
    }
    
}
