//
//  HomeViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 28/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import Alamofire

class HomeViewController: BaseVC {

    
    //TODO: - General
    
    
    //TODO: - Contorls
    
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var viwCustom: UIView!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var btnProfileOutlet: UIButton!
    @IBOutlet weak var btnNotificationOutlet: UIButton!
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var menuOutlet: UIButton!
    
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var constHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblCriticalAlertTitle: UILabel!
    @IBOutlet weak var constCriticalAlertHeight: NSLayoutConstraint!
    
    //TODO: - Let's Code
    override func viewDidLoad() {
        super.viewDidLoad()
        self.autolayoutCustomView()
        print("Hello Home viewcontroller")
        //Revealviewcontroller code
        self.navigationController?.isNavigationBarHidden = true
        if self.revealViewController() != nil {
            menuOutlet.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationForCloseAlert(notification:)), name: Notification.Name(NotificationIdentifierCloseCriticalAlert), object: nil)
        
        
        self.initialise()
        self.createBottomView()
        
        self.callAPI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let count = Int(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)
        if count == 0{
            self.lblNotificationCount.isHidden = true
        }        
        self.lblNotificationCount.text = "\(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func methodOfReceivedNotificationForCloseAlert(notification: Notification){
        if APP_DELEGATE.hideCriticalAlertView{
            constCriticalAlertHeight.constant = 0
            self.lblCriticalAlertTitle.isHidden = true
        }
        tblMain.reloadData()
    }
    
    //TODO: - Custom function
    func initialise(){
        circularButton(button: self.btnProfileOutlet)
        circularLabel(label: self.lblNotificationCount)
        self.lblNotificationCount.backgroundColor = UIColor(hexString: NotificationCountBG)
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
      
        if SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto != ""{
            self.btnProfileOutlet.setImage(nil, for: UIControlState.normal)
            self.btnProfileOutlet.sd_setBackgroundImage(with: URL(string:(SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto)!), for: .normal)
        }
        
        
        if APP_DELEGATE.hideCriticalAlertView{
            constCriticalAlertHeight.constant = 0
            self.lblCriticalAlertTitle.isHidden = true
        }
    }
    
    func autolayoutCustomView(){
        print("APP_DELEGATE.screenWidth:\(APP_DELEGATE.screenWidth)")
         if APP_DELEGATE.screenWidth > 414{
            //iPad air 2
           // constTop.constant = -350//(APP_DELEGATE.screenWidth/1.5)
            constHeight.constant = APP_DELEGATE.screenWidth/1.2
          
        }
        self.viwCustom.layoutIfNeeded()
    }
    
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
   
    
    
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }
    
    //This method is use to show bottom button are pressed
    //1001: Home, 1002: Search, 1003: Assistance, 1004: Setting
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            displayHomeScreen()
            break;
        case 1002:
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "idSearchViewController") as! SearchViewController
            self.navigationController?.pushViewController(searchVC, animated: true)
            break;
        case 1003:
            let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idHelpdeskViewController") as! HelpdeskViewController
            helpVC.isMenuNavigation = false
            self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
            displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    
    
    
    
    
    
    //TODO: - API Integration
    func callAPI(){
        
        let parameters : Parameters = ["": ""]
        
        print("dashboard parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = load_dashboard
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToGetAPI("\(BASE_URL)load/dashboard",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [DashboardModel]
                {
                    
                    let UserModelresult = result as? [DashboardModel]
                    
                    if (UserModelresult?.count)! > 0{
                        if let dict = UserModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                 return
                            }
                            SINGLETON_OBJECT.entDashboardModel = dict
                            //print("dict:\(String(describing: dict.arrProgramList![0].strDescription))")
                            //print("arrBoardMessagesList dict:\(String(describing: dict.arrBoardMessagesList![0].strName))")
                            print("SINGLETON_OBJECT.entDashboardModel.userBadges?.iFacilitatorBadgeCountt dict:\(String(describing: SINGLETON_OBJECT.entDashboardModel.userBadges!.iFacilitatorBadgeCount))")
                            
                            print("dict.arrUpcoming_activitiesList:\(String(describing: dict.arrUpcoming_activitiesList!.count))")
                            
                            self.lblNotificationCount.text = "\(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)"
                            
                            NotificationCenter.default.post(name: Notification.Name(NotificationIdentifierReloaddashboard), object: nil)
                            print("Data Send")
                        }
                    }
                }
            }
        })
        }
    }
    
    
}
