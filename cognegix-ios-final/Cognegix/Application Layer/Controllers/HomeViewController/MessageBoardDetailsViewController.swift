//
//  MessageBoardDetailsViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 10/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class MessageBoardDetailsViewController: BaseVC {

    
    //TODO: - General
    var objBoardMessagesList = BoardMessagesListModel()
    //TODO: - Controls
    
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var lblDateTitle: UILabel!
    @IBOutlet weak var lblSenderTitle: UILabel!
    @IBOutlet weak var lblSubjectTitle: UILabel!
    @IBOutlet weak var lblMessageTitle: UILabel!
    
    
    
   
    @IBOutlet weak var viwWeb: UIWebView!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblSender: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.initialization()
        viwWeb.loadHTMLString(objBoardMessagesList.strMessage_body, baseURL: nil)
        self.lblDate.text = objBoardMessagesList.strStart_date
        self.lblSender.text = objBoardMessagesList.strMessage_from
        self.lblSubject.text = objBoardMessagesList.strMessage_subject
        
         //viwWeb.loadHTMLString("<html><body><p>\"Understanding of Negotiation Skills\" virtual classroom is scheduled on 25th March 2018 at 2:00 PM <br />            Lorem Ipsum text Lorem Ipsum text Lorem Ipsum text Lorem Ipsum text<br /> Lorem Ipsum text Lorem Ipsum text Lorem Ipsum text Lorem Ipsum text<br />            Lorem Ipsum text Lorem Ipsum text Lorem Ipsum text Lorem Ipsum text<br /> Lorem Ipsum text Lorem Ipsum text Lorem Ipsum text Lorem Ipsum text<br /></p></body></html>", baseURL: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        circularButton(button: self.btnProfileImage)
        circularLabel(label: self.lblNotificationCount)
        self.lblNotificationCount.backgroundColor = UIColor(hexString: NotificationCountBG)
        
        //Set profile iamge
        if SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto != ""{
            self.btnProfileImage.setImage(nil, for: UIControlState.normal)
            self.btnProfileImage.sd_setBackgroundImage(with: URL(string:(SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto)!), for: .normal)
        }
        let count = Int(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)
        if count == 0{
            self.lblNotificationCount.isHidden = true
        }        
        self.lblNotificationCount.text = "\(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //TODO: - Custom Function
    func initialization(){
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        roundedCornerLabel(lbl: self.lblDateTitle)
        roundedCornerLabel(lbl: self.lblSenderTitle)
        roundedCornerLabel(lbl: self.lblSubjectTitle)
        roundedCornerLabel(lbl: self.lblMessageTitle)
    }
    
    
    //TODO: - UIButton Action
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
