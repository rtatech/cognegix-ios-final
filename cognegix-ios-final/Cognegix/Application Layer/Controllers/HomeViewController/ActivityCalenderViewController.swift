//
//  ActivityCalenderViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 28/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class ActivityCalenderViewController: BaseVC,iCarouselDelegate, iCarouselDataSource {

    //TODO: - General
    var arrActivityCalender = [UpcomingActivitiesModel]()
    //TODO: - Controls
    
    @IBOutlet weak var pageControlNew: UIPageControl!
    @IBOutlet weak var viwCarouselNew: iCarousel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viwDetail: UIView!
    @IBOutlet weak var viwCarousel: iCarousel!
    @IBOutlet weak var viwOuter: UIView!
    
    var iInnerCarouselCount : Int = 0
    var arrUpcomingActivitiesDetailActivity =  [UpcomingActivitiesDetailActivityModel]()
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name(NotificationIdentifierReloaddashboard), object: nil)
        
        
        
        
        
        //Temp Calender function
        var tenDaysfromNow: Date {
            return (Calendar.current as NSCalendar).date(byAdding: .day, value: 10, to: Date(), options: [])!
            
        }
        print(tenDaysfromNow.dayOfWeek()!) // Wednesday
        
        print(tenDaysfromNow)
        
        
        
        
        //Carousel
        
        //pageControl.numberOfPages = 4
        pageControl.hidesForSinglePage = true
        viwCarousel.isPagingEnabled = true
        viwCarousel.bounces = false
        viwCarousel.delegate = self
        viwCarousel.dataSource = self
        viwCarousel.reloadData()
        
        pageControlNew.numberOfPages = iInnerCarouselCount
        pageControlNew.hidesForSinglePage = true
        viwCarouselNew.bounces = false
        viwCarouselNew.delegate = self
        viwCarouselNew.dataSource = self
        viwCarouselNew.reloadData()
        
       roundedCornerView(viw: viwDetail, clr: btnColor)
        
        //Corner radious
        self.viwOuter.layer.cornerRadius = 5
        self.viwOuter.clipsToBounds = true
        self.viwOuter.layer.borderWidth = 1
        self.viwOuter.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @objc func methodOfReceivedNotification(notification: Notification){
        pageControl.numberOfPages = ((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?.count)!/7)//subjectArray.count
        viwCarousel.reloadData()
        print("Data recived")
        if SINGLETON_OBJECT.entDashboardModel.bStatus{
            self.dateSelected(index:0)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Carousel Delegate
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        if carousel == viwCarousel{
            if SINGLETON_OBJECT.entDashboardModel.bStatus{
                return 4
            }else{
                return 0
            }
        
        }else {
            return iInnerCarouselCount
        }
    }
    
    //show the card at the particular index
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        if carousel == viwCarousel{
        let viwCard = (Bundle.main.loadNibNamed("CalenderView", owner: self, options: nil)![0] as? UIView)! as! CalenderView
        viwCard.frame = viwCarousel.frame
        
        viwCard.outerView.layer.cornerRadius = 5
        viwCard.clipsToBounds = true
        print("viewForItemAt index:\(index)")
            
        
        if index == 0{
            viwCard.lblFirstTitle.text =    SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[0].strDay //"Sat"
            viwCard.lblTwoTitle.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[1].strDay //"Sun"
            viwCard.lblThreeTitle.text =    SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[2].strDay //"Mon"
            viwCard.lblFourTitle.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[3].strDay //"Tue"
            viwCard.lblFiveTitle.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[4].strDay //"Wed"
            viwCard.lblSixTitle.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[5].strDay //"Thu"
            viwCard.lblSevenTitle.text =    SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[6].strDay //"Fri"
            
            viwCard.lblFirstDate.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[0].strDate //"1"
            viwCard.lblTwoDate.text =       SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[1].strDate //"2"
            viwCard.lblThreeDate.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[2].strDate //"3"
            viwCard.lblFourDate.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[3].strDate //"4"
            viwCard.lblFiveDate.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[4].strDate //"5"
            viwCard.lblSixDate.text =       SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[5].strDate //"6"
            viwCard.lblSevenDate.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[6].strDate //"7"
            
            viwCard.btnFirstOutlet.tag =   0 //Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[0].strDate)!)!
            viwCard.btnTwoOutlet.tag =     1 //Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[1].strDate)!)! //2
            viwCard.btnThreeOutlet.tag =   2 //Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[2].strDate)!)! //3
            viwCard.btnFourOutlet.tag =    3 //Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[3].strDate)!)! //4
            viwCard.btnFiveOutlet.tag =    4 //Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[4].strDate)!)! //5
            viwCard.btnSixOutlet.tag =     5 //Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[5].strDate)!)! //6
            viwCard.btnSevenOutlet.tag =   6 //Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[6].strDate)!)! //7
            
            //0
            let firstCount = (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![0].arrActivities!.count)
            if  firstCount > 0{
                 viwCard.lblFirstIndicator.isHidden = false
            }else{
                viwCard.lblFirstIndicator.isHidden = true
            }
           
            //1
            let twoCount = (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![1].arrActivities!.count)
            
            if twoCount > 0{
                viwCard.lblTwoIndicator.isHidden = false
            }else{
                viwCard.lblTwoIndicator.isHidden = true
            }
            
            //2
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![2].arrActivities!.count) > 0{
                viwCard.lblThreeIndicator.isHidden = false
            }else{
                viwCard.lblThreeIndicator.isHidden = true
            }
            
            //3
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![3].arrActivities!.count) > 0{
                viwCard.lblFourIndicator.isHidden = false
            }else{
                viwCard.lblFourIndicator.isHidden = true
            }
            
            //4
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![4].arrActivities!.count) > 0{
                viwCard.lblFiveIndicator.isHidden = false
            }else{
                viwCard.lblFiveIndicator.isHidden = true
            }
            
            //5
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![5].arrActivities!.count) > 0{
                viwCard.lblSixIndicator.isHidden = false
            }else{
                viwCard.lblSixIndicator.isHidden = true
            }
            
            //6
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![6].arrActivities!.count) > 0{
                viwCard.lblSevenIndicator.isHidden = false
            }else{
                viwCard.lblSevenIndicator.isHidden = true
            }
           
        }else if index == 1{
            
            viwCard.lblFirstTitle.text =    SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[7].strDay //"Sat"
            viwCard.lblTwoTitle.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[8].strDay //"Sun"
            viwCard.lblThreeTitle.text =    SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[9].strDay //"Mon"
            viwCard.lblFourTitle.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[10].strDay //"Tue"
            viwCard.lblFiveTitle.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[11].strDay //"Wed"
            viwCard.lblSixTitle.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[12].strDay //"Thu"
            viwCard.lblSevenTitle.text =    SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[13].strDay //"Fri"
            
            
            
            viwCard.lblFirstDate.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[7].strDate //"1"
            viwCard.lblTwoDate.text =       SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[8].strDate //"2"
            viwCard.lblThreeDate.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[9].strDate //"3"
            viwCard.lblFourDate.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[10].strDate //"4"
            viwCard.lblFiveDate.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[11].strDate //"5"
            viwCard.lblSixDate.text =       SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[12].strDate //"6"
            viwCard.lblSevenDate.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[13].strDate //"7"
            
            viwCard.btnFirstOutlet.tag =   7 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[7].strDate)!)!
            viwCard.btnTwoOutlet.tag =     8 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[8].strDate)!)! //2
            viwCard.btnThreeOutlet.tag =   9 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[9].strDate)!)! //3
            viwCard.btnFourOutlet.tag =    10  // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[10].strDate)!)! //4
            viwCard.btnFiveOutlet.tag =    11  //Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[11].strDate)!)! //5
            viwCard.btnSixOutlet.tag =     12  //Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[12].strDate)!)! //6
            viwCard.btnSevenOutlet.tag =   13  //Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[13].strDate)!)! //7
            
            
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![7].arrActivities!.count) > 0{
                viwCard.lblFirstIndicator.isHidden = false
            }else{
                viwCard.lblFirstIndicator.isHidden = true
            }
            
            
            //1
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![8].arrActivities!.count) > 0{
                viwCard.lblTwoIndicator.isHidden = false
            }else{
                viwCard.lblTwoIndicator.isHidden = true
            }
            
            //2
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![9].arrActivities!.count) > 0{
                viwCard.lblThreeIndicator.isHidden = false
            }else{
                viwCard.lblThreeIndicator.isHidden = true
            }
            
            //3
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![10].arrActivities!.count) > 0{
                viwCard.lblFourIndicator.isHidden = false
            }else{
                viwCard.lblFourIndicator.isHidden = true
            }
            
            //4
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![11].arrActivities!.count) > 0{
                viwCard.lblFiveIndicator.isHidden = false
            }else{
                viwCard.lblFiveIndicator.isHidden = true
            }
            
            //5
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![12].arrActivities!.count) > 0{
                viwCard.lblSixIndicator.isHidden = false
            }else{
                viwCard.lblSixIndicator.isHidden = true
            }
            
            //6
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![13].arrActivities!.count) > 0{
                viwCard.lblSevenIndicator.isHidden = false
            }else{
                viwCard.lblSevenIndicator.isHidden = true
            }
            
           
        }else if index == 2{
            
            viwCard.lblFirstTitle.text =    SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[14].strDay //"Sat"
            viwCard.lblTwoTitle.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[15].strDay //"Sun"
            viwCard.lblThreeTitle.text =    SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[16].strDay //"Mon"
            viwCard.lblFourTitle.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[17].strDay //"Tue"
            viwCard.lblFiveTitle.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[18].strDay //"Wed"
            viwCard.lblSixTitle.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[19].strDay //"Thu"
            viwCard.lblSevenTitle.text =    SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[20].strDay //"Fri"
            
            
            
            viwCard.lblFirstDate.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[14].strDate //"1"
            viwCard.lblTwoDate.text =       SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[15].strDate //"2"
            viwCard.lblThreeDate.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[16].strDate //"3"
            viwCard.lblFourDate.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[17].strDate //"4"
            viwCard.lblFiveDate.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[18].strDate //"5"
            viwCard.lblSixDate.text =       SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[19].strDate //"6"
            viwCard.lblSevenDate.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[20].strDate //"7"
            
            
            viwCard.btnFirstOutlet.tag =  14 //  Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[14].strDate)!)!
            viwCard.btnTwoOutlet.tag =    15 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[15].strDate)!)! //2
            viwCard.btnThreeOutlet.tag =  16 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[16].strDate)!)! //3
            viwCard.btnFourOutlet.tag =   17 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[17].strDate)!)! //4
            viwCard.btnFiveOutlet.tag =   18 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[18].strDate)!)! //5
            viwCard.btnSixOutlet.tag =    19 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[19].strDate)!)! //6
            viwCard.btnSevenOutlet.tag =  20 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[20].strDate)!)! //7
            
            
            
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![14].arrActivities!.count) > 0{
                viwCard.lblFirstIndicator.isHidden = false
            }else{
                viwCard.lblFirstIndicator.isHidden = true
            }
            
            
            //1
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![15].arrActivities!.count) > 0{
                viwCard.lblTwoIndicator.isHidden = false
            }else{
                viwCard.lblTwoIndicator.isHidden = true
            }
            
            //2
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![16].arrActivities!.count) > 0{
                viwCard.lblThreeIndicator.isHidden = false
            }else{
                viwCard.lblThreeIndicator.isHidden = true
            }
            
            //3
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![17].arrActivities!.count) > 0{
                viwCard.lblFourIndicator.isHidden = false
            }else{
                viwCard.lblFourIndicator.isHidden = true
            }
            
            //4
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![18].arrActivities!.count) > 0{
                viwCard.lblFiveIndicator.isHidden = false
            }else{
                viwCard.lblFiveIndicator.isHidden = true
            }
            
            //5
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![19].arrActivities!.count) > 0{
                viwCard.lblSixIndicator.isHidden = false
            }else{
                viwCard.lblSixIndicator.isHidden = true
            }
            
            //6
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![20].arrActivities!.count) > 0{
                viwCard.lblSevenIndicator.isHidden = false
            }else{
                viwCard.lblSevenIndicator.isHidden = true
            }
            
            
            
        }else if index == 3{
            viwCard.lblFirstTitle.text =    SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[21].strDay //"Sat"
            viwCard.lblTwoTitle.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[22].strDay //"Sun"
            viwCard.lblThreeTitle.text =    SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[23].strDay //"Mon"
            viwCard.lblFourTitle.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[24].strDay //"Tue"
            viwCard.lblFiveTitle.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[25].strDay //"Wed"
            viwCard.lblSixTitle.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[26].strDay //"Thu"
            viwCard.lblSevenTitle.text =    SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[27].strDay //"Fri"
            
            
            
            viwCard.lblFirstDate.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[21].strDate //"1"
            viwCard.lblTwoDate.text =       SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[22].strDate //"2"
            viwCard.lblThreeDate.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[23].strDate //"3"
            viwCard.lblFourDate.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[24].strDate //"4"
            viwCard.lblFiveDate.text =      SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[25].strDate //"5"
            viwCard.lblSixDate.text =       SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[26].strDate //"6"
            viwCard.lblSevenDate.text =     SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[27].strDate //"7"
            
            viwCard.btnFirstOutlet.tag =  21 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[21].strDate)!)!
            viwCard.btnTwoOutlet.tag =    22 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[22].strDate)!)! //2
            viwCard.btnThreeOutlet.tag =  23 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[23].strDate)!)! //3
            viwCard.btnFourOutlet.tag =   24 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[24].strDate)!)! //4
            viwCard.btnFiveOutlet.tag =   25 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[25].strDate)!)! //5
            viwCard.btnSixOutlet.tag =    26 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[26].strDate)!)! //6
            viwCard.btnSevenOutlet.tag =  27 // Int((SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[27].strDate)!)! //7
            
            
            
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![21].arrActivities!.count) > 0{
                viwCard.lblFirstIndicator.isHidden = false
            }else{
                viwCard.lblFirstIndicator.isHidden = true
            }
            
            
            //1
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![22].arrActivities!.count) > 0{
                viwCard.lblTwoIndicator.isHidden = false
            }else{
                viwCard.lblTwoIndicator.isHidden = true
            }
            
            //2
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![23].arrActivities!.count) > 0{
                viwCard.lblThreeIndicator.isHidden = false
            }else{
                viwCard.lblThreeIndicator.isHidden = true
            }
            
            //3
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![24].arrActivities!.count) > 0{
                viwCard.lblFourIndicator.isHidden = false
            }else{
                viwCard.lblFourIndicator.isHidden = true
            }
            
            //4
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![25].arrActivities!.count) > 0{
                viwCard.lblFiveIndicator.isHidden = false
            }else{
                viwCard.lblFiveIndicator.isHidden = true
            }
            
            //5
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![26].arrActivities!.count) > 0{
                viwCard.lblSixIndicator.isHidden = false
            }else{
                viwCard.lblSixIndicator.isHidden = true
            }
            
            //6
            if (SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList![27].arrActivities!.count) > 0{
                viwCard.lblSevenIndicator.isHidden = false
            }else{
                viwCard.lblSevenIndicator.isHidden = true
            }
          
        }
        
        
        viwCard.btnFirstOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
        viwCard.btnTwoOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
        viwCard.btnThreeOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
        viwCard.btnFourOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
        viwCard.btnFiveOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
        viwCard.btnSixOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
        viwCard.btnSevenOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
            
             return viwCard
            
        }else if carousel == viwCarouselNew{
            let viwCard = (Bundle.main.loadNibNamed("CalenderDetailView", owner: self, options: nil)![0] as? UIView)! as! CalenderDetailView
            
            viwCard.frame = viwCarouselNew.frame
            
            viwCard.btnLaunch.layer.cornerRadius = 5
            viwCard.btnLaunch.clipsToBounds = true
            
            let data = self.arrUpcomingActivitiesDetailActivity[index]
            viwCard.lblTitle.text =  data.strTopic_name
            viwCard.btnLaunch.tag = index
            viwCard.btnLaunch.addTarget(self, action: #selector(btnLaunchSelect), for: .touchUpInside)
             
             return viwCard
        }
        
        let viwCard = (Bundle.main.loadNibNamed("CalenderView", owner: self, options: nil)![0] as? UIView)! as! CalenderView
        viwCard.frame = viwCarousel.frame
        return viwCard
        
    }
    //For spacing of two items
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if(carousel == viwCarousel)
        {
            if (option == .spacing) {
                return value * 1.1
            }
            return value
        }
        else
        {
            if (option == .spacing) {
                return value * 1.0
            }
            return value
        }
        
    }
    
    //scrolling started
    func carouselDidScroll(_ carousel: iCarousel) {
        let index = carousel.currentItemIndex
      //  print("index:\(index)")
        if carousel == viwCarousel{
        pageControl.currentPage = index
        }else{
            pageControlNew.currentPage = index
        }
        
    }
    
    //on select of specific item
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
    
    //scroll end
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        
    }
    
    
    func dateSelected(index:Int){
        let data = SINGLETON_OBJECT.entDashboardModel.arrUpcoming_activitiesList?[index]
        self.lblDate.text = data?.strDate
        self.lblMonth.text = data?.strMonth
        iInnerCarouselCount = (data?.arrActivities?.count)!
        pageControlNew.numberOfPages = iInnerCarouselCount
        self.arrUpcomingActivitiesDetailActivity = (data?.arrActivities)!
        viwCarouselNew.reloadData()
    }

    
    //TODO: - UIButton
    @objc func btnLaunchSelect(_ sender: UIButton) {
        let partVC = self.storyboard?.instantiateViewController(withIdentifier: "idLearningTreeViewController") as! LearningTreeViewController
        let data = self.arrUpcomingActivitiesDetailActivity[sender.tag]
        partVC.strLaunchURL = data.strLaunch_url
        partVC.strLearningTreeTitle = "Learning Tree"
        partVC.itHasURL = true
        self.navigationController?.pushViewController(partVC, animated: true)
    }
    
    @objc func btnDateSelect(_ sender: UIButton) {
        print("sender.tag:\(sender.tag)")
        self.dateSelected(index:sender.tag)
    }

}
