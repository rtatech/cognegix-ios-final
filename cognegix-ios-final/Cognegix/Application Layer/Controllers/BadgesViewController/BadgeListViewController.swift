//
//  BadgeListViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 29/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import Alamofire

class BadgeListViewController: BaseVC, iCarouselDelegate, iCarouselDataSource, UITableViewDelegate, UITableViewDataSource{

    
    
    //TODO: - General
    var titleArray : [String] = ["Fastest Completion","Faculty Appreciation","Participant Appreciation","Content Rating","Quiz Completion"]
    var badgeArray : [String] = ["badge1","badge2","badge3","badge4","badge5"]
    var badgeCountArray : [Int] = [0,0,0,0,0]
    
    var completionDateArray : [String] = ["Completion Date: 21 Jan 2018","Completion Date: 21 Jan 2018","Completion Date: 21 Jan 2018","Completion Date: 21 Jan 2018","Completion Date: 21 Jan 2018"]
    
    
   
    var leaderBoardNameArray : [String] = ["","","","",""]
    var leaderBoardBadgeArray : [String] = ["Fastest Completion","Faculty Appreciation","Participant Appreciation","Content Rating","Quiz Completion"]
     var leaderBoardBadgeImageArray : [String] = ["badge1","badge2","badge3","badge4","badge5"]
    var leaderBoardBadgeCountArray : [Int] = [0,0,0,0,0]
    
    var iProgramID : Int = Int()
    var dataSourceVal = [String]()
    var arrProgramList = [programListModel]()
    //TODO: - Controls
    let dropDown = DropDown()
    
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var viwCustom: UIView!
    @IBOutlet weak var viwCarousel: iCarousel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var constLeaderboard: NSLayoutConstraint!
    @IBOutlet weak var viwLeaderboard: UIView!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var lblDDProgram: UILabel!
    @IBOutlet weak var btnSelectProgOutlet: UIButton!
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var btnBackOutlet: UIButton!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialise()
        self.createBottomView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom Function
    func initialise(){
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        circularButton(button: self.btnProfileImage)
        circularLabel(label: self.lblNotificationCount)
        self.lblNotificationCount.backgroundColor = UIColor(hexString: NotificationCountBG)
        
        self.viwCustom.layer.cornerRadius = self.viwCustom.frame.size.height/2
        self.viwCustom.clipsToBounds = true
        
        self.viwLeaderboard.layer.borderColor = UIColor.lightGray.cgColor
        self.viwLeaderboard.layer.borderWidth = 1
        self.viwLeaderboard.layer.cornerRadius = 5
        self.viwLeaderboard.clipsToBounds = true
        
        //iCaro
         pageControl.hidesForSinglePage = true
        viwCarousel.isPagingEnabled = true
        viwCarousel.bounces = false
        viwCarousel.delegate = self
        viwCarousel.dataSource = self
        viwCarousel.reloadData()
        
        self.tblMain.tableFooterView = UIView()
        self.tblMain.dataSource = self
        self.tblMain.delegate = self
        
        self.btnSelectProgOutlet.layer.cornerRadius = 5
        self.btnSelectProgOutlet.clipsToBounds = true
        self.btnSelectProgOutlet.layer.borderColor = UIColor.black.cgColor
        self.btnSelectProgOutlet.layer.borderWidth  = 1
        self.bottomView.backgroundColor = UIColor(hexString: menuBgColor)
        
        viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
        
        //Set profile iamge
        if SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto != ""{
            self.btnProfileImage.setImage(nil, for: UIControlState.normal)
            self.btnProfileImage.sd_setBackgroundImage(with: URL(string:(SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto)!), for: .normal)
        }
        let count = Int(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)
        if count == 0{
            self.lblNotificationCount.isHidden = true
        }
        
        self.lblNotificationCount.text = "\(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)"
        
        
        self.callAPI()
      //
       
        for ind in self.arrProgramList{
            self.dataSourceVal.append(ind.strName)
        }
       
    }
    
    func callToShareSheet(strMessage:String){
        // text to share
        let text = strMessage//"This is some text that I want to share."
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
        
    }
    
    
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    //TODO: - UITableViewDatasource method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! MyDocumentListTableViewCell
        
        //Style
        cell.selectionStyle = .none
        
        cell.viwOuter.layer.cornerRadius = 5
        cell.viwOuter.clipsToBounds = true
        cell.viwOuter.layer.borderWidth = 1
        cell.viwOuter.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.imgBadge.image = UIImage(named: self.badgeArray[indexPath.row])
        circularImageView(imgViw: cell.imgBadge)
        cell.imgBadge.layer.borderColor = UIColor.white.cgColor
        cell.imgBadge.layer.borderWidth = 2
        cell.lblBadgeCount.layer.cornerRadius = cell.lblBadgeCount.frame.size.width / 2
        cell.lblBadgeCount.clipsToBounds = true
        cell.lblBadgeCount.text = "\(self.badgeCountArray[indexPath.row])"
        
        cell.lblTitle.text = self.titleArray[indexPath.row]
        cell.lblDate.isHidden = true
        
        cell.viwCustom.layer.cornerRadius = cell.viwCustom.frame.size.height/2
        cell.viwCustom.clipsToBounds = true
        cell.viwCustom.layoutIfNeeded()
        
        
        cell.btnView.layer.cornerRadius = 5
        cell.btnView.clipsToBounds = true
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(handleShare(_:)), for: .touchUpInside)
        return cell
    }
    
    
    
    
    //MARK:- Carousel Delegate
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return leaderBoardNameArray.count
    }
    
    //show the card at the particular index
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let viwCard = (Bundle.main.loadNibNamed("LeaderBoardView", owner: self, options: nil)![0] as? UIView)! as! LeaderBoardView
        viwCard.frame = viwCarousel.frame
        
        viwCard.imgBadge.layer.cornerRadius = viwCard.imgBadge.frame.size.width/2
        viwCard.imgBadge.clipsToBounds = true
        
        viwCard.imgProfile.layer.cornerRadius = viwCard.imgProfile.frame.size.width/2
        viwCard.imgProfile.clipsToBounds = true
        
        viwCard.lblName.text = self.leaderBoardNameArray[index]
        viwCard.lblTitle.text = self.leaderBoardBadgeArray[index]
        viwCard.lblBadgeCount.text = "\(self.leaderBoardBadgeCountArray[index])"
        viwCard.lblBadgeCount.layer.cornerRadius = viwCard.lblBadgeCount.frame.size.height/2
        viwCard.lblBadgeCount.clipsToBounds = true
        
        
        
        viwCard.imgBadge.image = UIImage(named: self.leaderBoardBadgeImageArray[index])
        viwCard.imgProfile.image = UIImage(named: "profile")
        
        
        return viwCard
    }
    //For spacing of two items
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if(carousel == viwCarousel)
        {
            if (option == .spacing) {
                return value * 1.1
            }
            return value
        }
        else
        {
            if (option == .spacing) {
                return value * 1.0
            }
            return value
        }
        
    }
    
    //scrolling started
    func carouselDidScroll(_ carousel: iCarousel) {
        let index = carousel.currentItemIndex
        print("index:\(index)")
        pageControl.currentPage = index
    }
    
    //on select of specific item
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
    
    //scroll end
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        
    }
    
    
    //TODO: - UIButton Action
    
    //This method is use to show bottom button are pressed
    //1001: Home, 1002: Search, 1003: Assistance, 1004: Setting
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            displayHomeScreen()
            break;
        case 1002:
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "idSearchViewController") as! SearchViewController
            self.navigationController?.pushViewController(searchVC, animated: true)
            break;
        case 1003:
            let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idHelpdeskViewController") as! HelpdeskViewController
            helpVC.isMenuNavigation = false
            self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
            displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }
    
    
    @objc func handleShare(_ sender: UIButton){
        self.callToShareSheet(strMessage: "I got \(self.badgeCountArray[sender.tag]) \(self.titleArray[sender.tag]) badges in Cognegix")
    }

    
    @IBAction func btnSelectProgClick(_ sender: UIButton) {
        dropDown.anchorView = lblDDProgram
        
        dropDown.dataSource = self.dataSourceVal//["Understanding of Negotiation Skills","Understanding negotiation situation","Grievance handling","Performance Appraisal handling"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btnSelectProgOutlet.setTitle(item, for: .normal)
            print("Selected item: \(item) at index: \(index)")
            self.iProgramID = self.arrProgramList[index].iID
            self.callAPI()
            
        }
        dropDown.show()
    }
    @IBAction func btnBackClick(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    //TODO: - API Integration
    func callAPI(){
        
        let parameters : Parameters = ["programId": "\(iProgramID)",
            "userId":"\(SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.iID ?? 0)"]
        
        print("Badge list parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = user_program_badges
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)user/program/badges",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [UserProgramBadgesModelClass]
                {
                    
                    let UserModelresult = result as? [UserProgramBadgesModelClass]
                    
                    if (UserModelresult?.count)! > 0{
                        if let dict = UserModelresult?[0] {
                            if !dict.bStatus {
                                for index in 0...self.badgeCountArray.count-1{
                                    self.badgeCountArray[index] = 0
                                }
                                self.tblMain.reloadData()
                                self.showAlert(strMSG: dict.strMessage)
                                return
                            }
                             self.badgeCountArray[0] = dict.arrUserBadgesModel!.iFastestActivityBadge
                             self.badgeCountArray[1] = dict.arrUserBadgesModel!.iFacilitatorBadgeCount
                             self.badgeCountArray[2] = dict.arrUserBadgesModel!.iParticipantBadgeCount
                             self.badgeCountArray[3] = dict.arrUserBadgesModel!.iContentRatingBadgeCount
                             self.badgeCountArray[4] = dict.arrUserBadgesModel!.iQuizCompletionBadgeCount
                            self.tblMain.reloadData()
                            self.callToLeaderboardAPI()
                            print("dict:\(String(describing: dict.arrUserBadgesModel!.iFacilitatorBadgeCount))")
                        }
                    }
                }
            }
        })
        }
    }
    
    
    
    func callToLeaderboardAPI(){
        
        let parameters : Parameters = ["programId": "\(iProgramID)"]
        
        print("Badge list parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = program_badge_leaders
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)program/badge/leaders",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [ProgramBadgeLeadersModelClass]
                {
                    
                    let UserModelresult = result as? [ProgramBadgeLeadersModelClass]
                    
                    if (UserModelresult?.count)! > 0{
                        if let dict = UserModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                return
                            }
                            self.leaderBoardNameArray[0] = (dict.arrProgramBadgeLeadersList!.arrLeaderFastestActivityBadge?.strName)!
                            self.leaderBoardBadgeCountArray[0] = (dict.arrProgramBadgeLeadersList!.arrLeaderFastestActivityBadge?.iCount)!
                            self.leaderBoardNameArray[1] = (dict.arrProgramBadgeLeadersList!.arrLeaderFacilitatorBadgeCount?.strName)!
                            self.leaderBoardBadgeCountArray[1] = (dict.arrProgramBadgeLeadersList!.arrLeaderFacilitatorBadgeCount?.iCount)!
                            self.leaderBoardNameArray[2] = (dict.arrProgramBadgeLeadersList!.arrLeaderParticipantBadgeCount?.strName)!
                            self.leaderBoardBadgeCountArray[2] = (dict.arrProgramBadgeLeadersList!.arrLeaderParticipantBadgeCount?.iCount)!
                            self.leaderBoardNameArray[3] = (dict.arrProgramBadgeLeadersList!.arrLeaderContentRatingBadgeCount?.strName)!
                            self.leaderBoardBadgeCountArray[3] = (dict.arrProgramBadgeLeadersList!.arrLeaderContentRatingBadgeCount?.iCount)!
                            self.leaderBoardNameArray[4] = (dict.arrProgramBadgeLeadersList!.arrLeaderQuizCompletionBadgeCount?.strName)!
                            self.leaderBoardBadgeCountArray[4] = (dict.arrProgramBadgeLeadersList!.arrLeaderQuizCompletionBadgeCount?.iCount)!
                            self.viwCarousel.reloadData()
                            self.pageControl.numberOfPages = 5
                            
                            
                            print("dict:\(String(describing: dict.arrProgramBadgeLeadersList!.arrLeaderFacilitatorBadgeCount?.iCount))")
                        }
                    }
                }
            }
        })
        }
    }
    
}
