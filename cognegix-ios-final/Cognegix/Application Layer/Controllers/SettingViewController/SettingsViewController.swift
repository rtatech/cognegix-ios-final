//
//  SettingsViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 10/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import Alamofire


class SettingsViewController: BaseVC {

    //TODO: - General
    
    
    //TODO: - Controls
    
    @IBOutlet weak var profileSwitct: UISwitch!
    @IBOutlet weak var btnChangePwdOutlet: UIButton!
    @IBOutlet weak var txtConfPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNewPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtOldPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var viwChangePwd: UIView!
    @IBOutlet weak var viwProfile: UIView!
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialization()
        
        //Set profile iamge
        if SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto != ""{
            self.btnProfileImage.setImage(nil, for: UIControlState.normal)
            self.btnProfileImage.sd_setBackgroundImage(with: URL(string:(SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto)!), for: .normal)
        }
        
         let count = Int(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)
            if count == 0{
                self.lblNotificationCount.isHidden = true
            }
       
        self.lblNotificationCount.text = "\(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)"
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        circularButton(button: self.btnProfileImage)
          circularLabel(label: self.lblNotificationCount)
        circularButton(button: self.btnChangePwdOutlet)
        roundedCornerView(viw: self.viwProfile, clr: lightGrayColor)
        roundedCornerView(viw: self.viwChangePwd, clr: lightGrayColor)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom Function
    func initialization(){
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
    }
    
    
    //TODO: - UIButton Action

    @IBAction func btnChangePwdClick(_ sender: UIButton) {
        
        if self.txtOldPassword.validateBasic(name: "old password") &&
            self.txtNewPassword.validateBasic(name: "New password") &&
            self.txtConfPassword.validateBasic(name: "Confirm password") &&
            self.txtNewPassword.text == self.txtConfPassword.text{
            if self.txtOldPassword.text != self.txtNewPassword.text {
                self.callAPI()
            }else{
                showAlert(strMSG: "Confirm password is same as old password")
            }
            
        }else if(self.txtNewPassword.text != self.txtConfPassword.text){
            showAlert(strMSG: "Password does not match")
        }
        
       
    }
    
    @IBAction func profileSwitchChange(_ sender: Any) {
        self.callAPIToUpdateProfileVisibility()
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }
    
    //TODO: - API Integration
    func callAPI(){
        let parameters : Parameters = ["old_password": self.txtOldPassword.text ?? "",
                                       "password": self.txtNewPassword.text ?? "",
                                       "password_confirmation": self.txtConfPassword.text ?? ""]
      
        
        print("Login parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = changepassword
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)user/password/reset",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result.boolValue
                {
                    
                    self.showAlert(strMSG: "Password updated successfully")
                }else{
                   
                }
            }
        })
        }
        
        
        
    }
    
    
    func callAPIToUpdateProfileVisibility(){
        var profileValue : Int = Int()
        if profileSwitct.isOn{
            profileValue = 1
        }else{
             profileValue = 0
        }
        //profile_type:1   // 0 public 1 private
        let parameters : Parameters = ["profile_type": "\(profileValue)"]
        
        
        print("profile switch parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = update_profile_type
        Singleton.shared.requiredToken = true
        
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)user/update/profile-type",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result.boolValue
                {
                    
                    self.showAlert(strMSG: "Password updated successfully")
                }else{
                    
                }
            }
        })
        }
        
        
        
    }
    
    
}
