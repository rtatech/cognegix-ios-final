//
//  AddForumThreadViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 16/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import Alamofire

class AddForumThreadViewController: BaseVC, UITextViewDelegate {

    
    
    //TODO: - General
     var is_textEdited : Bool = Bool()
    var is_DesctextEdited : Bool = Bool()
     var progId : Int = 1
    //TODO: - Controls
    
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var txtQuestion: UITextView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialise()
        self.initPlaceholderForTextView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom Function
    func initialise(){
        self.txtQuestion.delegate = self
        viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        self.txtQuestion.layer.borderColor = UIColor(hexString: lightGrayColor)?.cgColor
        self.txtQuestion.layer.borderWidth = 1.0
        self.txtQuestion.layer.cornerRadius = 5
        self.txtQuestion.clipsToBounds = true
        
         self.txtDescription.delegate = self
        self.txtDescription.layer.borderColor = UIColor(hexString: lightGrayColor)?.cgColor
        self.txtDescription.layer.borderWidth = 1.0
        self.txtDescription.layer.cornerRadius = 5
        self.txtDescription.clipsToBounds = true
    }
    
    func initPlaceholderForTextView(){
        
        self.txtQuestion.text = "Enter Subject"
        self.txtQuestion.textColor = UIColor.lightGray
        is_textEdited = false
        
        self.txtDescription.text = "Enter Description (Optional)"
        self.txtDescription.textColor = UIColor.lightGray
        is_DesctextEdited = false
    }
    
    //TODO: UITextViewDelegate Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtQuestion{
        if textView.textColor == UIColor.lightGray{
            textView.text = nil
            textView.textColor = UIColor.black
            is_textEdited = true
        }
        }else if textView == txtDescription{
        
         if textView.textColor == UIColor.lightGray{
            textView.text = nil
            textView.textColor = UIColor.black
            is_DesctextEdited = true
        }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtQuestion{
            
            if textView.text.isEmpty{
                txtQuestion.text = "Enter Subject"
                txtQuestion.textColor = UIColor.lightGray
                is_textEdited = false
            }
            
        }else if textView == txtDescription{
            
            if textView.text.isEmpty{
                txtDescription.text = "Enter Description (Optional)"
                txtDescription.textColor = UIColor.lightGray
                is_DesctextEdited = false
            }
            
        }
    }
    
    
    //TODO: - UIButton Action

    @IBAction func btnPostClick(_ sender: UIButton) {
        if is_textEdited{
            self.callAPI()
        }else{
            showAlert(strMSG: "Please Enter Subject")
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    //TODO: - API Integration
    func callAPI(){
        
        var descText : String = String()
        if is_DesctextEdited{
            descText = self.txtDescription.text ?? ""
        }
        let parameters : Parameters = ["program_id": "\(progId)",
                                       "subject":self.txtQuestion.text ?? "",
                                       "description":descText]
        
        print("add forum parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = forumpostadd
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)forum/thread/add",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result as! Bool
                {
                    if result.boolValue{
                       // self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                        self.initPlaceholderForTextView()
                    }else{
                        self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    }
                    
                }
            }
        })
        }
        
        
        
    }
    
}
