//
//  ActivityDetailViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 03/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class ActivityDetailViewController: BaseVC,iCarouselDelegate, iCarouselDataSource {

    
    //TODO: - General
    
    var objProgram = programDetailObjectModel()
    
    
    //TODO: - Controls
    
    @IBOutlet weak var lblProgPercentage: UILabel!
    @IBOutlet weak var viwCircularProgress: UICircularProgressRingView!
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var viwCarousel: iCarousel!
    @IBOutlet weak var viwOuter: UIView!
    @IBOutlet weak var viwCustom: UIView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name(NotificationIdentifierReloadProgramDetail), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
         createNewView()
        
    }

    @objc func methodOfReceivedNotification(notification: Notification){
        
        objProgram = SINGLETON_OBJECT.objProgramDetails
        self.lblProgPercentage.text = "\(objProgram.iCompletion_percentage)%"
        
        let f = objProgram.iCompletion_percentage
        self.viwCircularProgress.value = f
        
        if SINGLETON_OBJECT.isFacilitator{
            self.lblProgPercentage.isHidden = true
              self.viwCircularProgress.isHidden = true
        }else{
            self.lblProgPercentage.isHidden = false
              self.viwCircularProgress.isHidden = true
        }
        
        viwCarousel.isPagingEnabled = true
        viwCarousel.bounces = false
        viwCarousel.delegate = self
        viwCarousel.dataSource = self
        viwCarousel.reloadData()
        
        
        print("Data recived")
    }
    
    
    
    //TODO: - Custom Function
    func createNewView(){
        viwCustom.backgroundColor = UIColor(hexString: menuBgColor)
        viwCustom.round(corners: [.bottomLeft, .bottomRight], radius: self.viwCustom.frame.size.width/2)
        self.viwCustom.layoutIfNeeded()
    }
    
    
    
    func createNewShape(){
        print("self.customView.frame.size.width:\(self.viwCustom.frame.size.width)")
        print("UIScreen.main.bounds.size.width:\(UIScreen.main.bounds.size.width)")
        //  self.constHeight.constant = UIScreen.main.bounds.size.width
        self.constTop.constant = -(UIScreen.main.bounds.size.width/2.6)
        
        self.viwCustom.layoutIfNeeded()
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.viwCustom.frame
        rectShape.position = self.viwCustom.center
        rectShape.path = UIBezierPath(roundedRect: self.viwCustom.bounds, byRoundingCorners: [.bottomLeft , .bottomRight , .bottomRight], cornerRadii: CGSize(width: self.viwCustom.frame.size.width, height: self.viwCustom.frame.size.width)).cgPath
        
        self.viwCustom.layer.backgroundColor = UIColor(hexString: menuBgColor)?.cgColor
        //Here I'm masking the textView's layer with rectShape layer
        self.viwCustom.layer.mask = rectShape
    }
    
    
    //MARK:- Carousel Delegate
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return 1
    }
    
    //show the card at the particular index
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let viwCard = (Bundle.main.loadNibNamed("LearningProgramView", owner: self, options: nil)![0] as? UIView)! as! LearningProgramView
        viwCard.frame = viwCarousel.frame
        
        viwCard.outerView.layer.cornerRadius = 5
        viwCard.clipsToBounds = true
        print("viewForItemAt index:\(index)")
        
        let photoURl = "\(VIDEO_BASE_URL)\(objProgram.strPhoto_path)"
        print("photoURl index:\(photoURl)")
        viwCard.imgProgram.sd_setImage(with: URL(string: (photoURl)), placeholderImage: UIImage(named: "temp_program_screen.png"))
        
        
        
       // viwCard.imgProgram.image = UIImage(named: Singleton.shared.progDetailImageString)
        viwCard.layer.cornerRadius = viwCard.frame.size.width / 2
        viwCard.clipsToBounds = true
        viwCard.lblProgramTitle.text = objProgram.strName
        viwCard.lblProgramObj.text = objProgram.strDescription//"identifying a negotiation skills"
        viwCard.btnLaunch.tag = index
        viwCard.btnLaunch.isHidden = true
        
        viwCard.btnLaunch.layer.cornerRadius = 5
        viwCard.clipsToBounds = true
        
        return viwCard
    }
    //For spacing of two items
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if(carousel == viwCarousel)
        {
            if (option == .spacing) {
                return value * 1.3
            }
            return value
        }
        else
        {
            if (option == .spacing) {
                return value * 1.0
            }
            return value
        }
        
    }
    
    //scrolling started
    func carouselDidScroll(_ carousel: iCarousel) {
       
        let index = carousel.currentItemIndex
        print("index:\(index)")
        
    }
    
    //on select of specific item
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
    
    //scroll end
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
