//
//  ForumProgramViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 08/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import Alamofire
class ForumTableViewCell : UITableViewCell{
    
    @IBOutlet weak var lblOtherTime: UILabel!
    @IBOutlet weak var lblMyTime: UILabel!
    @IBOutlet weak var lblDescViwOther: UILabel!
    @IBOutlet weak var lblTitleViwOther: UILabel!
    @IBOutlet weak var lblDescViwMe: UILabel!
    @IBOutlet weak var lblTitleViwMe: UILabel!
    @IBOutlet weak var viwOther: UIView!
    @IBOutlet weak var viwMe: UIView!
    @IBOutlet weak var lblDate: UILabel!
}
class ForumProgramViewController: BaseVC, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {

    
    //TODO: - General
      var arrForumThread = [forumThreadMessageModel]()
    var dateValue : String = String()
    var iProgramId : Int = Int()
    var iPage : Int = Int()
    var iMessageID : Int = Int()
    var iForumId : Int = Int()
    var iThreadId : Int = Int()
    
    //TODO:  - Controls
    
    @IBOutlet weak var viwDate: UIView!
    @IBOutlet weak var btnSendOutlet: UIButton!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var tblMain: UITableView!
    
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblMain.dataSource = self
        self.tblMain.delegate = self
        self.tblMain.tableFooterView = UIView()
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
        self.callAPI()
        
         setupLongPressGesture()
    }
    
    func displayToast(){
        //self.viwDate.makeToast("132")
        if dateValue != ""{
            self.viwDate.hideAllToasts()
            self.viwDate.makeToast(dateValue, duration: 3.0, position: .top)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        longPressGesture.delegate = self
        self.tblMain.addGestureRecognizer(longPressGesture)
    }
    
    
    func displayShareSheetOnLongpress(tag : Int){
           //Matched
            let alert = UIAlertController(title: "Do you want to delete message", message: "", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.destructive, handler: { (value) in
                //Call for delete API
                self.iMessageID = tag
                self.callAPIToDeleteMessage()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        
       
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: self.tblMain)
            if let indexPath = tblMain.indexPathForRow(at: touchPoint) {
                print("indexPath.row:\(indexPath.row)")
                let data = self.arrForumThread[indexPath.row]
                
                if data.iUserId  == SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.iUser_id{
                    self.displayShareSheetOnLongpress(tag :  data.iID)
                }
               
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    //TODO: - UITableviewDataSource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrForumThread.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! ForumTableViewCell
        cell.selectionStyle = .none
        
        roundedCornerView(viw: cell.viwMe, clr: lightGrayColor)
        roundedCornerView(viw: cell.viwOther, clr: lightGrayColor)
       
        let data = self.arrForumThread[indexPath.row]
        let outDate = BaseVC.convertDateToRespectiveFormat(strDate: data.strCreatdDate, strInputFormat: "yyyy-MM-dd HH:mm:ss", strOutputFormat: "dd MMM yyyy")
        
        
        cell.lblDate.text = outDate
       self.dateValue = outDate
        self.displayToast()
        if data.iUserId == SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.iUser_id{
            //Self
            cell.viwMe.isHidden = false
            cell.lblTitleViwMe.text = data.strFirstName
            cell.lblDescViwMe.text = data.strMessage
            cell.viwOther.isHidden = true
            
            let outTime = BaseVC.convertDateToRespectiveFormat(strDate: data.strCreatdDate, strInputFormat: "yyyy-MM-dd HH:mm:ss", strOutputFormat: "h:mm a")
            cell.lblMyTime.text = outTime
           cell.lblOtherTime.isHidden = true
        
            
        }else{
            //Other
            
            cell.viwMe.isHidden = true
            cell.viwOther.isHidden = false
            cell.lblTitleViwOther.text = data.strFirstName
            cell.lblDescViwOther.text = data.strMessage
            let outTime = BaseVC.convertDateToRespectiveFormat(strDate: data.strCreatdDate, strInputFormat: "yyyy-MM-dd HH:mm:ss", strOutputFormat: "h:mm a")
            cell.lblOtherTime.text = outTime
            cell.lblMyTime.isHidden = true
            
            
        }
        
        
        /*if indexPath.row  == 0{
            
        }else if indexPath.row  == 1{
           
        }else if indexPath.row == 2{
            cell.viwMe.isHidden = true
            cell.viwOther.isHidden = false
            cell.lblTitleViwOther.text = "Jack"
            cell.lblDescViwOther.text = "Lorem Ipsum is simply dummy text of the and typesetting."
            cell.lblDate.text = ""
            
        }else if indexPath.row  == 3{
            cell.viwMe.isHidden = false
            cell.lblTitleViwMe.text = "Marco"
            cell.lblDescViwMe.text = "Lorem Ipsum is simply dummy text of."
            cell.viwOther.isHidden = true
            cell.lblDate.text = "22-Mar-2018"
        }*/
        
        return cell
    }
    
    
    //TODO: - UIButton Action
   
    @IBAction func btnSendClick(_ sender: UIButton) {
        if self.txtMessage.validateBasic(name: "Message"){
            self.callAPIToSendQuery()
        }
    }
    
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //TODO: - API Integration
    
    func callAPIToSendQuery(){
        let parameters : Parameters = ["program_id": "\(iProgramId)",
            "message": self.txtMessage.text ?? "",
            "forum_id":"\(iForumId)",
            "thread_id":"\(iThreadId)"]
        
        
        print("Send Query parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = my_query_thread_message
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)forum/thread/post/add",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                
                if result.boolValue
                {
                    self.txtMessage.text = ""
                    APP_DELEGATE.errorMessage = ""
                    self.callAPI()
                    print("Query posted")
                }else{
                    if APP_DELEGATE.errorMessage != ""{
                       // self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                        APP_DELEGATE.errorMessage = ""
                    }
                }
            }
        })
        }
    }
    
    
    
    func callAPI(){
        let parameters : Parameters = ["program_id": "\(iProgramId)",
                                       "page":"\(iPage)",
                                       "forum_id":"\(iForumId)",
                                       "thread_id":"\(iThreadId)"]
        
        print("Program forum thread  parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = forum_thread_posts
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)forum/thread/posts",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
               
                if result is [ForumThreadPostsModel]
                {
                    
                    let notificationsModelresult = result as? [ForumThreadPostsModel]
                    
                    
                    if (notificationsModelresult?.count)! > 0{
                        
                        if let dict = notificationsModelresult?[0] {
                            if !dict.bStatus{
                                return
                            }
                            print("dict:\(dict.arrForumMessages![0].strMessage)")
                            self.arrForumThread = dict.arrForumMessages!
                            APP_DELEGATE.errorMessage = ""
                            self.tblMain.reloadData()
                        }
                        
                    }
                }else{
                    if APP_DELEGATE.errorMessage != ""{
                        self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                        APP_DELEGATE.errorMessage = ""
                        self.tblMain.reloadData()
                    }
                }
                
            }
        })
        }
        
        
        
    }
    
    
    
    
    
    func callAPIToDeleteMessage(){
        let parameters : Parameters = ["program_id": "\(iProgramId)",
            "message_id":"\(iMessageID)",
            "forum_id":"\(iForumId)",
            "thread_id":"\(iThreadId)"]
        
        print("Program forum thread  Message Delete parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = forum_thread_post_delete
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)forum/thread/post/delete",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                
                if result is [ForumThreadPostsModel]
                {
                    
                    let notificationsModelresult = result as? [ForumThreadPostsModel]
                    
                    
                    if (notificationsModelresult?.count)! > 0{
                        
                        if let dict = notificationsModelresult?[0] {
                            if !dict.bStatus{
                                return
                            }
                            //print("dict:\(dict.arrForumMessages![0].strMessage)")
                            self.arrForumThread.removeAll()
                            APP_DELEGATE.errorMessage = ""
                            self.callAPI()
                            //
                        }
                        
                    }
                }else{
                    if APP_DELEGATE.errorMessage != ""{
                        self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                        APP_DELEGATE.errorMessage = ""
                        self.tblMain.reloadData()
                    }
                }
                
            }
        })
        }
        
        
        
    }
    
}
