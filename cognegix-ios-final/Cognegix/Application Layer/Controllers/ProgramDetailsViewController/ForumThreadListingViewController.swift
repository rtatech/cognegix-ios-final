//
//  ForumThreadListingViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 16/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import Alamofire

class ForumThreadListingTableViewCell : UITableViewCell{
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viwOuter: UIView!
}

class ForumThreadListingViewController: BaseVC, UITableViewDelegate, UITableViewDataSource  {

    //TODO: - General
    let nameArray : [String] = ["Jagat Kumar Gour","Rupal Shetty",]
    let durationArray : [String] = ["30 min","20 min"]
    let subjectArray : [String] = ["Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical","Lorem Ipsum is not simply random text."]
    let descriptionArray : [String] = ["There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."]
    
    var arrForumThread = [forumMessagesModel]()
    var objForumModel = ForumPostsModel()
    var progId : Int = 1
    var iPageIndex : Int = 0
    //TODO: - Controls
    
    @IBOutlet weak var viwCircle: UIView!
    @IBOutlet weak var btnAddOutlet: UIButton!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var viwHeader: UIView!
    
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.initialise()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.arrForumThread.removeAll()
        self.tblMain.reloadData()
        iPageIndex = 0
         self.callAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom Function
    func initialise(){
        self.tblMain.tableFooterView = UIView()
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        circularButton(button: self.btnAddOutlet)
        self.viwCircle.layer.cornerRadius = self.viwCircle.frame.size.width/2
        self.viwCircle.clipsToBounds = true
    }
    
    //Scroll Pagin
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 && self.objForumModel.iRemaining>0 {
             self.iPageIndex  = self.iPageIndex  + 1
            self.callAPI()
            
        }
    }
    
    //TODO: - UITableViewDatasource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrForumThread.count//self.nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! ForumThreadListingTableViewCell
        cell.selectionStyle = .none
        
        roundedCornerView(viw: cell.viwOuter, clr: lightGrayColor)
       
         //cell.imgProfile.image = UIImage(named:"profile_placeholder")
        
        let data = arrForumThread[indexPath.row]
        
        cell.imgProfile.sd_setImage(with: URL(string: (data.strCreated_by_photo)), placeholderImage: UIImage(named: "profile_placeholder"))
        
        circularImageView(imgViw: cell.imgProfile)
        cell.lblName.text = data.strCreatedBy//self.nameArray[indexPath.row]
        cell.lblSubject.text = data.strSubject//self.subjectArray[indexPath.row]
        cell.lblDuration.text = data.strFormatted_created_at//self.durationArray[indexPath.row]
        cell.lblDescription.text = data.strMessage//self.descriptionArray[indexPath.row]
        
        // Check if the last row number is the same as the last current data element
        if indexPath.row == self.arrForumThread.count - 1  && self.objForumModel.iRemaining>0 {
             self.iPageIndex  = self.iPageIndex  + 1
            self.callAPI()
        }
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Indexpath:\(indexPath.row)")
        let data = self.arrForumThread[indexPath.row]
        let forumVC = self.storyboard?.instantiateViewController(withIdentifier: "idForumProgramViewController") as! ForumProgramViewController
        forumVC.iProgramId = progId
        forumVC.iPage = 1
        forumVC.iForumId = data.iForum_id
        forumVC.iThreadId = data.iID
        self.navigationController?.pushViewController(forumVC, animated: true)
    }
    
    //TODO: - UIButton Action

    @IBAction func btnAddNewClick(_ sender: UIButton) {
        let forumVC = self.storyboard?.instantiateViewController(withIdentifier: "idAddForumThreadViewController") as! AddForumThreadViewController
        forumVC.progId = progId
        self.navigationController?.pushViewController(forumVC, animated: true)
    }
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //TODO: - API Integration
    func callAPI(){
        
        let parameters : Parameters = ["program_id": "\(progId)",
                                       "page":"\(self.iPageIndex)"]
        
        print("Program forum thread  parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = forumposts
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)forum/threads",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [ForumPostsModel]
                {
                    
                    let notificationsModelresult = result as? [ForumPostsModel]
                    
                    
                    if (notificationsModelresult?.count)! > 0{
                        
                        if let dict = notificationsModelresult?[0] {
                            if !dict.bStatus{
                                return
                            }
                            print("dict:\(dict.arrForumMessages![0].strFirstName)")
                           // self.arrForumThread.append(dict.arrForumMessages)
                            //self.arrForumThread = dict.arrForumMessages!
                            
                            for ind in dict.arrForumMessages!{
                                self.arrForumThread.append(ind)
                            }
                            self.objForumModel.iRemaining = dict.iRemaining
                            
                            /*if dict.iRemaining > 0{
                                self.iPageIndex  = self.iPageIndex  + 1
                            }*/
                           self.tblMain.reloadData()
                        }
                        
                    }
                }
                
            }
        })
        }
    }
    
    
    
}
