//
//  ProgramDetailsViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 03/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import Alamofire
import AVKit
import AVFoundation

class ProgramDetailsViewController: BaseVC {

    //TODO: - General
   
    var selecteProgramId : Int = Int()
    var selecteProgramName : String = String()
    var selectedProgDoc : String = String()
    var selectedProgVideo : String = String()
    //TODO: - Controls
     var player: AVPlayer?
    
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var btnDetailOutlet: UIButton!
    @IBOutlet weak var viwMessageBoard: UIView!
    @IBOutlet weak var btnParticipantOutlet: UIButton!
    @IBOutlet weak var btnForumOutlet: UIButton!
    @IBOutlet weak var viwRating: HCSStarRatingView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var constHeight: NSLayoutConstraint!
    @IBOutlet weak var viwCustom: UIView!
    @IBOutlet weak var viwVideo: UIView!
    @IBOutlet weak var btnViewDetailsOutlet: UIButton!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var btnBackOutlet: UIButton!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.initialise()
        self.createBottomView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.callAPI()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        player?.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - Custom function
    func initialise(){
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        self.bottomView.backgroundColor = UIColor(hexString: menuBgColor)
        
        circularButton(button: self.btnProfileImage)
        circularLabel(label: self.lblNotificationCount)
        self.lblNotificationCount.backgroundColor = UIColor(hexString: NotificationCountBG)
        
         roundedCornerButton(button: self.btnViewDetailsOutlet, corner: 5, clr: btnColor)
        roundedCornerButton(button: self.btnForumOutlet, corner: 5, clr: btnColor)
        roundedCornerButton(button: self.btnParticipantOutlet, corner: 5, clr: btnColor)
        roundedCornerButton(button: self.btnDetailOutlet, corner: 5, clr: btnColor)
        roundedCornerView(viw: self.viwMessageBoard, clr: lightGrayColor)
        
        viwCustom.round(corners: [.topRight, .bottomRight], radius: self.viwCustom.frame.size.height/2)
        
        if SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto != ""{
            self.btnProfileImage.setImage(nil, for: UIControlState.normal)
            self.btnProfileImage.sd_setBackgroundImage(with: URL(string:(SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto)!), for: .normal)
        }
        let count = Int(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)
        if count == 0{
            self.lblNotificationCount.isHidden = true
        }
        
        self.lblNotificationCount.text = "\(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)"
        
        
        //Data bind
       
    }
    
    //This function is use to create bottom view
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    
    //TODO: - Custom function
    func initPlayer(){
        var videoURLServer =  SINGLETON_OBJECT.entDashboardModel.arrProgramList![SINGLETON_OBJECT.selectedProgIndex].objLearning_tree?.strProgramDirectorVideoUrl
        if videoURLServer != ""{
            
            if (videoURLServer?.containsIgnoringCase("/storage/mediaserver/video/"))!{
                videoURLServer = "\(VIDEO_BASE_URL)\(String(describing: videoURLServer!))"
            }
        }
        
        let partVC = self.storyboard?.instantiateViewController(withIdentifier: "idProgramDocumentViewController") as! ProgramDocumentViewController
        partVC.strLaunchURL =  videoURLServer!
        self.navigationController?.pushViewController(partVC, animated: true)
        
        
        
       // videoURLServer = "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"
        //strLaunchURL
        /*let url : NSString = videoURLServer as! NSString
        let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        var videoPlayURL : NSURL = NSURL(string: urlStr as String)!
       
        print("videoPlayURL:\(String(describing: videoPlayURL))")
       // let videoURL = URL(string: videoPlayURL!)
        let player = AVPlayer(url: videoPlayURL as URL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }*/
        
    }
    
    

    //TODO: - UIButton Action
    //This method is use to show bottom button are pressed
    //1001: Home, 1002: Search, 1003: Assistance, 1004: Setting
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            displayHomeScreen()
            break;
        case 1002:
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "idSearchViewController") as! SearchViewController
            self.navigationController?.pushViewController(searchVC, animated: true)
            break;
        case 1003:
            let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idHelpdeskViewController") as! HelpdeskViewController
            helpVC.isMenuNavigation = false
            self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
            displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnVideoClick(_ sender: UIButton) {
        self.initPlayer()
    }
    
    
    //Forum : 61, participant: 62, Learning Tree: 63
    @IBAction func btnClick(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 61:
            let forumVC = self.storyboard?.instantiateViewController(withIdentifier: "idForumThreadListingViewController") as! ForumThreadListingViewController
            forumVC.progId = selecteProgramId
            self.navigationController?.pushViewController(forumVC, animated: true)
            break;
        case 62:
           let partVC = self.storyboard?.instantiateViewController(withIdentifier: "idParticipantListViewController") as! ParticipantListViewController
           partVC.iProgramID = selecteProgramId
            SINGLETON_OBJECT.iRemainBadge  = SINGLETON_OBJECT.objProgramDetails.iRemaining_distribution_badges
           self.navigationController?.pushViewController(partVC, animated: true)
            break;
        case 63:
            let partVC = self.storyboard?.instantiateViewController(withIdentifier: "idLearningTreeViewController") as! LearningTreeViewController
            partVC.progID = selecteProgramId
            partVC.progName = selecteProgramName
            partVC.strLearningTreeTitle = "Learning Tree"
            self.navigationController?.pushViewController(partVC, animated: true)
            break;
        case 64:
            let partVC = self.storyboard?.instantiateViewController(withIdentifier: "idProgramDocumentViewController") as! ProgramDocumentViewController
            partVC.strLaunchURL = "\(VIDEO_BASE_URL)\(selectedProgDoc)"
            self.navigationController?.pushViewController(partVC, animated: true)
            break;
        default:
            break;
        }
    }
    
    
    
    //TODO: - API Integration
    func callAPI(){
        
        let parameters : Parameters = ["program_id": "\(selecteProgramId)"]
        
        print("notification list  parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = program_detail
        Singleton.shared.requiredToken = true
        
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)program/detail",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [ProgramDetailModel]
                {
                    
                    let UserModelresult = result as? [ProgramDetailModel]
                    
                    if (UserModelresult?.count)! > 0{
                        if let dict = UserModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                return
                            }
                            
                            print("dict.arrProgramList![0].strName:\(String(describing: dict.objProgramdetails!.strName))")
                            SINGLETON_OBJECT.objProgramDetails = dict.objProgramdetails!
                            
                            NotificationCenter.default.post(name: Notification.Name(NotificationIdentifierReloadProgramDetail), object: nil)
                            print("Data Send")
                        }
                    }
                }
                
            }
        })
        }
        
        
        
    }
    
    
    
}
