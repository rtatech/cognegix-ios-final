//
//  ParticipantListViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 07/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import Alamofire

class ParticipantTableViewCell : UITableViewCell{
    
    @IBOutlet weak var btnVideoCall: UIButton!
    @IBOutlet weak var lblProgIdentity: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgPartLogo: UIImageView!
}

class ParticipantListViewController: BaseVC, UITableViewDataSource,UITextFieldDelegate, UITableViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate {

    //TODO: - General
  
    var arrParticipantList = [userListModel]()
      var arrTmpParticipantList = [userListModel]()
    var iProgramID : Int = Int()
    //TODO: - Controls
    
    @IBOutlet weak var constTableBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var btnCommonVideoCallOutlet: UIButton!
    @IBOutlet weak var txtSearchBar: UISearchBar!
    @IBOutlet weak var txtSearch: DesignableUITextField!
    //@IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var tblMain: UITableView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblMain.dataSource = self
        self.tblMain.delegate = self
        self.txtSearch.delegate = self
        txtSearchBar.delegate = self
        self.txtSearchBar.isHidden = true
        self.txtSearch.isHidden = false
        self.tblMain.tableFooterView = UIView()
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
        
        let myLayer = CALayer()
        let myImage = UIImage(named: "bottom_Search")?.cgImage
        myLayer.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        myLayer.contents = myImage
        txtSearch.layer.addSublayer(myLayer)
        roundedCornerButton(button: self.btnCommonVideoCallOutlet, corner: 5, clr: btnColor)
        
      //Facilitator Conditions
        if SINGLETON_OBJECT.isFacilitator{
            self.btnCommonVideoCallOutlet.isHidden = false
            constTableBottomSpace.constant = 50
        }else{
             self.btnCommonVideoCallOutlet.isHidden = true
            constTableBottomSpace.constant = 0
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.txtSearch.layer.cornerRadius = self.txtSearch.frame.size.height/2
        self.txtSearch.clipsToBounds = true
        
        self.callAPI()
    }
    
    //UITextField Delegate Method implementation
   /* func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text != "" {
            arrTmpParticipantList.removeAll()
            for participant in arrParticipantList {
                let name = participant.strFirst_name + " " + participant.strLast_nam
                if name.contains(textField.text!){
                    arrTmpParticipantList.append(participant)
                }
            }
        } else {
            arrTmpParticipantList = self.arrParticipantList
        }
        
        tblMain.reloadData()
        return true
    }
   */
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        if textField.text != "" {
            arrTmpParticipantList.removeAll()
            for ind in arrParticipantList {
                let name = ind.strFirst_name + " " + ind.strLast_nam
                if name.containsIgnoringCase(textField.text!){
                    arrTmpParticipantList.append(ind)
                }
            }
        } else {
            arrTmpParticipantList = self.arrParticipantList
        }
        tblMain.reloadData()
        
        return true
    }
    
    
   
 
    @objc func searchRecordsAsPerText(_ textfield:UITextField) {
       
        if textfield.text?.characters.count != 0 {
             arrTmpParticipantList.removeAll()
            for ind in arrParticipantList {
                let name = ind.strFirst_name + ind.strLast_nam
                if name.contains(textfield.text!){
                     arrTmpParticipantList.append(ind)
                }
                
                /*let range = name.lowercased().range(of: textfield.text!, options: .caseInsensitive, range: nil,   locale: nil)
                
                if range != nil {
                    arrTmpParticipantList.append(ind)
                }*/
            }
        } else {
            arrTmpParticipantList = self.arrParticipantList
        }
        
        tblMain.reloadData()
    }
    
    
    //TODO: UISearchBarDelegate method implementation
    
    /*func filterTableViewForEnterText(searchText: String) {
        _ = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
        let filteredItems = self.arrParticipantList.filter { $0.strLast_nam.contains(searchText)}
        self.arrTmpParticipantList = filteredItems
        self.tblMain.reloadData()
    }
    
    func searchDisplayController(_ controller: UISearchDisplayController, shouldReloadTableForSearch searchString: String?) -> Bool{
        self.filterTableViewForEnterText(searchText: searchString!)
        return true
    }
    
    func searchDisplayController(_ controller: UISearchDisplayController,
                                 shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        self.filterTableViewForEnterText(searchText: self.searchDisplayController!.searchBar.text!)
        return true
    }
    */
    
    //TODO: - UITableViewDataSource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return  self.arrTmpParticipantList.count//self.arrParticipantList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! ParticipantTableViewCell
        cell.selectionStyle = .none
        
        roundedCornerButton(button: cell.btnView, corner: 5, clr: btnColor)
        roundedCornerButton(button: cell.btnVideoCall, corner: 5, clr: btnColor)
        if SINGLETON_OBJECT.isFacilitator{
            cell.btnVideoCall.isHidden = false
        }else{
           cell.btnVideoCall.isHidden = true
        }
        let data = self.arrTmpParticipantList[indexPath.row]//self.arrParticipantList[indexPath.row]
        cell.lblName.text = data.strFirst_name + " " + data.strLast_nam
        cell.lblDesignation.text = data.strDesignation
        cell.lblMobileNumber.text = "Mobile: " + data.strWork_phone_number
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(self.profileDetailPress(_:)), for: .touchUpInside)
        return cell
    }
    
    //TODO: - UIButton Action
    @objc func profileDetailPress(_ sender:UIButton){
        let profdtVC = self.storyboard?.instantiateViewController(withIdentifier: "idOtherParticipantProfileViewController") as! OtherParticipantProfileViewController
        let data = self.arrTmpParticipantList[sender.tag]//self.arrParticipantList[sender.tag]
        profdtVC.objParticipantList = data
        profdtVC.iProgramID = iProgramID
        //SINGLETON_OBJECT.iRemainBadge = data.iRemaining_distribution_badges
        self.navigationController?.pushViewController(profdtVC, animated: true)
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
     self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCommonVideoCallClick(_ sender: UIButton) {
    }
    
    
    //
    
    
    
    //TODO: - API Integration
    func callAPI(){
        
        let parameters : Parameters = ["program_id": "\(iProgramID)"]
        
        print("notification list  parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = program_users
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)program/users",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [ParticipantListModel]
                {
                    
                    let notificationsModelresult = result as? [ParticipantListModel]
                    
                    
                    if (notificationsModelresult?.count)! > 0{
                        
                        if let dict = notificationsModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                return
                            }
                          //  print("dict:\(String(describing: dict.arrUserList![0].strFirst_name))")
                            self.arrParticipantList = dict.arrUserList!
                            self.arrTmpParticipantList = self.arrParticipantList
                            
                            //SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count = dict.arrNotifications!.count
                            self.tblMain.reloadData()
                        }
                        
                    }
                }
                
            }
        })
        }
        
        
        
    }
    
    
}
