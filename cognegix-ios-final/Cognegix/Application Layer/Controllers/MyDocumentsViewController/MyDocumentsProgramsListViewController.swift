//
//  MyDocumentsProgramsListViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 29/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import Alamofire

class MyDocumentsProgramListTableViewCell : UITableViewCell{
    
    @IBOutlet weak var lblIndicator: UILabel!
    @IBOutlet weak var lblProgramTitle: UILabel!
}

class MyDocumentsProgramsListViewController: BaseVC, UITableViewDataSource, UITableViewDelegate {

    
    //TODO: - General
    var titleArray = ["Understanding of Negotiation Skills","Understanding negotiation situation","Grievance handling","Performance Appraisal handling"]
    
    var arrProgramList = [programListModel]()
    //TODO: - Controls
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var menuBtnOutlet: UIButton!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    override func viewDidLoad() {
        super.viewDidLoad()

        //Bottom Menu Navigation
        self.createBottomView()
        
        circularButton(button: self.btnProfileImage)
        circularLabel(label: self.lblNotificationCount)
        self.lblNotificationCount.backgroundColor = UIColor(hexString: NotificationCountBG)
        
        self.bottomView.backgroundColor = UIColor(hexString: menuBgColor)
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        self.tblMain.tableFooterView = UIView()
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        
//        self.tblMain.rowHeight = UITableViewAutomaticDimension
//        self.tblMain.estimatedRowHeight = 100;
//        
        //Revealviewcontroller code
        self.navigationController?.isNavigationBarHidden = true
        if self.revealViewController() != nil {
            menuBtnOutlet.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
        //Set profile iamge
        if SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto != ""{
            self.btnProfileImage.setImage(nil, for: UIControlState.normal)
            self.btnProfileImage.sd_setBackgroundImage(with: URL(string:(SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto)!), for: .normal)
        }
        let count = Int(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)
        if count == 0{
            self.lblNotificationCount.isHidden = true
        }        
        self.lblNotificationCount.text = "\(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)"
        
        
        self.callAPI()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - UITableViewDatasource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrProgramList.count//titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! MyDocumentsProgramListTableViewCell
        
        cell.selectionStyle = .none
        let data = arrProgramList[indexPath.row]
        cell.lblProgramTitle.text = data.strName//self.titleArray[indexPath.row]
        cell.lblIndicator.text = "PR"
        cell.lblIndicator.backgroundColor = .random()
        cell.lblIndicator.layer.cornerRadius = cell.lblIndicator.frame.size.width/2
        cell.lblIndicator.clipsToBounds = true
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dtVC = self.storyboard?.instantiateViewController(withIdentifier: "idMyDocumentListViewController") as! MyDocumentListViewController
          dtVC.arrProgramList = arrProgramList
        let data = arrProgramList[indexPath.row]
        if  data.strUser_role == strFacilitator{
            SINGLETON_OBJECT.isFacilitator = true
        }else{
            SINGLETON_OBJECT.isFacilitator = false
        }
        self.navigationController?.pushViewController(dtVC, animated: true)
    }

    //This function is use to create bottom view
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    //TODO: - UIButton Action

    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
           displayHomeScreen()
            break;
        case 1002:
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "idSearchViewController") as! SearchViewController
            self.navigationController?.pushViewController(searchVC, animated: true)
            break;
        case 1003:
            let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idHelpdeskViewController") as! HelpdeskViewController
            helpVC.isMenuNavigation = false
            self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
            displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }
    
    
    
    
    //TODO: - API Integration
    func callAPI(){
        
        let parameters : Parameters = ["": ""]
        
        print("mydocument progr parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = user_programs
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToGetAPI("\(BASE_URL)user/programs",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [DashboardModel]
                {
                    let UserModelresult = result as? [DashboardModel]
                    if (UserModelresult?.count)! > 0{
                        if let dict = UserModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                return
                            }
                            print("dict:\(String(describing: dict.arrProgramList![0].strDescription))")
                            self.arrProgramList = dict.arrProgramList!
                            self.tblMain.reloadData()
                        }
                    }
                }
            }
        })
        }
    }
    
}
