//
//  MenuViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 25/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit



class MenuViewController: BaseVC {

    
    //TODO: - General
    
    //TODO: - Controls
    
    @IBOutlet weak var viwCollection: UIView!
    @IBOutlet weak var viwProfile: UIView!
    
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var imgLayer: UIImageView!
    @IBOutlet weak var viwRoundMe: UIView!
    
    
    //ButtonOutlets
   
    
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Hello Menu")
        circularImageView(imgViw: self.imgProfilePic)
         //circularImageView(imgViw: self.imgLayer)
         viwRoundMe.round(corners: [.bottomLeft, .bottomRight], radius: self.viwRoundMe.frame.size.height/2)
        //self.imgLayer.layer.borderColor = UIColor.clear.cgColor
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.lblName.text = (SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strFirstName)! +  " " + (SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strLastName)!
        self.lblDesignation.text = SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strDesignation
        self.lblCompanyName.text = SINGLETON_OBJECT.entUserModel.arrUser_organization_detail?.strName
        
        self.imgProfilePic.sd_setImage(with: URL(string: (SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto)!), placeholderImage: UIImage(named: "profile_placeholder"))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //Tag: 11: program, 12: profile, 13: My Doc, 14: Past Pro, 15: Badge, 16: Helpdesk, 17: My Qury, 18: Logout
    @IBAction func btnClick(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 11:
            //Programs
            let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "idHomeViewController") as! HomeViewController
            let frontView = UINavigationController.init(rootViewController:homeVC)
            revealViewController().pushFrontViewController(frontView, animated: true)
            break;
        case 12:
            //Profile
            let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "idProfileViewController") as! ProfileViewController
            let frontView = UINavigationController.init(rootViewController:homeVC)
            revealViewController().pushFrontViewController(frontView, animated: true)
            break;
        case 13:
            //My Documents
            let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "idMyDocumentsProgramsListViewController") as! MyDocumentsProgramsListViewController
            let frontView = UINavigationController.init(rootViewController:homeVC)
            revealViewController().pushFrontViewController(frontView, animated: true)
            break;
        case 14:
            //Past programs
            let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "idPastProgramViewController") as! PastProgramViewController
            let frontView = UINavigationController.init(rootViewController:homeVC)
            revealViewController().pushFrontViewController(frontView, animated: true)
            
            break;
        case 15:
            //Badges
            let badgeListVC = self.storyboard?.instantiateViewController(withIdentifier: "idBadgesProgramListViewController") as! BadgesProgramListViewController
            let frontView = UINavigationController.init(rootViewController:badgeListVC)
            revealViewController().pushFrontViewController(frontView, animated: true)
            break;
        case 16:
            //Helpdesk
            let helpDeskVC = self.storyboard?.instantiateViewController(withIdentifier: "idHelpdeskViewController") as! HelpdeskViewController
            let frontView = UINavigationController.init(rootViewController:helpDeskVC)
            helpDeskVC.isMenuNavigation = true
            revealViewController().pushFrontViewController(frontView, animated: true)
            break;
        case 17:
            //Forum
            let badgeListVC = self.storyboard?.instantiateViewController(withIdentifier: "idForumProgramListViewController") as! ForumProgramListViewController
            let frontView = UINavigationController.init(rootViewController:badgeListVC)
            revealViewController().pushFrontViewController(frontView, animated: true)
            break;
        case 18:
            //Assement
            let badgeListVC = self.storyboard?.instantiateViewController(withIdentifier: "idAssesmentViewController") as! AssesmentViewController
            let frontView = UINavigationController.init(rootViewController:badgeListVC)
            revealViewController().pushFrontViewController(frontView, animated: true)
            break;
        case 19:
            //My Query
            let MyQueryProgVC = self.storyboard?.instantiateViewController(withIdentifier: "idMyQueryProgramListViewController") as! MyQueryProgramListViewController
            let frontView = UINavigationController.init(rootViewController:MyQueryProgVC)
            revealViewController().pushFrontViewController(frontView, animated: true)
            break;
        case 20:
            General.removeSaved(name: strUserName)
            General.removeSaved(name: strUserPassword)
            General.removeSaved(name: isUserLogin)
            APP_DELEGATE.API_Name = ""
            Singleton.shared.requiredToken = false
            APP_DELEGATE.beginApplication()
            // showAlertWithDismis(strMSG: "You are logout")
            //Logout
            break;
        default:
            break;
        }
    }
    

}

