//
//  NotificationDetailsViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 13/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import Alamofire

class NotificationDetailsViewController: BaseVC {

    
    //TODO: - General
    var strText : String = String()
    var notificationID : Int = Int()
    //TODO: - Controls
    
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
        self.txtMessage.text = strText
        self.callAPI()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //TODO: - API Integration
    func callAPI(){
        
        let parameters : Parameters = ["notification_id": "\(notificationID)"]
        
        print("notification read  parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = Notification_mark_read
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)user/notification/mark/read",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [NotificationModel]
                {
                    
                    let notificationsModelresult = result as? [NotificationModel]
                    
                    
                    if (notificationsModelresult?.count)! > 0{
                        
                        if let dict = notificationsModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                return
                            }
                        }
                        
                    }
                }
                
            }
        })
        }
        
        
        
    }
    
    

}
