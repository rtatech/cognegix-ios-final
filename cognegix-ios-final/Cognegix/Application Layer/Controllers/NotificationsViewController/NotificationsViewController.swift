//
//  NotificationsViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 10/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import Alamofire

class NotificationsTableViewCell : UITableViewCell{
    
    @IBOutlet weak var viwOuter: UIView!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblPostedBy: UILabel!
    @IBOutlet weak var lblNotificationMsg: UILabel!
}

class NotificationsViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {

    
    
    //TODO: - General
    // let notificationTitleArray : [String] = ["New program Understanding of \"Negotiation Skills\" is about to start this week.","You have successfully completed topic \"Identifying objectives and building agendas\""]
    
    var arrNotification = [notificationsListModel]()
    //TODO: - Controls
    
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var btnProfileOutlet: UIButton!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        self.tblMain.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.callAPI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - UITableViewDatasource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrNotification.count//notificationTitleArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! NotificationsTableViewCell
        cell.selectionStyle = .none
        
        let data = arrNotification[indexPath.row]
        
        cell.lblDuration.text = data.strFormatted_created_at//"1 day ago"
        //cell.lblNotificationMsg.text = data.strNotification//self.notificationTitleArray[indexPath.row]
        cell.lblPostedBy.isHidden = true
        if data.iIs_read == 1{
            cell.lblNotificationMsg.text =  data.strNotification//self.notificationTitleArray[indexPath.row]
            cell.viwOuter.backgroundColor = UIColor.white
        }else{
             cell.lblNotificationMsg.text = "*" + data.strNotification//self.notificationTitleArray[indexPath.row]
            cell.viwOuter.backgroundColor = UIColor.lightGray
        }
        roundedCornerView(viw: cell.viwOuter, clr: lightGrayColor)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        let notiDt = self.storyboard?.instantiateViewController(withIdentifier: "idNotificationDetailsViewController") as! NotificationDetailsViewController
        let data = arrNotification[indexPath.row]
        notiDt.strText = data.strNotification
        notiDt.notificationID = data.iID
        self.navigationController?.pushViewController(notiDt, animated: true)
    }
    

    @IBAction func btnBackClick(_ sender: UIButton) {
        if self.navigationController != nil{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    
    
    //TODO: - API Integration
    func callAPI(){
        
        let parameters : Parameters = ["notification_is_read": "all"]
        
        print("notification list  parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = NotificationList
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)user/notification",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [NotificationModel]
                {
                    
                    let notificationsModelresult = result as? [NotificationModel]
                   
                    
                    if (notificationsModelresult?.count)! > 0{
                        
                        if let dict = notificationsModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                return
                            }
                            print("dict:\(String(describing: dict.arrNotifications![0].strNotification))")
                            self.arrNotification = dict.arrNotifications!
                            
                            var unreadNotiCount : Int = Int()
                            for index in 0...dict.arrNotifications!.count-1{
                                let data = dict.arrNotifications![index]
                                if data.iIs_read != 1{
                                    unreadNotiCount = unreadNotiCount + 1
                                }
                            }
                            SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count =  unreadNotiCount //dict.arrNotifications!.count
                            self.tblMain.reloadData()
                        }
                      
                    }
                }
                
            }
        })
        }
        
        
        
    }
    
    
    
    
    
}
