//
//  HelpDeskBeforeLoginViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 10/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import Alamofire

class HelpDeskBeforeLoginViewController: BaseVC,UITextViewDelegate {

    //TODO: - General
    var bIsHelpTyepMail : Bool = Bool()
    var is_textEdited : Bool = Bool()
    //TODO: - Controls
    
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var btnSubmitOutlet: UIButton!
    @IBOutlet weak var txtOrganizationName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var viwHeader: UIView!
    
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialise()
        self.initPlaceholderForTextView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //TODO: - Custom Function
    func initialise(){
        
        self.txtFirstName.text = ""
        self.txtLastName.text = ""
        self.txtEmail.text = ""
        self.txtOrganizationName.text = ""
        self.initPlaceholderForTextView()
        
        
        viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        circularTextfield(textfield: self.txtFirstName)
        circularTextfield(textfield: self.txtLastName)
        circularTextfield(textfield: self.txtEmail)
        circularTextfield(textfield: self.txtOrganizationName)
        circularButton(button: self.btnSubmitOutlet)
        
        //Left padding
        self.txtFirstName.setLeftPaddingPoints(20)
         self.txtLastName.setLeftPaddingPoints(20)
         self.txtEmail.setLeftPaddingPoints(20)
         self.txtOrganizationName.setLeftPaddingPoints(20)
        //Right padding
        self.txtFirstName.setRightPaddingPoints(8)
        self.txtLastName.setRightPaddingPoints(8)
        self.txtEmail.setRightPaddingPoints(8)
        self.txtOrganizationName.setRightPaddingPoints(8)
        
        
        self.txtMessage.delegate = self
        self.txtMessage.layer.borderColor = UIColor(hexString: menuBgColor)?.cgColor
        self.txtMessage.layer.borderWidth = 1.0
        self.txtMessage.layer.cornerRadius = 10
        self.txtMessage.clipsToBounds = true
    }
    
    func initPlaceholderForTextView(){
        
        self.txtMessage.text = "Message"
        self.txtMessage.textColor = UIColor.lightGray
        is_textEdited = false
        
    }

    
    //TODO: UITextViewDelegate Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtMessage{
            if textView.textColor == UIColor.lightGray{
                textView.text = nil
                textView.textColor = UIColor.black
                is_textEdited = true
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtMessage{
            if textView.text.isEmpty{
                txtMessage.text = "Message"
                txtMessage.textColor = UIColor.lightGray
                is_textEdited = false
            }
        }
    }
    
    
    
    //TODO: - UIButton Action

    @IBAction func btnBackClick(_ sender: UIButton) {
         self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func btnSubmitClick(_ sender: UIButton) {
        if self.txtFirstName.validateBasic(name: "First Name") &&
        self.txtLastName.validateBasic(name: "Last Name") &&
        self.txtOrganizationName.validateBasic(name: "Organization Name") &&
        self.txtEmail.validateBasic(name: "Email ID") &&
        self.txtEmail.validateEmail(name: "Email ID") &&
        self.is_textEdited{
            self.callAPI()
        }
    }
    
    
    
    //TODO: - API Integration
    func callAPI(){
       //['mail', 'sms']
        var valHelpType : String = String()
        if bIsHelpTyepMail{
            valHelpType = "mail"
        }else{
            valHelpType = "sms"
        }
        
        let parameters : Parameters = ["help_type": valHelpType,
                                       "help_content":self.txtMessage.text ?? "",
                                       "first_name":self.txtFirstName.text ?? "",
                                       "last_name":self.txtLastName.text ?? "",
                                       "organization_name":self.txtOrganizationName.text ?? "",
                                       "email":self.txtEmail.text ?? ""]
        
        print("Helpdesk outer list  parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = guest_help_raise
       
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)guest/help/raise",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result.boolValue
                {
                    self.showAlert(strMSG: "Help raised successfully")
                    self.initialise()
                    self.initPlaceholderForTextView()
                }else{
                    self.initialise()
                    self.initPlaceholderForTextView()
                    
                }
            }
        })
        }
        
        
        
    }
    
    
    
    
}
