//
//  HelpDeskAskViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 21/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import Alamofire

class HelpDeskAskViewController: BaseVC, UITextViewDelegate {

    //TODO: - General
    var bIsHelpTyepMail : Bool = Bool()
    var is_textEdited : Bool = Bool()
    
    //TODO: - Controls
    
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var btnSubmitOutlet: UIButton!
    @IBOutlet weak var txtMessage: UITextView!
    //TODO: - Let's Code
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialise()
        self.initPlaceholderForTextView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - Custom Function
    func initialise(){
        
        self.initPlaceholderForTextView()
        
        
        viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
        circularButton(button: self.btnSubmitOutlet)
        
        self.txtMessage.delegate = self
        self.txtMessage.layer.borderColor = UIColor(hexString: menuBgColor)?.cgColor
        self.txtMessage.layer.borderWidth = 1.0
        self.txtMessage.layer.cornerRadius = 10
        self.txtMessage.clipsToBounds = true
    }
    
    func initPlaceholderForTextView(){
        
        self.txtMessage.text = "Message"
        self.txtMessage.textColor = UIColor.lightGray
        is_textEdited = false
        
    }

    
    //TODO: UITextViewDelegate Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtMessage{
            if textView.textColor == UIColor.lightGray{
                textView.text = nil
                textView.textColor = UIColor.black
                is_textEdited = true
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtMessage{
            if textView.text.isEmpty{
                txtMessage.text = "Message"
                txtMessage.textColor = UIColor.lightGray
                is_textEdited = false
            }
        }
    }
    
    
    //TODO: - UIButton Action

    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitClick(_ sender: UIButton) {
        if self.is_textEdited{
            self.callAPI()
        }else{
            self.showAlert(strMSG: "Please enter message")
        }
    }
    
    
    
    
    //TODO: - API Integration
    func callAPI(){
        //['mail', 'sms']
        var valHelpType : String = String()
        if bIsHelpTyepMail{
            valHelpType = "mail"
        }else{
            valHelpType = "sms"
        }
        
        let parameters : Parameters = ["help_type": valHelpType,
                                       "help_content":self.txtMessage.text ?? "" ]
        
        print("Helpdesk outer list  parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = guest_help_raise
        
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)user/help/raise",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result.boolValue
                {
                    self.showAlert(strMSG: "Help raised successfully")
                    self.initialise()
                    self.initPlaceholderForTextView()
                }else{
                    self.initialise()
                    self.initPlaceholderForTextView()
                }
            }
        })
        }
        
        
        
    }
    
    
    
    
}
