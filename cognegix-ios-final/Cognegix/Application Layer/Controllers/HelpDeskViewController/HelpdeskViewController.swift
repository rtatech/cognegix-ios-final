//
//  HelpdeskViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 02/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import Alamofire

class FAQTableViewCell : UITableViewCell{
    
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var constViewHeight: NSLayoutConstraint!
}

class HelpdeskViewController: BaseVC, UITableViewDataSource, UITableViewDelegate {

    //TODO: - General
    var isLoginNavigation : Bool = Bool()
    var isMenuNavigation : Bool = Bool()
    private var descCellExpand : Bool = false
    private var expandCellIndex : Int = Int()
    var arrFAQ = [FAQsListModel]()
    //var arrExpandCell : [Bool] = [false,false,false,false,false,false,false,false,false,false]
    
    
    //TODO: - Controls
    
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    
    @IBOutlet weak var bottomConstraintConstant: NSLayoutConstraint!
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var btnMenuOutlet: UIButton!
    
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblMain.tableFooterView = UIView()
        self.createBottomView()
        self.initialise()
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        //Revealviewcontroller code
        self.navigationController?.isNavigationBarHidden = true
        if isMenuNavigation{
            if self.revealViewController() != nil {
                btnMenuOutlet.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            }
        }else{
            self.btnMenuOutlet.setImage(UIImage(named: "ic_left-arrow"), for: .normal)
            self.btnMenuOutlet.addTarget(self, action: #selector(self.btnBackPress(_:)), for: .touchUpInside)
            
        }
        
        
        self.callAPI()
        
    }
    
    
    func initialise(){
        circularButton(button: self.btnProfileImage)
        circularLabel(label: self.lblNotificationCount)
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
        //Set profile iamge
        if SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto != ""{
            self.btnProfileImage.setImage(nil, for: UIControlState.normal)
            self.btnProfileImage.sd_setBackgroundImage(with: URL(string:(SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto)!), for: .normal)
        }
        let count = Int(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)
        if count == 0{
            self.lblNotificationCount.isHidden = true
        }
        
        self.lblNotificationCount.text = "\(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - UITableViewDatasource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrFAQ.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! FAQTableViewCell
        
        cell.selectionStyle = .none
        
        cell.outerView.layer.cornerRadius = 5
        cell.outerView.layer.borderWidth = 1
        cell.outerView.layer.borderColor = UIColor(hexString: lightGrayColor)?.cgColor
        cell.outerView.clipsToBounds = true
        
        cell.titleView.layer.borderColor = UIColor(hexString: lightGrayColor)?.cgColor
        //cell.titleView.layer.borderWidth = 0
        
        let data = self.arrFAQ[indexPath.row]
        
        cell.constViewHeight.constant = CGFloat(data.dTitleHeight+2)
        
        print("123:\(data.strTitle.heightWithConstrainedWidth(width: cell.lblTitle.frame.size.width, font: UIFont(name: "Roboto-medium", size: 15.0)!))")
       // print("self.arrExpandCell[indexPath.row]:\(self.arrExpandCell[indexPath.row])")
        if data.bIsExpand{
            cell.imgArrow.image = UIImage(named: "up-button")
            //cell.titleView.layer.borderWidth = 1
        }else{
            cell.titleView.layer.borderColor = UIColor.black.cgColor
            //cell.titleView.layer.borderWidth = 1
            cell.imgArrow.image = UIImage(named: "down-button")
        }
        
        cell.lblTitle.text =   data.strTitle
        cell.lblDescription.text = data.strDescription
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for ind in 0...self.arrFAQ.count-1{
            if ind == indexPath.row{
                self.arrFAQ[indexPath.row].bIsExpand = true
            }else{
                self.arrFAQ[ind].bIsExpand = false
            }
            
        }
        
        //expandCellIndex = indexPath.row
      //  let indexPathRow:Int = 0
        let indexPosition = IndexPath(row: indexPath.row, section: 0)
        self.tblMain.reloadRows(at: [indexPosition], with: UITableViewRowAnimation.none)
       // self.tblMain.reloadRows(at: [indexPosition], with: .none)

        
        self.tblMain.beginUpdates()
        self.tblMain.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.arrFAQ[indexPath.row].bIsExpand == true{
            //if descCellExpand{
            return  CGFloat(self.arrFAQ[indexPath.row].dHeight+90) // 160
            //}else{
            //    return 60
            //}
        }else{
            return 88
        }
        
    }
    
    //This function is use to create bottom view
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    
    //TODO: - UIButton Action
    @objc func btnBackPress(_ sender:UIButton){
        if self.navigationController != nil{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    //This method is use to show bottom button are pressed
    //1001: Home, 1002: Search, 1003: Assistance, 1004: Setting
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            displayHomeScreen()
            break;
        case 1002:
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "idSearchViewController") as! SearchViewController
            self.navigationController?.pushViewController(searchVC, animated: true)
            break;
        case 1004:
           displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
        default:
            break;
        }
    }

    //91: EMail, 92: Message, 93: call
    @IBAction func btnHelpDeskMenuClick(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 91:
            let hdVC = self.storyboard?.instantiateViewController(withIdentifier: "idHelpDeskAskViewController")  as! HelpDeskAskViewController
            hdVC.bIsHelpTyepMail = true
           self.navigationController?.pushViewController(hdVC, animated: true)
            break;
        case 92:
            let hdVC = self.storyboard?.instantiateViewController(withIdentifier: "idHelpDeskAskViewController")  as! HelpDeskAskViewController
            hdVC.bIsHelpTyepMail = true
            self.navigationController?.pushViewController(hdVC, animated: true)
            break;
        case 93:
            if let url = NSURL(string: "tel://\(callInitiateToNumber)"), UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(url as URL)
            }else{
                showAlert(strMSG: "Unable to call on number")
            }
            break;
        default:
            break;
        }
    }
    
    
    
    
    
    //TODO: - API Integration
    func callAPI(){
        
        let parameters : Parameters = ["": ""]
        
        print("dashboard parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = help_faq_list
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToGetAPI("\(BASE_URL)help/faq/list",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [HelpDeskFAQListModel]
                {
                    
                    let UserModelresult = result as? [HelpDeskFAQListModel]
                    
                    if (UserModelresult?.count)! > 0{
                        if let dict = UserModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                return
                            }
                            self.arrFAQ = dict.arrFaqs!
                            
                            for index in 0...self.arrFAQ.count-1{
                               let desc = self.arrFAQ[index].strDescription
                               let height = desc.heightWithConstrainedWidth(width: UIScreen.main.bounds.size.width-20, font: UIFont(name: "Roboto-medium", size: 15.0)!)
                                let Title = self.arrFAQ[index].strTitle
                                let tHeight = Title.heightWithConstrainedWidth(width: UIScreen.main.bounds.size.width-20, font: UIFont(name: "Roboto-medium", size: 15.0)!)
                                
                                self.arrFAQ[index].dHeight = Double(height) + Double(tHeight)
                                self.arrFAQ[index].dTitleHeight = Double(tHeight)+10
                            }
                            
                            
                            
                            self.tblMain.reloadData()
                            //print("dict.arrFaqs[0].strTitle:\(String(describing: dict.arrFaqs![0].strTitle))")
                        }
                    }
                }
            }
        })
        }
    }
    
    
}
