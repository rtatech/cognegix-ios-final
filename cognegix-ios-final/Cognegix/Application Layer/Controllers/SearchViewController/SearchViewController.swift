//
//  SearchViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 25/06/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import Alamofire

class SearchViewController: BaseVC,UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {

    
    //TODO: - General
    var arrLearningTreeSearch = [ProgramLearningTreeListModel]()
    //TODO: - Controls
    
    @IBOutlet weak var txtSearch: DesignableUITextField!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txtSearch.delegate = self
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: UITextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField.text != ""{
            self.callAPI()
        }
        return true
    }
    
    
    
    
    //TODO: - UITableViewDataSource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return  self.arrLearningTreeSearch.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! ParticipantTableViewCell
        cell.selectionStyle = .none
        
        let data = self.arrLearningTreeSearch[indexPath.row]
        cell.lblName.text = data.strProgram_name
        cell.lblDesignation.text = data.strTopic_name
        cell.lblMobileNumber.isHidden = true
        //cell.btnView.isHidden = true
        
        cell.lblProgIdentity.text = "PR"
        cell.lblProgIdentity.backgroundColor = .random()
        cell.lblProgIdentity.layer.cornerRadius = cell.lblProgIdentity.frame.size.width/2
        cell.lblProgIdentity.clipsToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print("Index:\(indexPath.row)")
        let partVC = self.storyboard?.instantiateViewController(withIdentifier: "idLearningTreeViewController") as! LearningTreeViewController
        let data = self.arrLearningTreeSearch[indexPath.row]
        partVC.strLearningTreeTitle = "Learning Tree"
        partVC.strLaunchURL = data.strLaunch_url
        partVC.itHasURL = true
        self.navigationController?.pushViewController(partVC, animated: true)
    }
    
    //TODO: - UIButton Action

    @IBAction func btnBackClick(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    //TODO: - API Integration
    func callAPI(){
        
        let parameters : Parameters = ["search": self.txtSearch.text! ?? ""]
        
        print("notification list  parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = program_learning_tree_search
        Singleton.shared.requiredToken = true
        
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)program/learning/tree/search",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [LearningTreeSearchModel]
                {
                    
                    let notificationsModelresult = result as? [LearningTreeSearchModel]
                    
                    
                    if (notificationsModelresult?.count)! > 0{
                        
                        if let dict = notificationsModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                return
                            }
                            //print("dict:\(String(describing: dict.arrLearningTreeProgram![0].strProgram_name))")
                            self.arrLearningTreeSearch = dict.arrLearningTreeProgram!
                            self.tblMain.reloadData()
                        }
                        
                    }
                }
                
            }
        })
        
        }
        
        
    }
    
    
}
