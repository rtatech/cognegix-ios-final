//
//  LoginViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 22/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import Alamofire
class LoginViewController: BaseVC {

    //TODO:- General
    var tapGesture : UITapGestureRecognizer  = UITapGestureRecognizer()
    
    //TODO: - Controls
    //CustomView
    @IBOutlet weak var viwCustom: UIView!
    @IBOutlet weak var constHeight: NSLayoutConstraint!
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var constInputField: NSLayoutConstraint!
    @IBOutlet weak var constLogoHeight: NSLayoutConstraint!
    @IBOutlet weak var constLogoWidth: NSLayoutConstraint!
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var viwLogoBack: UIView!
    
    @IBOutlet weak var lblTerms: UILabel!
    
    @IBOutlet weak var btnForgotPwdOutlet: UIButton!
    @IBOutlet weak var btnLoginOutlet: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var imgBackground: UIImageView!
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.autolayoutCustomView()
        
       
       self.createNewView()
        
        circularImageView(imgViw: self.imgLogo)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
         self.initialization()
        circularTextfield(textfield: self.txtUsername)
        circularTextfield(textfield: self.txtPassword)
        circularButton(button: self.btnLoginOutlet)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - Custom Function
    func initialization(){
        self.btnForgotPwdOutlet.setTitleColor(UIColor(hexString: menuBgColor), for: .normal)
        //Left padding
        self.txtPassword.setLeftPaddingPoints(20)
        self.txtUsername.setLeftPaddingPoints(20)
        //Right padding
        self.txtPassword.setRightPaddingPoints(8)
        self.txtUsername.setRightPaddingPoints(8)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.tapLabel(_:)))
        self.lblTerms.addGestureRecognizer(tapGesture)
        self.lblTerms.isUserInteractionEnabled = true
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.linkLabel()
        APP_DELEGATE.API_Name = ""
        Singleton.shared.requiredToken = false
        
       //AUto login
        let defaults = UserDefaults.standard
        if let stringOne = defaults.value(forKey: isUserLogin) as? String {
            if stringOne == "1"{
                self.txtUsername.text = General.loadSaved(name: strUserName)
                self.txtPassword.text = General.loadSaved(name: strUserPassword)
                
                self.callAPI()
            }
            
        }
        //self.txtUsername.text =  "millind.moravekar@vnnogile.com" //vivek.chudgar@vnnogile.com" //"tagsab2000@gmail.com"// "chandrabhan.pal@vnnogile.in"// "" //"chandrabhan.pal@vnnogile.in"//
        //self.txtPassword.text = "pass@123"// "pass@1234"
 
    }
    
    func linkLabel(){
        self.lblTerms.text = "By logging in, you agree to the Terms & Conditions of Cognegix Digital Learning"
        let text = (self.lblTerms.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms & Conditions")
        underlineAttriString.addAttribute(NSAttributedStringKey.font, value:UIFont(name: "HelveticaNeue-bold", size: 13)!, range: range1)
        underlineAttriString.addAttribute(NSAttributedStringKey.underlineStyle, value:NSUnderlineStyle.styleSingle.rawValue, range: range1)
         self.lblTerms.attributedText = underlineAttriString
    }
    func autolayoutCustomView(){
        print("APP_DELEGATE.screenWidth:\(APP_DELEGATE.screenWidth)")
        if APP_DELEGATE.screenWidth == 320{
            constTop.constant = -80
            constHeight.constant = 320
            constInputField.constant = 32
        }else if APP_DELEGATE.screenWidth == 375{
            constTop.constant = -100
            constHeight.constant = 375
            constInputField.constant = 46
        }else if APP_DELEGATE.screenWidth == 414{
            
        }else if APP_DELEGATE.screenWidth < 320{
            
        }else if APP_DELEGATE.screenWidth > 414{
            //iPad air 2
            constTop.constant = -350//(APP_DELEGATE.screenWidth/1.5)
            constHeight.constant = APP_DELEGATE.screenWidth
            constInputField.constant = 58
            constLogoWidth.constant = 350
             constLogoHeight.constant = 350
        }
         self.viwCustom.layoutIfNeeded()
    }
    func createNewView(){
        viwCustom.backgroundColor = UIColor(hexString: menuBgColor)
        viwCustom.round(corners: [.bottomLeft, .bottomRight], radius: self.viwCustom.frame.size.width/2)
        self.viwCustom.layoutIfNeeded()
    }
    
    
    
    func createNewShape(){
        print("self.customView.frame.size.width:\(self.viwCustom.frame.size.width)")
        print("UIScreen.main.bounds.size.width:\(UIScreen.main.bounds.size.width)")
        self.constHeight.constant = UIScreen.main.bounds.size.width
        self.constTop.constant = -(UIScreen.main.bounds.size.width/4)
        
        self.viwCustom.layoutIfNeeded()
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.viwCustom.frame
        rectShape.position = self.viwCustom.center
        rectShape.path = UIBezierPath(roundedRect: self.viwCustom.bounds, byRoundingCorners: [.bottomLeft , .bottomRight , .bottomRight], cornerRadii: CGSize(width: self.viwCustom.frame.size.width, height: self.viwCustom.frame.size.width)).cgPath
        
        self.viwCustom.layer.backgroundColor = UIColor(hexString: menuBgColor)?.cgColor
        //Here I'm masking the textView's layer with rectShape layer
        self.viwCustom.layer.mask = rectShape
    }
    
    //TODO: - UIButton Action
    //11: Forgot, 12: Login
    
    
    @IBAction func btnClick(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 11:
            //Forgot Pwd button action
            let forgotVC = self.storyboard?.instantiateViewController(withIdentifier: "idForgotPasswordFirstViewController")  as! ForgotPasswordFirstViewController
            self.present(forgotVC, animated: true, completion: nil)
            break;
        case 12:
            //Login button action
            
           callAPI()
           
            break;
        default:
            break;
        }
    }
    
    @IBAction func tapLabel(_ gesture: UITapGestureRecognizer) {
        let text = (self.lblTerms.text)!
        let termsRange = (text as NSString).range(of: "Terms & Conditions")
        let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
        if gesture.didTapAttributedTextInLabel(label: self.lblTerms, inRange: termsRange) {
            let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "idTermsAndConditionsViewController")  as! TermsAndConditionsViewController
            self.present(termsVC, animated: true, completion: nil)
        } else if gesture.didTapAttributedTextInLabel(label: self.lblTerms, inRange: privacyRange) {
            print("Tapped privacy")
        } else {
            print("Tapped none")
        }
    }
    
    
    //Tag: 71: call, 81: Email, 91: MEssage
    @IBAction func btnHelpDeskMenuClick(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 71:
            if let url = NSURL(string: "tel://\(callInitiateToNumber)"), UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(url as URL)
            }else{
                showAlert(strMSG: "Unable to call on number")
            }
            break;
        case 81:
            let hdVC = self.storyboard?.instantiateViewController(withIdentifier: "idHelpDeskBeforeLoginViewController")  as! HelpDeskBeforeLoginViewController
            hdVC.bIsHelpTyepMail = true
            self.present(hdVC, animated: true, completion: nil)
            break;
        case 91:
            let hdVC = self.storyboard?.instantiateViewController(withIdentifier: "idHelpDeskBeforeLoginViewController")  as! HelpDeskBeforeLoginViewController
             hdVC.bIsHelpTyepMail = false
            self.present(hdVC, animated: true, completion: nil)
            break;
        default:
            break;
        }
    }
    
    
    //TODO: - API Integration
    func callAPI(){
        //"tagsab2000@gmail.com", "password": "",
        
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
           
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
        
        
        
        let parameters : Parameters = ["username": self.txtUsername.text ?? "",
        "password": self.txtPassword.text ?? "",
            "device_token":"",
            "device_type":"iOs"]
            
            print("Login parameters:\(parameters)")
            
            let objData = DataManager()
            
            objData.callToAPI("\(BASE_URL)user/authenticate",parameters:parameters, completion: { (result, error) in
                
                print("result:\(result.count)")
                if error.isKind(of: NSError.classForCoder()) //Error code
                {
                    General.hideActivityIndicator()
                }
                else
                {
                    General.hideActivityIndicator()
                    if APP_DELEGATE.errorMessage != ""{
                        self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                        APP_DELEGATE.errorMessage = ""
                    }
                    if result is [UserModel]
                    {
                       
                            let UserModelresult = result as? [UserModel]
                        
                            if (UserModelresult?.count)! > 0{
                                if let dict = UserModelresult?[0] {
                                    if !dict.bStatus {
                                        self.showAlert(strMSG: dict.strMessage)
                                       // APP_DELEGATE.showAlert(strMessage: dict.strMessage)
                                        return
                                    }
                                    
                                    print("dict:\(String(describing: dict.arrUser_detail?.strEmail))")
                                    SINGLETON_OBJECT.entUserModel = dict
                                    print("dict:\(String(describing: SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strEmail))")
                                    
                                    //Navigate
                                    let tempVc = self.storyboard?.instantiateViewController(withIdentifier: "idRevelBase")  as! UINavigationController
                                    self.present(tempVc, animated: true, completion: nil)
                                    
                                    General.saveData(data: self.txtUsername.text!, name: strUserName)
                                    General.saveData(data: self.txtPassword.text!, name: strUserPassword)
                                    General.saveData(data: "1", name: isUserLogin)
                                    
                                }
                            }
                    }
                    /*if result is [user_profile_detail]
                    {
                        
                        let UserModelresult = result as? [user_profile_detail]
                        
                        if (UserModelresult?.count)! > 0{
                            if let dict = UserModelresult?[0] {
                                print("dict:\(String(describing: dict.strFirstName) )")
                                // SINGLETON_OBJECT.entuserModel = dict
                            }
                        }
                    }*/
                    /*if result is [user_organization_detail]
                    {
                        
                        let UserModelresult = result as? [user_organization_detail]
                        
                        if (UserModelresult?.count)! > 0{
                            if let dict = UserModelresult?[0] {
                                print("dict:\(String(describing: dict.strName) )")
                                // SINGLETON_OBJECT.entuserModel = dict
                            }
                        }
                    }*/
                }
            })
            
        }
        
    }
    
    
    
    
    

}
