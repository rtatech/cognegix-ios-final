//
//  MyQueriesViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 11/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit


import Alamofire


class MyQueriesTableViewCell : UITableViewCell{
    
    @IBOutlet weak var lblAskedToTitle: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblDateValue: UILabel!
    @IBOutlet weak var lblProgramValue: UILabel!
    @IBOutlet weak var lblTopicValue: UILabel!
    @IBOutlet weak var lblAskedToValue: UILabel!
    @IBOutlet weak var btnView: UIButton!
    
}


class MyQueriesViewController: BaseVC, UITableViewDelegate,UITableViewDataSource {

    
    //TODO: - General
    let dropDown = DropDown()
    var arrQueryList = [queriesListModel]()
    var iProgID : Int = Int()
    var arrProgramList = [programListModel]()
    var dataSourceVal = [String]()
    
    //TODO: -- Controls
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var btnPlusOutlet: UIButton!
    @IBOutlet weak var viwCircle: UIView!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var lblDD: UILabel!
    @IBOutlet weak var btnSelectProgramOutlet: UIButton!
    
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        circularButton(button: self.btnPlusOutlet)
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        self.viwCircle.layer.cornerRadius = self.viwCircle.frame.size.width / 2
        self.viwCircle.clipsToBounds = true
        circularButton(button: self.btnProfileImage)
        circularLabel(label: self.lblNotificationCount)
        self.lblNotificationCount.backgroundColor = UIColor(hexString: NotificationCountBG)
        
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        
        roundedCornerButton(button: self.btnSelectProgramOutlet, corner: 5, clr: lightGrayColor)
        
        
        
        //Bottom Menu Navigation
        self.createBottomView()
        
        
        self.callAPI()
        
        //Set profile iamge
        if SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto != ""{
            self.btnProfileImage.setImage(nil, for: UIControlState.normal)
            self.btnProfileImage.sd_setBackgroundImage(with: URL(string:(SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto)!), for: .normal)
        }
        
        let count = Int(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)
        if count == 0{
            self.lblNotificationCount.isHidden = true
        }
        self.lblNotificationCount.text = "\(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)"
        
        
        for ind in self.arrProgramList{
            self.dataSourceVal.append(ind.strName)
        }
        
        if  SINGLETON_OBJECT.isFacilitator{
            self.btnPlusOutlet.isHidden = true
             self.viwCircle.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //TODO: - Custom function
    
    //This function is use to create bottom view
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    
    
    //TODO: - UITableViewDataSource Method implementation
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrQueryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! MyQueriesTableViewCell
        cell.selectionStyle = .none
        
        roundedCornerView(viw: cell.outerView, clr: lightGrayColor)
        roundedCornerButton(button: cell.btnView, corner: 5, clr: menuBgColor)
        
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(self.btnViewClick(_:)), for: .touchUpInside)
        
        let data = self.arrQueryList[indexPath.row]
        
        
        
        cell.lblDateValue.text = "Not in response"//"0\(indexPath.row + 1) March 201\(indexPath.row)"
        cell.lblProgramValue.text = data.strProgramName
        cell.lblTopicValue.text = data.strTopicName
        cell.lblAskedToValue.text = data.strReceiverName
        if  SINGLETON_OBJECT.isFacilitator{
            cell.lblAskedToTitle.text = "Asked by:"
        }else{
            cell.lblAskedToTitle.text = "Asked to:"
        }
        
        return cell
    }
    
    
    //TODO: - UIButton Click
    
    @objc func btnViewClick(_ sender:UIButton){
        let data = self.arrQueryList[sender.tag]
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "idQueryChatViewController") as! QueryChatViewController
        detailVC.iQueryThreadID = data.iQueryThreadId
        detailVC.strPageTitle = data.strProgramName
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    //This method is use to show bottom button are pressed
    //1001: Home, 1002: Search, 1003: Assistance, 1004: Setting
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            displayHomeScreen()
            break;
        case 1002:
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "idSearchViewController") as! SearchViewController
            self.navigationController?.pushViewController(searchVC, animated: true)
            break;
        case 1003:
            let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idHelpdeskViewController") as! HelpdeskViewController
            helpVC.isMenuNavigation = false
            self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
            displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    
    
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }
    
    
    @IBAction func btnPlusClick(_ sender: UIButton) {
        let askqVC = self.storyboard?.instantiateViewController(withIdentifier: "idAskQueryViewController") as! AskQueryViewController
        askqVC.arrProgramList = self.arrProgramList
        self.navigationController?.pushViewController(askqVC, animated: true)
    }
    @IBAction func btnSelectProgram(_ sender: UIButton) {
        dropDown.anchorView = lblDD
        dropDown.dataSource = self.dataSourceVal//["Understanding of Negotiation Skills","Understanding negotiation situation","Grievance handling","Performance Appraisal handling"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btnSelectProgramOutlet.setTitle(item, for: .normal)
            print("Selected item: \(item) at index: \(index)")
            self.iProgID = self.arrProgramList[index].iID
            self.callAPI()
        }
        dropDown.show()
    }
    
    //11:Notification, 12: profile
    @IBAction func btnActionBarClick(_ sender: UIButton) {
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //TODO: - API Integration
    func callAPI(){
        let parameters : Parameters = ["program_id": "\(iProgID)"]
        
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = myquery_thread_list
        Singleton.shared.requiredToken = true
        
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)my-query/thread/list",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [MyQueryThreadListModel]
                {
                    
                    let notificationsModelresult = result as? [MyQueryThreadListModel]
                    
                    
                    if (notificationsModelresult?.count)! > 0{
                        
                        if let dict = notificationsModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                return
                            }
                          //  print("dict:\(String(describing: dict.arrQueries![0].strProgramName))")
                            self.arrQueryList = dict.arrQueries!
                            self.tblMain.reloadData()
                        }
                        
                    }
                }
                
            }
        })
    }
    
    }
    
    
}
