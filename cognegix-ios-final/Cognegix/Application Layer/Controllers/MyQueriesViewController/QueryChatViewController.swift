//
//  QueryChatViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 11/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit


import Alamofire

class QueryChatViewCell : UITableViewCell{
    
    @IBOutlet weak var lblOtherTime: UILabel!
    @IBOutlet weak var lblMyTime: UILabel!
    @IBOutlet weak var lblDescViwOther: UILabel!
    @IBOutlet weak var lblTitleViwOther: UILabel!
    @IBOutlet weak var lblDescViwMe: UILabel!
    @IBOutlet weak var lblTitleViwMe: UILabel!
    @IBOutlet weak var viwOther: UIView!
    @IBOutlet weak var viwMe: UIView!
    @IBOutlet weak var lblDate: UILabel!
}
class QueryChatViewController: BaseVC, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    
    //TODO: - General
    var arrQueryList = [queryMessagesModel]()
    var iMessageID : Int = Int()
    var dateValue : String = String()
    var strPageTitle : String = String()
    var iQueryThreadID : Int = Int()
    var dateArray : [String] = ["20-Mar-2018","","21-Mar-2018",""]
    //TODO: - Controls
    
    @IBOutlet weak var viwDate: UIView!
    @IBOutlet weak var lblProgTitle: UILabel!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var btnSendOutlet: UIButton!
    
    //TODO: - Let's Message
    
    override func viewDidLoad() {
        super.viewDidLoad()
      lblProgTitle.text = strPageTitle // "Negotiation Skill"
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        self.tblMain.tableFooterView = UIView()
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
        self.callAPI()
        
        setupLongPressGesture()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func displayToast(){
        //self.viwDate.makeToast("132")
        if dateValue != ""{
            self.viwDate.hideAllToasts()
            self.viwDate.makeToast(dateValue, duration: 3.0, position: .top)
        }
        
    }
    
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        longPressGesture.delegate = self
        self.tblMain.addGestureRecognizer(longPressGesture)
    }
    
    
    func displayShareSheetOnLongpress(tag : Int){
        //Matched
        let alert = UIAlertController(title: "Do you want to delete message", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.destructive, handler: { (value) in
            //Call for delete API
            self.iMessageID = tag
            self.callAPIToDeleteMessage()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: self.tblMain)
            if let indexPath = tblMain.indexPathForRow(at: touchPoint) {
                print("indexPath.row:\(indexPath.row)")
                let data = self.arrQueryList[indexPath.row]
                
                if data.iIsFromSender == 1{
                    self.displayShareSheetOnLongpress(tag :  data.iId)
                }
                
            }
        }
    }
    
    
    
    
    
    //TODO: - UITableviewDataSource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrQueryList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! QueryChatViewCell
        cell.selectionStyle = .none
        
        roundedCornerView(viw: cell.viwMe, clr: lightGrayColor)
        roundedCornerView(viw: cell.viwOther, clr: lightGrayColor)
        
        let data = self.arrQueryList[indexPath.row]
        let outDate = BaseVC.convertDateToRespectiveFormat(strDate: data.strCreatedAt, strInputFormat: "yyyy-MM-dd HH:mm:ss", strOutputFormat: "dd MMM yyyy")
        
        
         self.dateValue = outDate
         self.displayToast()
        if data.iIsFromSender == 1{
            //Self
            cell.viwMe.isHidden = false
            cell.lblTitleViwMe.text = data.strSender_name
            cell.lblDescViwMe.text = data.strMessage
            cell.viwOther.isHidden = true
            let outTime = BaseVC.convertDateToRespectiveFormat(strDate: data.strCreatedAt, strInputFormat: "yyyy-MM-dd HH:mm:ss", strOutputFormat: "h:mm a")
            cell.lblMyTime.text = outTime
            cell.lblOtherTime.isHidden = true
            
            //cell.lblDate.text = data.strFormatted_created_at//outDate
            
        }else{
            //Other
            cell.viwMe.isHidden = true
            cell.viwOther.isHidden = false
            cell.lblTitleViwOther.text = data.strSender_name
            cell.lblDescViwOther.text = data.strMessage
            
            let outTime = BaseVC.convertDateToRespectiveFormat(strDate: data.strCreatedAt, strInputFormat: "yyyy-MM-dd HH:mm:ss", strOutputFormat: "h:mm a")
            cell.lblOtherTime.text = outTime
            cell.lblMyTime.isHidden = true
            
            
            //cell.lblDate.text = data.strFormatted_created_at
        }
        
        return cell
    }
    
    
    @IBAction func btnSendClick(_ sender: UIButton) {
        if self.txtMessage.validateBasic(name: "Message"){
            callAPIToSendQuery()
        }
    }
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //TODO: - API Integration
    func callAPI(){
        let parameters : Parameters = ["query_thread_id": "\(iQueryThreadID)"]
        
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = my_query_thread_message_list
        Singleton.shared.requiredToken = true
        
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)my-query/thread/message/list",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [MyQueryThreadMessageListModel]
                {
                    
                    let notificationsModelresult = result as? [MyQueryThreadMessageListModel]
                    
                    
                    if (notificationsModelresult?.count)! > 0{
                        
                        
                        if let dict = notificationsModelresult?[0] {
                            
                           // print("dict:\(String(describing: dict.arrQueryMessage![0].strMessage))")
                            if dict.arrQueryMessage?.count != nil{
                            if dict.arrQueryMessage?.count != 0{
                                self.arrQueryList = dict.arrQueryMessage!
                                }
                                
                            }
                             self.txtMessage.text = ""
                            self.tblMain.reloadData()
                        }
                        
                    }
                }
                
            }
        })
        }
    }
    
    
    
    func callAPIToSendQuery(){
        
        let parameters : Parameters = ["query_thread_id": "\(iQueryThreadID)",
                                       "query": self.txtMessage.text ?? ""]
        
        
        print("Send Query parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = my_query_thread_message
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)my-query/thread/message",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                
                if result.boolValue
                {
                    APP_DELEGATE.errorMessage = ""
                    self.callAPI()
                    print("Query posted")
                    
                }else{
                    if APP_DELEGATE.errorMessage != ""{
                        self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                        APP_DELEGATE.errorMessage = ""
                    }
                }
            }
        })
        }
        
        
        
    }
    
    
    
    
    
    func callAPIToDeleteMessage(){
        let parameters : Parameters = ["query_thread_id": "\(iQueryThreadID)",
            "message_id":"\(iMessageID)"]
        
        print("Program forum thread  Message Delete parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = forum_thread_post_delete
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)my-query/thread/message/delete",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                
                if result is [ForumThreadPostsModel]
                {
                    
                    let notificationsModelresult = result as? [ForumThreadPostsModel]
                    
                    
                    if (notificationsModelresult?.count)! > 0{
                        
                        if let dict = notificationsModelresult?[0] {
                            if !dict.bStatus{
                                return
                            }
                            //print("dict:\(dict.arrForumMessages![0].strMessage)")
                            self.arrQueryList.removeAll()
                            APP_DELEGATE.errorMessage = ""
                            self.callAPI()
                            //
                        }
                        
                    }
                }else{
                    if APP_DELEGATE.errorMessage != ""{
                        self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                        APP_DELEGATE.errorMessage = ""
                        self.tblMain.reloadData()
                    }
                }
                
            }
        })
        }
        
        
        
    }
    
    
}
