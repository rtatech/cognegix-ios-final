//
//  AskQueryViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 11/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import Alamofire

class AskQueryViewController: BaseVC , UITextViewDelegate {
    
    
    //TODO: - General
    let dropDown = DropDown()
    
    var bProgSelected : Bool = Bool()
    var bTopicSelected : Bool = Bool()
    var bPersonSelected : Bool = Bool()
    
    var is_textEdited : Bool = Bool()
    
     var arrProgramList = [programListModel]()
    var arrProgramTopicList = [String]()
    var arrProgramTopicIDList = [Int]()
    
    var arrPersonToAskList = [String]()
    var arrPersonToAskIDList = [Int]()
    
    var dataSourceVal = [String]()
    
    var iProgId : Int = Int()
    var iTopicId : Int = Int()
    var iWhomeToAsk : Int = Int()
    //TODO: - Controls
    
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var lblDDPerson: UILabel!
    @IBOutlet weak var lblDDTopic: UILabel!
    @IBOutlet weak var lblDDProgram: UILabel!
    @IBOutlet weak var txtQuestion: UITextView!
    @IBOutlet weak var btnPersonToAskOutlet: UIButton!
    @IBOutlet weak var btnTopicOutlet: UIButton!
    @IBOutlet weak var btnProgramOutlet: UIButton!
    //TODO: - Let's Code
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.design()
        initPlaceholderForTextView()
        
        for ind in self.arrProgramList{
            self.dataSourceVal.append(ind.strName)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initialiseDetails(){
        self.bProgSelected = false
        self.btnProgramOutlet.setTitle("Select", for: .normal)
        
        self.bTopicSelected = false
        self.btnTopicOutlet.setTitle("Select", for: .normal)
        
        self.bPersonSelected = false
        self.btnPersonToAskOutlet.setTitle("Select", for: .normal)
    }
    
    func design(){
        roundedCornerButton(button: self.btnProgramOutlet, corner: 5, clr: lightGrayColor)
        roundedCornerButton(button: self.btnTopicOutlet, corner: 5, clr: lightGrayColor)
        roundedCornerButton(button: self.btnPersonToAskOutlet, corner: 5, clr: lightGrayColor)
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
        self.txtQuestion.layer.cornerRadius = 5
        self.txtQuestion.clipsToBounds = true
        self.txtQuestion.layer.borderColor = UIColor(hexString: lightGrayColor)?.cgColor
        self.txtQuestion.layer.borderWidth  = 1
        self.txtQuestion.delegate = self
        
    }
    
    
    func initPlaceholderForTextView(){
        
        
        self.txtQuestion.text = "Enter Question"
        self.txtQuestion.textColor = UIColor.lightGray
        is_textEdited = false
        
    }
    
    //TODO: UITextViewDelegate Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray{
            textView.text = nil
            textView.textColor = UIColor.black
            is_textEdited = true
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtQuestion{
            
            if textView.text.isEmpty{
                txtQuestion.text = "Enter Question"
                txtQuestion.textColor = UIColor.lightGray
                is_textEdited = false
            }
            
        }
    }
    
    
    //TODO: - UIButton Action
    
    //Tag 71: Program, 72: Topic, 73: Person to Ask
    @IBAction func btnDropDownClick(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 71:
            dropDown.anchorView = lblDDProgram
            dropDown.dataSource = self.dataSourceVal //["Understanding of Negotiation Skills","Understanding negotiation situation","Grievance handling","Performance Appraisal handling"]
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.btnProgramOutlet.setTitle(item, for: .normal)
                self.bProgSelected = true
                print("Selected item: \(item) at index: \(index)")
                self.iProgId = self.arrProgramList[index].iID
                self.callToFetchTopicAPI()
            }
            dropDown.show()
            break;
        case 72:
            dropDown.anchorView = lblDDTopic
            dropDown.dataSource = self.arrProgramTopicList//["Negotiation Skills", "Communication Skills", "English Training"]
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.bTopicSelected = true
                self.iTopicId = self.arrProgramTopicIDList[index]
                self.btnTopicOutlet.setTitle(item, for: .normal)
            }
            
            dropDown.show()
            break;
        case 73:
            dropDown.anchorView = lblDDPerson
            dropDown.dataSource = self.arrPersonToAskList //["Admin", "Facilitator", "Student"]
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.btnPersonToAskOutlet.setTitle(item, for: .normal)
                self.bPersonSelected = true
                print("Selected item: \(item) at index: \(index)")
                self.iWhomeToAsk = self.arrPersonToAskIDList[index]
            }
            
            dropDown.show()
            break;
        default:
            break;
        }
    }
    
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        if  self.navigationController != nil{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func btnPostClick(_ sender: UIButton) {
        if bProgSelected && bTopicSelected && bPersonSelected && is_textEdited{
            callAPI()
        }else{
            showAlert(strMSG: "Invalid inputs")
        }
    }
    
    
    //TODO: - API Integration
    
    func callToFetchTopicAPI(){
        let parameters : Parameters = ["program_id": "\(self.iProgId)"]
        
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = program_topics
        Singleton.shared.requiredToken = true
        
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)program/topics",parameters:parameters, completion: { (result, error) in
            
            print("Get program topics result:\(result)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [ProgramTopicListModel]
                {
                    let UserModelresult = result as? [ProgramTopicListModel]
                    if (UserModelresult?.count)! > 0{
                        if let dict = UserModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                return
                            }
                            print("dict:\(String(describing: dict.arrPerson_to_ask![0].strName))")
                            //Bind Topic ID
                            self.arrProgramTopicList.removeAll()
                            self.arrProgramTopicIDList.removeAll()
                            for ind in dict.arrProgram_topics!{
                                self.arrProgramTopicList.append(ind.strTopic_name)
                                self.arrProgramTopicIDList.append(ind.iTopic_id)
                            }
                            
                            
                            //Person to ASk
                            self.arrPersonToAskList.removeAll()
                            self.arrPersonToAskIDList.removeAll()
                            for indPerson in dict.arrPerson_to_ask!{
                                self.arrPersonToAskList.append(indPerson.strName)
                                self.arrPersonToAskIDList.append(indPerson.iID)
                            }
                            
                            
                            //self.arrProgramTopicList = dict.arrProgram_topics!
                           // self.arrProgramList = dict.arrProgramList!
                           // self.tblMain.reloadData()
                        }
                    }
                }
            }
        })
        }
    }
    
    
    
    func callAPI(){
        let parameters : Parameters = ["program_id": "\(self.iProgId)",
                                       "topic_id":"\(iTopicId)",
                                       "receiver_user_id":"\(iWhomeToAsk)",
            "query":self.txtQuestion.text ?? ""]
        
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = my_query_check_thread
        Singleton.shared.requiredToken = true
        
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)my-query/check/thread",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result.boolValue
                {
                   self.callAPIToSendQuery(iQueryThreadID:"\(APP_DELEGATE.iQueryThread)")
                    print("Query posted")
                    
                }else{
                    print("Query Posting failed")
                }
            }
        })
        }
    }
    
    
    
    func callAPIToSendQuery(iQueryThreadID:String){
        
        let parameters : Parameters = ["query_thread_id": iQueryThreadID,
            "query": self.txtQuestion.text ?? ""]
        
        
        print("Send Query parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = my_query_thread_message
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)my-query/thread/message",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result.boolValue
                {
                    print("Query posted")
                    self.initialiseDetails()
                    self.initPlaceholderForTextView()
                }else{
                    print("Query Posting failed")
                }
            }
        })
        }
    }
    
}
