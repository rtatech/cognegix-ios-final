//
//  LearningTreeViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 22/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class LearningTreeViewController: BaseVC, UIWebViewDelegate{
    
    
    //TODO: - General
    var strLearningTreeTitle : String = String()
    var itHasURL : Bool = Bool()
    var strLaunchURL : String = String()
    var progID : Int = Int()
    var progName : String = String()
    var arrURLVisited :[URL] = [URL]()
    var isLaunchButtonClick : Bool = Bool()
    //TODO: - Controls
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var viwWeb: UIWebView!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = strLearningTreeTitle
        //Bottom Menu Navigation
        self.createBottomView()
        
        viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        var learningTreeURL = "\(BASE_URL)program/learning/tree?tree_id=\(progID)&parent_node_title=\(progName)&parent_node_id=0&user_id=\(String(describing: SINGLETON_OBJECT.entUserModel.arrUser_profile_detail!.iUser_id))"
        
        learningTreeURL = learningTreeURL.replacingOccurrences(of: " ", with: "%20")
        print("learningTreeURL:\(learningTreeURL)")
        
        viwWeb.delegate = self
        if itHasURL{
            let url : NSString = strLaunchURL as NSString
            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            let searchURL : NSURL = NSURL(string: urlStr as String)!
            print("searchURL:\(searchURL)")
             if searchURL != nil{
                viwWeb.loadRequest(URLRequest(url: searchURL as URL))
             }
        }else{
            let url = URL (string: learningTreeURL)
            let requestObj = URLRequest(url: url!)
            viwWeb.delegate = self
            viwWeb.loadRequest(requestObj)
        }
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - UIWebView Delegate Method implemenation
    func webViewDidStartLoad(_ webView: UIWebView){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        General.showActivityIndicator()
       
        let strCurrentURL : URL = (viwWeb.request?.url?.absoluteURL)!
        print("self.arrURLVisited.last:\(String(describing: self.arrURLVisited.last))")
        
        
        print("learningTreeURL strCurrentURL:\(strCurrentURL)")
        
        if strCurrentURL.absoluteString.containsIgnoringCase("\(LEARNING_TREE_BASE_URL)svg?tree_id"){
            
            //self.arrURLVisited[0] == strCurrentURL
            self.arrURLVisited.append(strCurrentURL)
            
        }else{
            isLaunchButtonClick = true
        }
        
      
        
        
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        General.hideActivityIndicator()
        
       /*
        let strCurrentURL : URL = (viwWeb.request?.url?.absoluteURL)!
        print("self.arrURLVisited.last:\(String(describing: self.arrURLVisited.last))")
      
       // if self.arrURLVisited.last != strCurrentURL{
            print("learningTreeURL strCurrentURL:\(strCurrentURL)")
            
            if strCurrentURL.absoluteString.containsIgnoringCase("\(LEARNING_TREE_BASE_URL)svg?tree_id"){
               
                 //self.arrURLVisited[0] == strCurrentURL
                self.arrURLVisited.append(strCurrentURL)
                
            }else{
                isLaunchButtonClick = true
            }
            
            /*if strCurrentURL.absoluteString.containsIgnoringCase("storage/mediaserver") || strCurrentURL.absoluteString.containsIgnoringCase("storage/mediaserver") ||  strCurrentURL.absoluteString.containsIgnoringCase("https://www.youtube.com"){
                isLaunchButtonClick = true
            }*/
       // }
       */
       
        
    }
    
    //This function is use to create bottom view
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    //TODO: - UIButton Action
    
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            displayHomeScreen()
            break;
        case 1002:
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "idSearchViewController") as! SearchViewController
            self.navigationController?.pushViewController(searchVC, animated: true)
            break;
            break;
        case 1003:
            let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idHelpdeskViewController") as! HelpdeskViewController
            helpVC.isMenuNavigation = false
            self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
            displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        if isLaunchButtonClick{
            
            if self.arrURLVisited.count>0{
                //self.arrURLVisited = self.arrURLVisited.reversed()
                var url : URL!
                
               url =  self.arrURLVisited.last //self.arrURLVisited[0] //
                
                /*for index in 0...self.arrURLVisited.count-1{
                    if !self.arrURLVisited[index].absoluteString.containsIgnoringCase("\(LEARNING_TREE_BASE_URL)svg?tree_id") {
                        url = self.arrURLVisited[index]
                        break
                    }
                }*/
                
                /*for index in 0...self.arrURLVisited.count-1{
                    if !self.arrURLVisited[index].absoluteString.containsIgnoringCase("storage/mediaserver") || !self.arrURLVisited[index].absoluteString.containsIgnoringCase("storage/mediaserver") || !self.arrURLVisited[index].absoluteString.containsIgnoringCase("https://www.youtube.com") {
                        url = self.arrURLVisited[index]
                        break
                    }
                }*/
                
                
                print("On Back URL:\(String(describing: url))")
                let requestObj = URLRequest(url: url)
                viwWeb.loadRequest(requestObj)
                self.isLaunchButtonClick = false
            }
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        self.itHasURL = false
    }
}
