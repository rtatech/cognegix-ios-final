//
//  ForgotPasswordSecondViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 10/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordSecondViewController: BaseVC {

     //TODO: - General
    var strEmailId : String = String()
     //TODO: - Controls
    
    @IBOutlet weak var btnSubmitOutlet: UIButton!
    @IBOutlet weak var txtConfPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    override func viewDidLoad() {
        super.viewDidLoad()

         self.initialization()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        circularTextfield(textfield: self.txtOTP)
         circularTextfield(textfield: self.txtNewPassword)
         circularTextfield(textfield: self.txtConfPassword)
        circularButton(button: self.btnSubmitOutlet)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom Function
    func initialization(){
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        //Left padding
        self.txtOTP.setLeftPaddingPoints(20)
        self.txtNewPassword.setLeftPaddingPoints(20)
        self.txtConfPassword.setLeftPaddingPoints(20)
        
        //Right padding
        self.txtOTP.setRightPaddingPoints(8)
         self.txtNewPassword.setRightPaddingPoints(8)
         self.txtConfPassword.setRightPaddingPoints(8)
    }
    
    //TODO: - UIButton Action

    @IBAction func btnSubmitClick(_ sender: UIButton) {
        if self.txtOTP.validateBasic(name: "OTP") &&
            self.txtNewPassword.validateBasic(name: "New Password") &&
            self.txtConfPassword.validateBasic(name: "Confirm Password"){
            if self.txtNewPassword.text == self.txtConfPassword.text{
                self.callAPI()
            }else{
                self.showAlert(strMSG: "Password does not match with Confirm Password")
            }
            
        }
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    //TODO: - API Integration
    func callAPI(){
       
        let parameters : Parameters = ["email": strEmailId,
                                       "otp": self.txtOTP.text ?? "",
                                       "password": self.txtNewPassword.text ?? "",
                                       "password_confirmation": self.txtConfPassword.text ?? ""]
        

        print("Forgot Password second step:\(parameters)")
        
        let objData = DataManager()
         APP_DELEGATE.API_Name = OTPVerification
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)user/verify-otp",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                
                if result.boolValue{
                    
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                    
                    self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)

                }else{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                }
            }
        })
        }
    }
    
}
