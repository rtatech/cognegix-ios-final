//
//  ForgotPasswordFirstViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 10/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import Alamofire

class ForgotPasswordFirstViewController: BaseVC {

    
    
    //TODO: - General
    
    
    //TODO: - Controls
    
    @IBOutlet weak var btnSubmitOutlet: UIButton!
    @IBOutlet weak var txtEmailId: UITextField!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialization()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        circularTextfield(textfield: self.txtEmailId)
        circularButton(button: self.btnSubmitOutlet)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom Function
    func initialization(){
       self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        //Left padding
        self.txtEmailId.setLeftPaddingPoints(20)
        //Right padding
        self.txtEmailId.setRightPaddingPoints(8)
       
    }

    //TODO: - UIButton Action

    @IBAction func btnSubmitClick(_ sender: UIButton) {
        if self.txtEmailId.validateBasic(name: "Email ID"){
            self.callAPI()
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    //TODO: - API Integration
    func callAPI(){
        
         //tagsab2000@gmail.com
        let parameters : Parameters = ["email": self.txtEmailId.text ?? ""]
        
        print("forgot passowrd first parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = ForgetPassword
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)user/password/forget",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                
                if result.boolValue{
                    
                    APP_DELEGATE.errorMessage = ""
                    let forgotSecVC = self.storyboard?.instantiateViewController(withIdentifier: "idForgotPasswordSecondViewController")  as! ForgotPasswordSecondViewController
                    forgotSecVC.strEmailId = self.txtEmailId.text!
                    self.present(forgotSecVC, animated: true, completion: nil)
                }else{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                }
                
                
                /*if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                */
            }
        })
        
        }
        
        
    }
    
}
