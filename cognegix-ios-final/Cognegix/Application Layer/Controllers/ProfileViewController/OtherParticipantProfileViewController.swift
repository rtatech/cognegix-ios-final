//
//  OtherParticipantProfileViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 13/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class OtherParticipantProfileViewController: BaseVC ,iCarouselDelegate, iCarouselDataSource {
    
    //TODO: - General
     var objParticipantList = userListModel()
    var iProgramID : Int = Int()
    //TODO: - Controls
    
    //Profile
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    
    
    @IBOutlet weak var btnAssignBadgeOutlet: UIButton!
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var constHeight: NSLayoutConstraint!
    @IBOutlet weak var viwCustom: UIView!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var viwHeader: UIView!
    
    
    @IBOutlet weak var viwShowHide: UIView!
    //
    @IBOutlet weak var bottomView: BottomViewClass!
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var viwCarousel: iCarousel!
    
   
    //Social
    @IBOutlet weak var viewSocialOuter: UIView!
    @IBOutlet weak var viewSocialInner: UIView!
    
    @IBOutlet weak var imgFBArrow: UIImageView!
    @IBOutlet weak var imgTwitterArrow: UIImageView!
    @IBOutlet weak var imgGPlusArrow: UIImageView!
    @IBOutlet weak var imgLinkArrow: UIImageView!
    @IBOutlet weak var imgOutlookArrow: UIImageView!
    @IBOutlet weak var txtSocialLink: UITextField!
    
    
    
    
    //TODO: - Let's Code
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("Hello")
        //Revealviewcontroller code
        self.navigationController?.isNavigationBarHidden = true
        
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.autolayoutCustomView()
        self.createNewView()
        
        
        self.initialise()
         self.design()
        self.createBottomView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - Custom Function
    func initialise(){
        circularButton(button: self.btnAssignBadgeOutlet)
        
        circularImageView(imgViw: self.imgProfile)
        
        self.imgProfile.layer.borderColor = UIColor.white.cgColor
        self.imgProfile.layer.borderWidth = 5
        
        self.btnAssignBadgeOutlet.backgroundColor = UIColor(hexString: menuBgColor)
        self.viwCustom.backgroundColor = UIColor(hexString: menuBgColor)
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        self.bottomView.backgroundColor = UIColor(hexString: menuBgColor)
        
        roundedCornerView(viw: self.viwCarousel, clr: lightGrayColor)
        self.btnAssignBadgeOutlet.layer.borderColor = UIColor.white.cgColor
        self.btnAssignBadgeOutlet.layer.borderWidth = 5
        //iCaro
        pageControl.numberOfPages = 2
        pageControl.hidesForSinglePage = true
        viwCarousel.isPagingEnabled = true
        viwCarousel.bounces = false
        
        self.loadDataFromAPI()
    }
    
    func design(){
        self.viewSocialOuter.layer.cornerRadius = 5
        self.viewSocialOuter.clipsToBounds = true
        
        self.viewSocialInner.layer.cornerRadius = 5
        self.viewSocialInner.clipsToBounds = true
        
      
        self.txtSocialLink.isUserInteractionEnabled = false
        
        //Default FB Selection
        self.txtSocialLink.text = objParticipantList.strFacebook_id
        imgFBArrow.isHidden = false
        imgTwitterArrow.isHidden = true
        imgGPlusArrow.isHidden = true
        imgLinkArrow.isHidden = true
        imgOutlookArrow.isHidden = true
        
    }
    
    
    func loadDataFromAPI(){
        self.lblName.text = objParticipantList.strFirst_name + " " + objParticipantList.strLast_nam
        self.lblDesignation.text = objParticipantList.strDesignation
        self.lblCompany.text = objParticipantList.strOrganization_name
        
        self.imgProfile.sd_setImage(with: URL(string: (objParticipantList.strPhoto)), placeholderImage: UIImage(named: "profile_placeholder"))
        viwCarousel.reloadData()
        
        if objParticipantList.iIs_private == 0{
            //Public
            viwShowHide.isHidden = false
        }else{
             //Private
            viwShowHide.isHidden = true
        }
    }
    
    
    func autolayoutCustomView(){
        
        if APP_DELEGATE.screenWidth == 320{
            constTop.constant = -80
            constHeight.constant = 300
            
        }else if APP_DELEGATE.screenWidth == 375{
            constTop.constant = -40
            constHeight.constant = 300
            
        }else if APP_DELEGATE.screenWidth == 414{
            
        }else if APP_DELEGATE.screenWidth < 320{
            
        }
        self.viwCustom.layoutIfNeeded()
    }
    
    func createNewView(){
        viwCustom.backgroundColor = UIColor(hexString: menuBgColor)
        viwCustom.round(corners: [.bottomLeft, .bottomRight], radius: self.viwCustom.frame.size.width/2)
        self.viwCustom.layoutIfNeeded()
    }
    
    func createNewShape(){
        print("self.customView.frame.size.width:\(self.viwCustom.frame.size.width)")
        print("UIScreen.main.bounds.size.width:\(UIScreen.main.bounds.size.width)")
        self.constHeight.constant = UIScreen.main.bounds.size.width
        self.constTop.constant = -(UIScreen.main.bounds.size.width/2.3)
        
        self.viwCustom.layoutIfNeeded()
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.viwCustom.frame
        rectShape.position = self.viwCustom.center
        rectShape.path = UIBezierPath(roundedRect: self.viwCustom.bounds, byRoundingCorners: [.bottomLeft , .bottomRight , .bottomRight], cornerRadii: CGSize(width: self.viwCustom.frame.size.width, height: self.viwCustom.frame.size.width)).cgPath
        
        self.viwCustom.layer.backgroundColor = UIColor(hexString: menuBgColor)?.cgColor
        //Here I'm masking the textView's layer with rectShape layer
        self.viwCustom.layer.mask = rectShape
    }
    
    
    //MARK:- Carousel Delegate
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return 2
    }
    
    //show the card at the particular index
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let viwCard = (Bundle.main.loadNibNamed("BadgeView", owner: self, options: nil)![0] as? UIView)! as! BadgeView
        viwCard.frame = viwCarousel.frame
        print("asdasdas\(String(describing: objParticipantList.arrProgramBadgeLeadersList!.iParticipantFastestActivityBadge))")
        if index == 0{
            viwCard.imgFirst.image = UIImage(named: "badge1")
            viwCard.lblFirst.text = "Fastest Completion"
            viwCard.lblCountFirst.text = "\(String(describing: objParticipantList.arrProgramBadgeLeadersList!.iParticipantFastestActivityBadge))"//"3"
            viwCard.lblCountFirst.layer.cornerRadius = viwCard.lblCountFirst.frame.size.width / 2
            viwCard.lblCountFirst.clipsToBounds = true
            
            viwCard.imgSec.image = UIImage(named: "badge2")
            viwCard.lblSec.text = "Faculty Appreciation"
            viwCard.lblCountSec.text = "\(String(describing: objParticipantList.arrProgramBadgeLeadersList!.iParticipantFacilitatorBadgeCount))" //"3"
            viwCard.lblCountSec.layer.cornerRadius = viwCard.lblCountSec.frame.size.width / 2
            viwCard.lblCountSec.clipsToBounds = true
            
            viwCard.imgThird.image = UIImage(named: "badge3")
            viwCard.lblThird.text = "Participant Appreciation"
            viwCard.lblCountThird.text = "\(String(describing: objParticipantList.arrProgramBadgeLeadersList!.iParticipantParticipantBadgeCount))" //"3"
            viwCard.lblCountThird.layer.cornerRadius = viwCard.lblCountThird.frame.size.width / 2
            viwCard.lblCountThird.clipsToBounds = true
            
            
        }else{
            viwCard.imgFirst.image = UIImage(named: "badge4")
            viwCard.lblFirst.text = "Content Rating"
            viwCard.lblCountFirst.text = "\(String(describing: objParticipantList.arrProgramBadgeLeadersList!.iParticipantContentRatingBadgeCount))" //"3"
            viwCard.lblCountFirst.layer.cornerRadius = viwCard.lblCountFirst.frame.size.width / 2
            viwCard.lblCountFirst.clipsToBounds = true
            
            viwCard.imgSec.image = UIImage(named: "badge5")
            viwCard.lblSec.text = "Quiz Completion"
            viwCard.lblCountSec.text = "\(String(describing: objParticipantList.arrProgramBadgeLeadersList!.iParticipantQuizCompletionBadgeCount))" //"3"
            viwCard.lblCountSec.layer.cornerRadius = viwCard.lblCountSec.frame.size.width / 2
            viwCard.lblCountSec.clipsToBounds = true
            
            viwCard.imgThird.image = nil
            viwCard.lblThird.text = ""
            viwCard.lblCountThird.text = ""
            viwCard.lblCountThird.isHidden = true
            
        }
        
        return viwCard
    }
    //For spacing of two items
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if(carousel == viwCarousel)
        {
            if (option == .spacing) {
                return value * 1.1
            }
            return value
        }
        else
        {
            if (option == .spacing) {
                return value * 1.0
            }
            return value
        }
        
    }
    
    //scrolling started
    func carouselDidScroll(_ carousel: iCarousel) {
        let index = carousel.currentItemIndex
        
        pageControl.currentPage = index
    }
    
    //on select of specific item
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
    
    //scroll end
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        
    }
    
    
    
    
    
    
    //This function is use to create bottom view
     func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    
    
    //TODO: - UIButton Action
    
    //This method is use to show bottom button are pressed
    //1001: Home, 1002: Search, 1003: Assistance, 1004: Setting
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            displayHomeScreen()
            break;
        case 1002:
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "idSearchViewController") as! SearchViewController
            self.navigationController?.pushViewController(searchVC, animated: true)
            break;
        case 1003:
            let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idHelpdeskViewController") as! HelpdeskViewController
            helpVC.isMenuNavigation = false
            self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
            displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    
    @IBAction func btnAssignBadgeClick(_ sender: UIButton) {
        let bookCountVC = self.storyboard?.instantiateViewController(withIdentifier: "idParticipantAssignBadgeViewController") as! ParticipantAssignBadgeViewController
        bookCountVC.view.backgroundColor = .clear
        bookCountVC.modalPresentationStyle = .overCurrentContext
        bookCountVC.iRecipentId = objParticipantList.iUser_id
        bookCountVC.iProgramID = iProgramID
        bookCountVC.iRemainingBadge = SINGLETON_OBJECT.iRemainBadge   //objParticipantList.iRemaining_distribution_badges
        print("objParticipantList.iRemaining_distribution_badges:\(objParticipantList.iRemaining_distribution_badges)")
        self.present(bookCountVC, animated: false, completion: nil)
        
    }
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //Tag information: 101: FB, 102: Twitter, 103: g+, 104: Linked, 105: Outlook
    @IBAction func btnSocialClick(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 101:
            self.txtSocialLink.text = objParticipantList.strFacebook_id
            
            imgFBArrow.isHidden = false
            imgTwitterArrow.isHidden = true
            imgGPlusArrow.isHidden = true
            imgLinkArrow.isHidden = true
            imgOutlookArrow.isHidden = true
            
            break;
        case 102:
            self.txtSocialLink.text = objParticipantList.strTwitter_handle
            imgFBArrow.isHidden = true
            imgTwitterArrow.isHidden = false
            imgGPlusArrow.isHidden = true
            imgLinkArrow.isHidden = true
            imgOutlookArrow.isHidden = true
            
            break;
        case 103:
            self.txtSocialLink.text = objParticipantList.strGoogle_profile
            imgFBArrow.isHidden = true
            imgTwitterArrow.isHidden = true
            imgGPlusArrow.isHidden = false
            imgLinkArrow.isHidden = true
            imgOutlookArrow.isHidden = true
            
            break;
        case 104:
            self.txtSocialLink.text = objParticipantList.strLinkedin_profile
            imgFBArrow.isHidden = true
            imgTwitterArrow.isHidden = true
            imgGPlusArrow.isHidden = true
            imgLinkArrow.isHidden = false
            imgOutlookArrow.isHidden = true
            
            
            break;
        case 105:
            self.txtSocialLink.text = objParticipantList.strEmail
            imgFBArrow.isHidden = true
            imgTwitterArrow.isHidden = true
            imgGPlusArrow.isHidden = true
            imgLinkArrow.isHidden = true
            imgOutlookArrow.isHidden = false
            
            break;
        default:
            break;
        }
        
        
        
    }
    
    
}
