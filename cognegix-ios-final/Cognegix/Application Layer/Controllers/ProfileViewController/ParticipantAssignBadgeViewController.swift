//
//  ParticipantAssignBadgeViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 13/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import Alamofire

class ParticipantAssignTableViewCell : UITableViewCell{
    @IBOutlet weak var lblBadge: UILabel!
}


class ParticipantAssignBadgeViewController: BaseVC {
    
    //TODO: - General2018
    var valueArray : [String] = []
    var iRecipentId : Int = Int()
    var iProgramID : Int = Int()
    var iBadgeCount : Int = Int()
    var iRemainingBadge : Int = Int()
    //TODO: - Controls
    
    @IBOutlet weak var txtBadgeQty: SkyFloatingLabelTextField!
    @IBOutlet weak var btnCancelOutlet: UIButton!
    @IBOutlet weak var btnAssignOutlet: UIButton!
    @IBOutlet weak var viwBlur: UIView!
    @IBOutlet weak var viwAlert: UIView!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var lblCurrentLimit: UILabel!
    
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ParticipantAssignBadgeViewController.handleTap(_:)))
        print("iRemainingBadge:\(iRemainingBadge)")
        
        viwBlur.isUserInteractionEnabled = true
        viwBlur.addGestureRecognizer(tap)
        
        self.tblMain.tableFooterView = UIView()
        
        self.btnCancelOutlet.layer.cornerRadius = 5
        self.btnCancelOutlet.clipsToBounds = true
        self.btnAssignOutlet.layer.cornerRadius = 5
        self.btnAssignOutlet.clipsToBounds = true
        
        self.viwAlert.layer.cornerRadius = 5
        self.viwAlert.clipsToBounds = true
        print("iRemainingBadge:\(iRemainingBadge)")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
       
        self.lblCurrentLimit.text = "Current Limit: \(iRemainingBadge)"
        if SINGLETON_OBJECT.isFacilitator{
            self.lblCurrentLimit.isHidden = true
        }else{
             self.lblCurrentLimit.isHidden = false
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        // handling code
        print("Tapped")
        self.dismiss(animated: false, completion: nil)
    }
    
    
    //TAg: 11: Assign, 12: Cancel
    @IBAction func btnActionClick(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 11:
            if self.txtBadgeQty.text! != ""{
            iBadgeCount = Int(self.txtBadgeQty.text!)!
            if iBadgeCount>0{
                if SINGLETON_OBJECT.isFacilitator{
                 self.callAPI()
                }else{
                    if iRemainingBadge >= iBadgeCount{
                        self.callAPI()
                    }else{
                        self.showAlert(strMSG: "Badge count is low")
                    }
                }
            }else{
                 self.showAlert(strMSG: "Please enter badge")
            }
            }else{
                self.showAlert(strMSG: "Please enter badge")
            }
            break;
        case 12:
            self.dismiss(animated: false, completion: nil)
            break;
        default:
            break;
        }
    }
    
    
    
    
    //TODO: - API Integration
    func callAPI(){
        
        let parameters : Parameters = ["program_id": "\(iProgramID)",
                                       "badge_count":"\(iBadgeCount)",
                                       "recipient_user_id":"\(iRecipentId)"]
        
        print("badge distribute  parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = badge_distribute
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)badge/distribute",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [AssignBadgeModule]
                {
                    
                    let notificationsModelresult = result as? [AssignBadgeModule]
                    if (notificationsModelresult?.count)! > 0{
                        
                        if let dict = notificationsModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                return
                            }
                            print("dict:\(String(describing: dict.iremaining_distribution_badges))")
                            SINGLETON_OBJECT.iRemainBadge = dict.iremaining_distribution_badges
                          self.dismiss(animated: true, completion: nil)
                        }
                        
                    }
                }
            }
        })
        
        }
        
        
    }
    
}
