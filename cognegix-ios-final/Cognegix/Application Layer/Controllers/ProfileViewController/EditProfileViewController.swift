//
//  EditProfileViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 07/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import Alamofire

class EditProfileViewController: BaseVC, UINavigationControllerDelegate,UIImagePickerControllerDelegate, UITextFieldDelegate {

    
    
    //TODO: - General
    let imgPicker : UIImagePickerController = UIImagePickerController()
    var is_imageUpload : Bool = Bool()
    var is_profileImageSelected : Bool = Bool()
    
    //TODO: - Controls
    
    @IBOutlet weak var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtDesignation: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCompanyName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFb: SkyFloatingLabelTextField!
    @IBOutlet weak var txtTwitter: SkyFloatingLabelTextField!
    @IBOutlet weak var txtGPlus: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLinkedIn: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSecondEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSwitch: UISwitch!
    @IBOutlet weak var txtHomeNumber: SkyFloatingLabelTextField!
    
    
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnSaveOutlet: UIButton!
    @IBOutlet weak var viwCustom: UIView!
    
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var constHeight: NSLayoutConstraint!
    
    //TODO: - Let's Code
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgPicker.delegate = self
       
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.autolayoutCustomView()
        self.createNewView()
        
        self.initialise()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom function
    func initialise(){
        circularButton(button: self.btnSaveOutlet)
        circularImageView(imgViw: self.imgProfile)
        
         self.txtFirstName.delegate = self
        self.txtLastName.delegate = self
        self.txtDesignation.delegate = self
        self.txtPhoneNumber.delegate = self
        self.txtHomeNumber.delegate = self
        
        
        self.imgProfile.layer.borderColor = UIColor.white.cgColor
        self.imgProfile.layer.borderWidth = 5
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
        
        self.btnSaveOutlet.layer.borderColor = UIColor.white.cgColor
        self.btnSaveOutlet.layer.borderWidth = 5
        self.btnSaveOutlet.backgroundColor = UIColor(hexString: menuBgColor)
        self.viwCustom.backgroundColor = UIColor(hexString: menuBgColor)
        
        self.txtFirstName.text = SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strFirstName
        self.txtLastName.text = SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strLastName
        self.txtDesignation.text = SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strDesignation
        self.txtCompanyName.text = SINGLETON_OBJECT.entUserModel.arrUser_organization_detail?.strName
        
        self.txtFb.text = SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strFBProfile
        txtTwitter.text = SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strTwitterProfile
        txtGPlus.text = SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.googleProfile
        txtLinkedIn.text = SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strLinkedInProfile
        txtEmail.text = SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strEmail
        
        txtSecondEmail.text =  SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPersonalEmail
        
         self.imgProfile.sd_setImage(with: URL(string: (SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto)!), placeholderImage: UIImage(named: "profile_placeholder"))
        
        //Gender selection
        if (SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strGender.containsIgnoringCase("M"))!{
            btnSwitch.setOn(false, animated: true)
        }else{
            btnSwitch.setOn(true, animated: true)
        }
        
        txtHomeNumber.text = SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strHome_phone_number
        txtPhoneNumber.text = SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strWork_phone_number
    }
    
    func autolayoutCustomView(){
        
        if APP_DELEGATE.screenWidth == 320{
            constTop.constant = -80
            constHeight.constant = 300
            
        }else if APP_DELEGATE.screenWidth == 375{
            constTop.constant = -60
            constHeight.constant = 300
            
        }else if APP_DELEGATE.screenWidth == 414{
            
        }else if APP_DELEGATE.screenWidth < 320{
            
        }
        self.viwCustom.layoutIfNeeded()
    }
    
    func createNewView(){
        viwCustom.backgroundColor = UIColor(hexString: menuBgColor)
        viwCustom.round(corners: [.bottomLeft, .bottomRight], radius: self.viwCustom.frame.size.width/2)
        self.viwCustom.layoutIfNeeded()
    }
    
    
    //TODO: - Custom Function
    func askToChangeProfileImage(){
        let alert = UIAlertController(title: "Let's get a picture", message: "Choose a Picture Method", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        self.imgPicker.delegate = self
        
       /* if(is_imageUpload){
            let removeImageButton = UIAlertAction(title: "Remove profile picture", style: UIAlertActionStyle.destructive) { (UIAlertAction) -> Void in
                self.is_imageUpload = false
                self.imgProfile.image = UIImage(named: "profile_placeholder")
            }
            alert.addAction(removeImageButton)
        }else{
            print("No image is selected")
        }*/
        
        //Add AlertAction to select image from library
        let libButton = UIAlertAction(title: "Select photo from library", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            self.imgPicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.imgPicker.delegate = self
            self.imgPicker.allowsEditing = true
            self.is_profileImageSelected = true
            self.present(self.imgPicker, animated: true, completion: nil)
        }
        
        //Check if Camera is available, if YES then provide option to camera
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            let cameraButton = UIAlertAction(title: "Take a picture", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
                self.imgPicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imgPicker.allowsEditing = true
                self.is_profileImageSelected = true
                self.present(self.imgPicker, animated: true, completion: nil)
            }
            alert.addAction(cameraButton)
        } else {
            print("Camera not available")
            
        }
        
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (UIAlertAction) -> Void in
            print("Cancel Pressed")
        }
        
        alert.addAction(libButton)
        alert.addAction(cancelButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //TODO: - UITextFieldDelegate Method implementation
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      
        
        
        if textField == self.txtFirstName || textField == self.txtLastName || textField == self.txtDesignation{
        if string.characters.count > 0 {
           
            let alphaOnly = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
            let stringFromTextField = NSCharacterSet(charactersIn: string)
            let strValid = alphaOnly.isSuperset(of: stringFromTextField as CharacterSet)
            
            return strValid
            
            }
            
        }else if textField == self.txtPhoneNumber || textField == self.txtHomeNumber{
            if string.characters.count > 0 {
            let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
            let stringFromTextField = NSCharacterSet.init(charactersIn: string)
            var strValid = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
            
            if textField.text!.count > 9 {
                strValid = false
            }
            return strValid
            }
            
            
        }
        
        return true
    }
    
    
    
    // TODO: - UIImagePickerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let pickedImage : UIImage = info[UIImagePickerControllerEditedImage] as! UIImage
        
        // if(self.is_profileImageSelected){
        
        //let imageData = UIImageJPEGRepresentation(pickedImage, 0.8)
        self.imgProfile.image = pickedImage
        is_imageUpload = true
        self.callToProfilePicAPI()
        // }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        if(self.is_profileImageSelected){
            is_imageUpload = false
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    //TODO: - UIButton Action
    
    @IBAction func btnEditProfileClick(_ sender: UIButton) {
        self.askToChangeProfileImage()
    }
    
    
    @IBAction func btnSaveClick(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        var flag : Bool = Bool()
        var fbflag : Bool = Bool()
        var twflag : Bool = Bool()
        var gpflag : Bool = Bool()
        var lnflag : Bool = Bool()
        if self.txtFirstName.validateBasic(name: "First Name") &&
            self.txtLastName.validateBasic(name: "Last Name") &&
            self.txtDesignation.validateBasic(name: "Designation") &&
            self.txtEmail.validateBasic(name: "Email") &&
            self.txtSecondEmail.validateBasic(name: "Secondary Email"){
            flag = true
            
            if SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strFBProfile != ""{
                if self.txtFb.text == ""{
                    fbflag = false
                     showAlert(strMSG: "Please enter Facebook URL")
                }else{
                    if (self.txtFb.text?.containsIgnoringCase("https://www.facebook.com"))!{
                        fbflag = true
                    }else{
                        fbflag = false
                        showAlert(strMSG: "Please enter Valid Facebook URL")
                    }
                }
            }else{
                if (self.txtFb.text?.containsIgnoringCase("https://www.facebook.com"))!{
                    fbflag = true
                }else{
                    fbflag = false
                    showAlert(strMSG: "Please enter Valid Facebook URL")
                }
            }
            
            if SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strTwitterProfile != ""{
                if txtTwitter.text == ""{
                    twflag = false
                     showAlert(strMSG: "Please enter Twitter URL")
                }else{
                    if (self.txtTwitter.text?.containsIgnoringCase("https://www.twitter.com"))!{
                        twflag = true
                    }else{
                        twflag = false
                        showAlert(strMSG: "Please enter Valid Twitter URL")
                    }
                }
            }else{
                if (self.txtTwitter.text?.containsIgnoringCase("https://www.twitter.com"))!{
                    twflag = true
                }else{
                    twflag = false
                    showAlert(strMSG: "Please enter Valid Twitter URL")
                }
            }
            
            
            if SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.googleProfile != ""{
                if txtGPlus.text == ""{
                    gpflag = false
                    showAlert(strMSG: "Please enter Google URL")
                }else{
                    if (self.txtGPlus.text?.containsIgnoringCase("https://plus.google.com"))!{
                        gpflag = true
                    }else{
                        gpflag = false
                        showAlert(strMSG: "Please enter Valid Google URL")
                    }
                }
            }else{
                if (self.txtGPlus.text?.containsIgnoringCase("https://plus.google.com"))!{
                    gpflag = true
                }else{
                    gpflag = false
                    showAlert(strMSG: "Please enter Valid Google URL")
                }
            }
            
            
            
            if SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strLinkedInProfile != ""{
                if txtLinkedIn.text == ""{
                    lnflag = false
                    showAlert(strMSG: "Please enter LinkedIn URL")
                }else{
                    if (self.txtLinkedIn.text?.containsIgnoringCase("https://www.linkedin.com"))!{
                        lnflag = true
                    }else{
                        lnflag = false
                        showAlert(strMSG: "Please enter Valid LinkedIn URL")
                    }
                }
            }else{
                if (self.txtLinkedIn.text?.containsIgnoringCase("https://www.linkedin.com"))!{
                    lnflag = true
                }else{
                    lnflag = false
                    showAlert(strMSG: "Please enter Valid LinkedIn URL")
                }
            }
        }
        
        
        
        if flag && fbflag && twflag && gpflag && lnflag{
             self.callAPI()
        }
        
        
       
        
    }
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSwitchChanged(_ sender: Any) {
    }
    
    
    
    
    
    
    //TODO: - API Integration
    
    //Call to save profile details
    func callToProfilePicAPI(){
        
        var imageData: Data = Data()
        
        let idata: NSData? = UIImageJPEGRepresentation(self.imgProfile.image!, 0.3)! as Data as NSData;
            imageData.append(idata! as Data)
       
        let parameters : Parameters = ["": ""]
        
        print("Edit profile pic parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = update_profile_picture
        Singleton.shared.requiredToken = true
        objData.postRequestWithImageUpload(urlString: "\(BASE_URL)user/update/profile/picture", imageData: imageData, dict: parameters, completion: {(result, error) in
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [UserModel]
                {
                    
                    let UserModelresult = result as? [UserModel]
                    
                    if (UserModelresult?.count)! > 0{
                        if let dict = UserModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                 return
                            }
                            
                            print("dict:\(String(describing: dict.arrUser_detail?.strEmail))")
                            SINGLETON_OBJECT.entUserModel = dict
                            print("dict:\(String(describing: SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strEmail))")
                            self.imgProfile.sd_setImage(with: URL(string: (SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto)!), placeholderImage: UIImage(named: "profile_placeholder"))
                            self.sendBackAlert(strMessage: "Profile picture updated successfully!")
                        }
                    }
                }
            }
        })
        
        
        
        
    }
    
    //Call to save profile details
    func callAPI(){
        
        var imageData: Data = Data()
        
        let idata: NSData? = UIImageJPEGRepresentation(self.imgProfile.image!, 0.3)! as Data as NSData;
        imageData.append(idata! as Data)
        
        var genderValue = String()
        if btnSwitch.isOn{
            genderValue = "F"
        }else{
            genderValue = "M"
        }
        
        
        let parameters : Parameters = ["first_name": self.txtFirstName.text ?? "",
                                       "last_name": self.txtLastName.text ?? "",
                                       "gender":genderValue,
                                       "google_profile":self.txtGPlus.text ?? "",
                                       "linkedin_profile":self.txtLinkedIn.text ?? "",
                                       "facebook_id":self.txtFb.text ?? "",
                                       "twitter_handle":self.txtTwitter.text ?? "",
                                       "designation":self.txtDesignation.text ?? "",
                                       "personal_email":self.txtSecondEmail.text ?? "",
                                       "email":self.txtEmail.text ?? "",
                                       "work_phone_number":self.txtPhoneNumber.text ?? "",
                                       "home_phone_number":self.txtHomeNumber.text ?? ""
        ]
        
        print("Edit profile parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = updateProfile
        Singleton.shared.requiredToken = true
        
        
        
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToAPI("\(BASE_URL)user/update-detail",parameters:parameters, completion: { (result, error) in
       // objData.postRequestWithImageUpload(urlString: "\(BASE_URL)user/update-detail", imageData: imageData, dict: parameters, completion: {(result, error) in
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [UserModel]
                {
                    
                    let UserModelresult = result as? [UserModel]
                    
                    if (UserModelresult?.count)! > 0{
                        if let dict = UserModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                // APP_DELEGATE.showAlert(strMessage: dict.strMessage)
                                return
                            }
                            
                            print("dict:\(String(describing: dict.arrUser_detail?.strEmail))")
                            SINGLETON_OBJECT.entUserModel = dict
                            print("dict:\(String(describing: SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strEmail))")
                            self.imgProfile.sd_setImage(with: URL(string: (SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto)!), placeholderImage: UIImage(named: "profile_placeholder"))
                            self.sendBackAlert(strMessage: "Profile updated successfully!")
                        }
                    }
                }
            }
        })
        
        }
        
        
    }
    
    /*
    func callAPIToUploadImage(){
        var imageData: Data = Data()
        
        if   APP_DELEGATE.isImageSelected{
            let idata: NSData? = UIImageJPEGRepresentation(self.postImage, 0.3)! as Data as NSData;
            imageData.append(idata! as Data)
        }
        
        var typeOfMeal = String()
        
        if isVegSelected{
            typeOfMeal = typeOfMeal + "Veg,"
        }
        
        if isNonVegSelected{
            typeOfMeal = typeOfMeal + "Nonveg,"
            
        }
        
        if isVeganSelected{
            typeOfMeal = typeOfMeal + "Vegan"
        }
        
        
        
        
        
        /**/
        var tmpArra = [[String:String]]()
        if self.menuNameArray.count>0{
            for ind in 0...menuNameArray.count-1{
                var newDic = [String:String]()
                if self.ingredientNameArray.count>0{
                    newDic["\(self.menuNameArray[ind])"] = "\(self.ingredientNameArray[ind])"
                }else{
                    newDic["\(self.menuNameArray[ind])"] = ""
                }
                tmpArra.append(newDic)
            }
        }
        
        var jsonString : String = String()
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: tmpArra, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            print("jsonData:\(jsonData)")
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
            
        } catch {
            print(error.localizedDescription)
        }
        
        let parameters : Parameters = ["event_name":self.txtEventName.text!,
                                       "event_speciality":self.txtEventSpeciality.text ?? "",
                                       "date":self.convertDateFormater(self.txtDate.selectedItem!),
                                       "time":self.convertTimeFormat(time: self.txtTimeTo.selectedItem!),
                                       "dine_in_seats":self.txtDineIn.text ?? "0",
                                       "dine_in_price":self.txtDineInCost.text ?? "0",
                                       "take_away_seats":self.txtTakeAway.text ?? "0",
                                       "take_away_price":self.txtTakeAwayCost.text ?? "0",
                                       "type_of_Meal":typeOfMeal,
                                       "spice_meter":String(self.spiceMeter),
                                       "special_note":self.txtSpecialNote.text,
                                       "venue":self.txtAddress.text ?? "",
                                       "detail_address":self.txtAddress.text ?? "",
                                       "post_code":APP_DELEGATE.userZipcode,
                                       "latitude":String(APP_DELEGATE.userLocation.latitude),
                                       "longitude":String(APP_DELEGATE.userLocation.longitude),
                                       "user_ID":SINGLETON_OBJECT.entuserModel.user_ID ?? "",
                                       "result_type":"createevent",
                                       "menu":jsonString,
                                       "token":SINGLETON_OBJECT.entuserModel.token ?? ""]
        
        
        
        
        print("dictParam:\(parameters)")
        
        /**/
        
        
        print("Create Event Parameters:\(parameters)")
        let objData = DataManager()
        objData.postRequestWithImageUpload(urlString: "\(BASE_URL)createevent.php", imageData: imageData, videoData: videoData as Data, dict: parameters, completion: {(result, error) in
            
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if result is [EventModel]
                {
                    if APP_DELEGATE.errorMessage != ""{
                        self.showAlert(APP_NAME, message: APP_DELEGATE.errorMessage)
                        APP_DELEGATE.errorMessage = ""
                    }else{
                        let newresult = result as? [EventModel]
                        
                        if (newresult?.count)! > 0{
                            if let dict = newresult?[0] {
                                
                                SINGLETON_OBJECT.entEventModel = dict
                                NotificationCenter.default.post(name:NSNotification.Name(changeControllerToList), object: nil)
                                
                                print(SINGLETON_OBJECT.entEventModel.event_id ?? "")
                            }
                            
                        }
                    }
                }
            }
        })
        
    }
    */
    
    
}
