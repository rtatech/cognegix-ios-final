//
//  Strings.swift
//
//
//  Copyright © Suraj. All rights reserved.
//

import Foundation
import UIKit

//let BASE_URL = "http://cogvc.testursites.info/cgx-front/public/index.php/api/"
let BASE_URL = "http://182.59.4.193:8010/api/"
let VIDEO_BASE_URL = "http://182.59.4.193:8010"
let LEARNING_TREE_BASE_URL = "http://182.59.4.193:8010/"

let IMAGE_BASE_URL = ""
let APP_NAME = "Cognegix"

//MACROS

let USER_DEFAULTS = UserDefaults.standard
let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
let AlphabetsRange = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
let DigitRange = "0123456789"

let SINGLETON_OBJECT = Singleton.shared
let MAIN_STORYBOARD: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

let NO_NETWORK_CONNECTION = "The Internet connection appears to be offline. Please check your internet connection"
let SERVER_TIME_OUT = "The server is not responding, please try again later."
var appTitle = "Cognegix"

var strNoInternetConnection = "Please check internet connection and try again."
var strSomethigWentWrongRetry = "Something went wrong, Please try again."


//User roles
var strParticipant = "Participant"
var strFacilitator = "Facilitator"

//User Defaults
var strUserName = "strUserName"
var strUserPassword = "strUserPassword"
var isUserLogin = "isUserLogin"
//Color


//Notification Observer
let changeControllerToList = "changeControllerToList"

//Color Codes
let menuBgColor = "24a8f3"
let lightGrayColor = "9A9A9A"
let clearColor = ""
let NotificationCountBG = "5A7809"
let btnColor = "0C76B2"
let callInitiateToNumber = "9167572929"

var NotificationIdentifierReloaddashboard = "NotificationIdentifierReloaddashboard"
var NotificationIdentifierReloadProgramDetail = "NotificationIdentifierReloadProgramDetail"
var NotificationIdentifierReloadPercentage = "NotificationIdentifierReloadPercentage"
var NotificationIdentifierCloseCriticalAlert = "NotificationIdentifierCloseCriticalAlert"

//API Names:
var changepassword = "changepassword"
var update_profile_type = "update_profile_type"
var updateProfile = "updateProfile"
var update_profile_picture = "update_profile_picture"
var NotificationList = "NotificationList"
var program_detail = "program_detail"
var program_learning_tree_search = "program_learning_tree_search"
var badge_distribute = "badge_distribute"
var guest_help_raise = "guest_help_raise"
var Notification_mark_read = "Notification_mark_read"
var forumpostadd = "forumpostadd"
var forumposts = "forumposts"
var forum_thread_posts = "forum_thread_posts"
var forum_thread_post_delete = "forum_thread_post_delete"
var myquery_thread_list = "myquery-thread-list"
var my_query_thread_message_list = "my_query_thread_message_list"
var my_query_thread_message = "my_query_thread_message"
var my_query_check_thread =  "my_query_check_thread"
var program_topics = "program_topics"
var user_program_badges = "user_program_badges"
var program_badge_leaders = "program_badge_leaders"
var load_dashboard = "load_dashboard"
var user_past_programs = "user_past_programs"
var help_faq_list = "help_faq_list"
var program_users = "program_users"
var user_programs = "user_programs"
var Badges_of_User_by_Program = "Badges_of_User_by_Program"
var user_documents = "user_documents"
var pages = "pages"
var ForgetPassword = "Forget Password"
var OTPVerification = "OTPVerification"

var detailevent = "detailevent"
var verifysocialaccount = "verifysocialaccount"
var createevent = "createevent"
var bookevent = "bookevent"
var enquiry = "enquiry"
var signup_first = "signup_first"
var postcodeauthenticate = "postcodeauthenticate"

