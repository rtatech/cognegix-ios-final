//
//  BottomViewClass.swift
//  
//
//  Created by Suraj Sonawane on 13/03/18.
//  Copyright © 2018 team. All rights reserved.
//

import UIKit

class BottomViewClass: UIView {

    //TODO: - Controls03
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnHelpDesk: UIButton!
    @IBOutlet weak var btnSetting: UIButton!
    
    override init(frame: CGRect) { //For using custom view in code
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) { //For using custom view in IB
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    private func commonInit(){
        
        Bundle.main.loadNibNamed("BottomView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
    }
    
    
    //Tag: 11: Home, 12: Search: 13: Helpdesk 14: Setting
    @IBAction func btnHomeClick(_ sender: UIButton) {
        let tag = sender.tag
        
        switch tag {
        case 11:
            print("Home button click")
            break;
        case 12:
            print("Search button click")
            break;
        case 13:
            print("HelpDesk button click")
            break;
        case 14:
            print("Setting button click")
            break;
        default:
            break;
        }
    }
    
    

}
