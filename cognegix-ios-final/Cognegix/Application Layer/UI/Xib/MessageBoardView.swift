//
//  MessageBoardView.swift
//  LMC App
//
//  Created by Suraj Sonawane on 21/03/18.
//  Copyright © 2018 team. All rights reserved.
//

import UIKit

class MessageBoardView: UIView {

    @IBOutlet weak var lblDateVal: UILabel!
    @IBOutlet weak var btnViewOutlet: UIButton!
    @IBOutlet weak var lblSenderVal: UILabel!
    @IBOutlet weak var lblSubjectVal: UILabel!
    @IBOutlet weak var outerView: UIView!
    

}
