//
//  LeaderBoardView.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 29/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class LeaderBoardView: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgBadge: UIImageView!
    
    @IBOutlet weak var lblBadgeCount: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
}
