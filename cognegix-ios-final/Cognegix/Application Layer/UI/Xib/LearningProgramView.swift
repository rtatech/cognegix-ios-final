//
//  LearningProgramView.swift
//  
//
//  Created by Suraj Sonawane on 17/03/18.
//  Copyright © 2018 team. All rights reserved.
//

import UIKit

class LearningProgramView: UIView {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var imgProgram: UIImageView!
    @IBOutlet weak var lblProgramTitle: UILabel!
    @IBOutlet weak var lblProgramObj: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var btnLaunch: UIButton!
    

}
