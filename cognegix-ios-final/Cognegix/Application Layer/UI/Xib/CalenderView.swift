//
//  CalenderView.swift
//  LMC App
//
//  Created by Suraj Sonawane on 21/03/18.
//  Copyright © 2018 team. All rights reserved.
//

import UIKit

class CalenderView: UIView {

    @IBOutlet weak var outerView: UIView!
    
    @IBOutlet weak var lblFirstTitle: UILabel!
    @IBOutlet weak var lblFirstDate: UILabel!
    @IBOutlet weak var lblFirstIndicator: UILabel!
    @IBOutlet weak var btnFirstOutlet: UIButton!
    
    @IBOutlet weak var lblTwoTitle: UILabel!
    @IBOutlet weak var lblTwoDate: UILabel!
    @IBOutlet weak var lblTwoIndicator: UILabel!
    @IBOutlet weak var btnTwoOutlet: UIButton!
    
    @IBOutlet weak var lblThreeTitle: UILabel!
    @IBOutlet weak var lblThreeDate: UILabel!
    @IBOutlet weak var lblThreeIndicator: UILabel!
    @IBOutlet weak var btnThreeOutlet: UIButton!
    
    @IBOutlet weak var lblFourTitle: UILabel!
    @IBOutlet weak var lblFourDate: UILabel!
    @IBOutlet weak var lblFourIndicator: UILabel!
    @IBOutlet weak var btnFourOutlet: UIButton!
    
    @IBOutlet weak var lblFiveTitle: UILabel!
    @IBOutlet weak var lblFiveDate: UILabel!
    @IBOutlet weak var lblFiveIndicator: UILabel!
    @IBOutlet weak var btnFiveOutlet: UIButton!
    
    @IBOutlet weak var lblSixTitle: UILabel!
    @IBOutlet weak var lblSixDate: UILabel!
    @IBOutlet weak var lblSixIndicator: UILabel!
    @IBOutlet weak var btnSixOutlet: UIButton!
    
    @IBOutlet weak var lblSevenTitle: UILabel!
    @IBOutlet weak var lblSevenDate: UILabel!
    @IBOutlet weak var lblSevenIndicator: UILabel!
    @IBOutlet weak var btnSevenOutlet: UIButton!
    
    
    
    
    
    
    
}
