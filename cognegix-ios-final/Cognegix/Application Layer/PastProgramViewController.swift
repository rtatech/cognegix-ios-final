//
//  PastProgramViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 08/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit
import Alamofire

class PastProgramTableViewCell : UITableViewCell{
    
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var lblCompletionDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblIcon: UILabel!
    @IBOutlet weak var viwOuter: UIView!
}

class PastProgramViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {

    
    //TODO: - General
    var titleArray = ["Understanding of Negotiation Skills","Understanding negotiation situation","Grievance handling","Performance Appraisal handling"]
    var programImageArray : [String] = ["temp_program_screen.png","temp_program_screen1.jpg","temp_program_screen2.jpg","temp_program_screen3.jpg"]
    
    var arrProgramList = [programListModel]()
    
    //TODO: - Controls
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var menuBtnOutlet: UIButton!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Bottom Menu Navigation
        self.createBottomView()
        self.bottomView.backgroundColor = UIColor(hexString: menuBgColor)
        
        circularButton(button: self.btnProfileImage)
        circularLabel(label: self.lblNotificationCount)
        self.lblNotificationCount.backgroundColor = UIColor(hexString: NotificationCountBG)
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        self.tblMain.tableFooterView = UIView()
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        
        //Revealviewcontroller code
        self.navigationController?.isNavigationBarHidden = true
        if self.revealViewController() != nil {
            menuBtnOutlet.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        //Set profile iamge
        if SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto != ""{
            self.btnProfileImage.setImage(nil, for: UIControlState.normal)
            self.btnProfileImage.sd_setBackgroundImage(with: URL(string:(SINGLETON_OBJECT.entUserModel.arrUser_profile_detail?.strPhoto)!), for: .normal)
        }
        let count = Int(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)
        if count == 0{
            self.lblNotificationCount.isHidden = true
        }
        
        self.lblNotificationCount.text = "\(SINGLETON_OBJECT.entDashboardModel.iUnread_notification_count)"
        
        self.callAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //TODO: - UTableViewDatasource method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrProgramList.count//titleArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! PastProgramTableViewCell
        cell.selectionStyle = .none
        
        cell.lblIcon.layer.cornerRadius = cell.lblIcon.frame.size.width/2
        cell.lblIcon.clipsToBounds = true
        
        roundedCornerView(viw: cell.viwOuter, clr: lightGrayColor)
        roundedCornerButton(button: cell.btnView, corner: 5, clr: btnColor)
        
        let data = self.arrProgramList[indexPath.row]
        
        
        cell.lblCompletionDate.text = "Completion Date: \(data.strEnd_Date)"
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(btnGetCVVPressed), for: .touchUpInside)
        cell.lblTitle.text = data.strName
        cell.lblIcon.text = "UN"
        return cell
    }
    
    
    //This function is use to create bottom view
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    //TODO: - UIButton Action
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            displayHomeScreen()
            break;
        case 1002:
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "idSearchViewController") as! SearchViewController
            self.navigationController?.pushViewController(searchVC, animated: true)
            break;
        case 1003:
            let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idHelpdeskViewController") as! HelpdeskViewController
            helpVC.isMenuNavigation = false
            self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
           displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    
    //TODO: - UIButton Action
    
    @objc func btnGetCVVPressed(_ sender: UIButton) {
        let progDtVC = self.storyboard?.instantiateViewController(withIdentifier: "idProgramDetailsViewController") as! ProgramDetailsViewController
        
        let data = self.arrProgramList[sender.tag]
        SINGLETON_OBJECT.objProgram = data
         progDtVC.selectedProgDoc = data.strDocument_path
        progDtVC.selecteProgramId = data.iID
        progDtVC.selecteProgramName = data.strName
        SINGLETON_OBJECT.selectedProgIndex = sender.tag
        self.navigationController?.pushViewController(progDtVC, animated: true)
    }
    
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }

    
    
    
    
    //TODO: - API Integration
    func callAPI(){
        
        let parameters : Parameters = ["": ""]
        
        print("dashboard parameters:\(parameters)")
        
        let objData = DataManager()
        APP_DELEGATE.API_Name = user_past_programs
        Singleton.shared.requiredToken = true
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            
            self.showAlert(strMSG: strNoInternetConnection)
            return
        }else{
            print("Internet Connection Available!")
            
            
            
        objData.callToGetAPI("\(BASE_URL)user/past/programs",parameters:parameters, completion: { (result, error) in
            
            print("result:\(result.count)")
            if error.isKind(of: NSError.classForCoder()) //Error code
            {
                General.hideActivityIndicator()
            }
            else
            {
                General.hideActivityIndicator()
                if APP_DELEGATE.errorMessage != ""{
                    self.showAlert(strMSG: APP_DELEGATE.errorMessage)
                    APP_DELEGATE.errorMessage = ""
                }
                if result is [DashboardModel]
                {
                    
                    let UserModelresult = result as? [DashboardModel]
                    
                    if (UserModelresult?.count)! > 0{
                        if let dict = UserModelresult?[0] {
                            if !dict.bStatus {
                                self.showAlert(strMSG: dict.strMessage)
                                return
                            }
                            
                           self.arrProgramList = dict.arrProgramList!
                            self.tblMain.reloadData()
                        }
                    }
                }
            }
        })
        }
    }
    
    
}
