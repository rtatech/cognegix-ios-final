//
//  Bridging-Header.h
//  Cognegix
//
//  Created by Suraj Sonawane on 25/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

#ifndef Bridging_Header_h
#define Bridging_Header_h



#import "SWRevealViewController.h"


//ICarousel
#import "iCarousel.h"


//LinkLabel
#import "UIKit/UIGestureRecognizerSubclass.h"



//Start Rating
#import "HCSStarRatingView.h"

#endif /* Bridging_Header_h */
