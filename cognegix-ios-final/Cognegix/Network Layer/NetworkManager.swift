//
//  NetworkManager.swift
//
//


import Foundation
import Alamofire

class NetworkManager {
    
    //MARK: GET
    
    class func request(string: String, block: @escaping ([String: Any]?)->()) {
        Alamofire.request(string).responseJSON { response in
            
            // print(response.request)  // original URL request
            // print(response.response) // HTTP URL response
            // print(response.data)     // server data
            // print(response.result)   // result of response serialization
            
            if let result = response.result.value as? [String: Any] {
                block(result)
            } else {
                block(nil)
            }
        }
    }
    
    //MARK: POST
    
   /* class func urlRequestWithComponentsForUploadMultipleImages(urlString:String, parameters:Dictionary<String, String>, imagesData:[Data], imageName: String) -> (URLRequestConvertible , Data) {
        
        // create url request to send
        var mutableURLRequest = URLRequest(url: NSURL(string: urlString)! as URL)
        
        mutableURLRequest.httpMethod = Alamofire.HTTPMethod.post.rawValue
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        
        // create upload data to send
        var uploadData = Data()
        // add image
        for data in imagesData {
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Disposition: form-data; name=\"\(imageName)\"; filename=\"\(Date().timeIntervalSince1970).jpeg\"\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Type: image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append(data)
        }
        // add parameters
        for (key, value) in parameters {
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
        }
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        print("upload",parameters)
        return (mutableURLRequest , uploadData)
    }*/
    
   /* class func postRequestWithImageUpload(urlString: String, imagesData: [Data], dict: Parameters, block: @escaping ([String: Any]?)->()) {
        
        if APP_DELEGATE.isNetworkAvailable == true {
            
            MBProgressHUD.showAdded(to: APP_DELEGATE.window!, animated: true)
            
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                // import image to request
                var i = 1
                
                for imageData in imagesData {
                    
                    multipartFormData.append(imageData, withName: "image\(i)", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                    i += 1
                }
                
//                for (key, value) in dict {
                    for  (key, value)  in dict as! [String : String] {

//                    if let val = value {
                        multipartFormData.append(value.data(using: .utf8)!, withName: key)
//                    }
//                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
//                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)

                }
                
            }, to: self.getCompleteUrl(urlString),
               
               encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        MBProgressHUD.hide(for: APP_DELEGATE.window!, animated: true)

                        if response.result.isSuccess {
                            
                            if let result = response.result.value as? [String: Any] {
                                print("result:\(result)")
                                if result["status"] as! String == "S"  {
                                    block(result)
                                }
                                else {
                                    SCLAlertView().showError(ERROR_STRING, subTitle: result["message"] as! String)
                                    
                                    block(nil)
                                }
                            } else {
                                block(nil)
                            }
                        }
                    }
                case .failure(let error):
                    print(error)
                    MBProgressHUD.hide(for: APP_DELEGATE.window!, animated: true)

                }
                
            })
            
        }
        else {
            MBProgressHUD.hide(for: APP_DELEGATE.window!, animated: true)
            //            UIUtils.alertViewWithMessage(ERROR_STRING, message: NO_NETWORK_CONNECTION)
            SCLAlertView().showError(ERROR_STRING, subTitle: NO_NETWORK_CONNECTION)
            
        }
        
    }*/

    
    
    class func postRequest(urlString: String,dict: Parameters, block: @escaping ([String: Any]?)->()) {
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
       // if APP_DELEGATE.isNetworkAvailable == true {
            
            //MBProgressHUD.showAdded(to: APP_DELEGATE.window!, animated: true)
            
            Alamofire.request(self.getCompleteUrl(urlString), method: .post, parameters: dict, encoding: URLEncoding.httpBody, headers: headers).responseJSON
                { response in
                    
                    if response.result.isSuccess {
                        
                        if let result = response.result.value as? [String: Any] {
                            print("result:\(result)")
                            if result["data"] != nil  {
                                block(result["data"] as? [String : Any])
                            }
                            else {
//                                UIUtils.alertViewWithMessage(ERROR_STRING, message:result["message"] as! String)
                                
                               /* if result["message"] as! String != "User Post Not Found" {
                                   // SCLAlertView().showError(ERROR_STRING, subTitle: result["message"] as! String)
                                    block(result)
                                } else {
                                    block(nil)
                                }*/

                            }
                        } else {
                            block(nil)
                        }
                        
                    } else {
                        
                        print("Error : \(String(describing: response.result.error))")
//                        showErrorAlert(response.result.error! as NSError)
//                        UIUtils.alertViewWithMessage(ERROR_STRING, message:PAGE_NOT_FOUND_STRING)
                       // SCLAlertView().showError(ERROR_STRING, subTitle: PAGE_NOT_FOUND_STRING)


                        block(nil)
                    }
                    
                   // MBProgressHUD.hide(for: APP_DELEGATE.window!, animated: true)
            }
            
        //}
       /* else {
            MBProgressHUD.hide(for: APP_DELEGATE.window!, animated: true)
//            UIUtils.alertViewWithMessage(ERROR_STRING, message: NO_NETWORK_CONNECTION)
            SCLAlertView().showError(ERROR_STRING, subTitle: NO_NETWORK_CONNECTION)

        }*/
        
    }
    
    //MARK: Private Methods
    
    class func getCompleteUrl(_ url: String) -> String {
        
        let baseUrl: String = BASE_URL
        var completedURL: String =  ""
        let url1 = url
        
        if !url.isEmpty && !baseUrl.isEmpty     {
            
            let imageURL = url[url.startIndex] == "/" ? String(url1.substring(from: url1.characters.index(after: url1.startIndex))): url1
            completedURL = baseUrl[baseUrl.characters.index(before: baseUrl.endIndex)] != "/" ? String(baseUrl+"/"): baseUrl
            completedURL.append(imageURL)
            completedURL =  completedURL.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        }
        
        print("Complete Url :",completedURL)
        
        return completedURL
    }

    //MARK: Server Error Alert
    
    class func showErrorAlert(_ error:NSError) {
        
        if error._code == NSURLErrorTimedOut {
            //Show alert mesage
           // UIUtils.alertViewWithMessage(ALERT_STRING, message:SERVER_TIME_OUT)
        }
    }
    
    //MARK: Server response validations with code
    
    class func checkServerRequestFailuire(_ object: AnyObject) -> Bool {
        
//        if object.isKind(of: NSDictionary()) {
//            
//            let code = Int(object["cod"] as! String)
//            
//            MBProgressHUD.hide(for: APP_DELEGATE.window!, animated: true)
//            
//            if code == 200 {
//                
//                return false
//            }
//            if code == 401 || code == 404 {
//                
//                //                let message = object["message"] as! String
//                UIUtils.alertViewWithMessage("Alert", message: "INCORRECT_API_NAME")
//                return true
//            }
//            
//        }
        return false
    }

    
}
