//
//  RequestManager.swift

import Foundation
import Alamofire

import SwiftyJSON

class RequestManager: NSObject
{
    /**
     Function to call GET API with url

     - Parameter query: Web API url

     - Returns: completion handle with result or error
     */

    
    func requestUrl( query: String,params:Parameters,completion:@escaping (_ result: AnyObject, _ error: AnyObject)->Void)
    {
        
        var headers = [
            "Content-Type": "application/json"
        ]
        
        print("Singleton.shared.requiredToken:\(Singleton.shared.requiredToken)")
        if Singleton.shared.requiredToken{
            headers = [
                "Content-Type": "application/json",
                "Accept":"application/json",
                "Authorization":"Bearer \(Singleton.shared.entUserModel.arrUser_detail?.strApiToken ?? "")"
            ]
        }
    
       
         print("headers:\(headers)")
        print("params:\(params)")
        
        Alamofire.request(query, method: .post, parameters: params, encoding: JSONEncoding.default, headers:headers)
            .responseString{response in
                print(response)}
            .responseJSON { response in
                print(response.result)
                
            switch response.result
            {
            case .success: //success
                if let value = response.result.value
                {
                    completion(value as AnyObject, "" as AnyObject)
                }
                else if let value = response.result.error
                {
                    completion("" as AnyObject, value as AnyObject )
                }
            case .failure(let error): //error
                completion("" as AnyObject, error as AnyObject)
            }
           
        }
    }
    
    func requestDashboardGetUrl( query: String,params:Parameters,completion:@escaping (_ result: AnyObject, _ error: AnyObject)->Void)
        {
            
            var valheaders = [
                "Content-Type": "application/json"
            ]
            
            print("Singleton.shared.requiredToken:\(Singleton.shared.requiredToken)")
            if Singleton.shared.requiredToken{
                valheaders = [
                    "Accept":"application/json",
                    "Authorization":"Bearer \(Singleton.shared.entUserModel.arrUser_detail?.strApiToken ?? "")"
                ]
            }
            
            
            print("headers:\(valheaders)")
            print("params:\(params)")
            
            
            Alamofire.request(query,
                              method: .get,
                              headers:valheaders)
                .validate()
                .responseJSON { response in
                    guard response.result.isSuccess else {
                       completion("" as AnyObject, "" as AnyObject)
                        return
                    }
                    
                    if let value = response.result.value
                    {
                        completion(value as AnyObject, "" as AnyObject)
                    }
                    
            }
            
            
            
            
           /* Alamofire.request(query, method: .get, parameters: params, encoding: JSONEncoding.default, headers:valheaders)
                .responseString{response in
                    print("response:\(response)")}
                .responseJSON { response in
                    print(response.result)
                    
                    switch response.result
                    {
                    case .success: //success
                        if let value = response.result.value
                        {
                            completion(value as AnyObject, "" as AnyObject)
                        }
                        else if let value = response.result.error
                        {
                            completion("" as AnyObject, value as AnyObject )
                        }
                    case .failure(let error): //error
                        completion("" as AnyObject, error as AnyObject)
                    }
            }*/
        }
    
    
    
    func requestGetUrl( query: String,params:Parameters,completion:@escaping (_ result: AnyObject, _ error: AnyObject)->Void)
    {
        
        var valheaders = [
            "Content-Type": "application/json"
        ]
        
        print("Singleton.shared.requiredToken:\(Singleton.shared.requiredToken)")
        if Singleton.shared.requiredToken{
            valheaders = [
                "Accept":"application/json",
                "Authorization":"Bearer \(Singleton.shared.entUserModel.arrUser_detail?.strApiToken ?? "")"
            ]
        }
        
        
        print("headers:\(valheaders)")
        print("params:\(params)")
        
        
        Alamofire.request(query, method: .get, parameters: params, encoding: JSONEncoding.default, headers:valheaders)
            .responseString{response in
                print("response:\(response)")}
            .responseJSON { response in
                print(response.result)
                
                switch response.result
                {
                case .success: //success
                    if let value = response.result.value
                    {
                        completion(value as AnyObject, "" as AnyObject)
                    }
                    else if let value = response.result.error
                    {
                        completion("" as AnyObject, value as AnyObject )
                    }
                case .failure(let error): //error
                    completion("" as AnyObject, error as AnyObject)
                }
        }
    }
    
    
}
